﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BibliotekaKsiazek;
using System.Diagnostics;

namespace Biblioteka.Forms
{
    public partial class PanelCzytelnika : Form
    {
        private KsiazkaRepository ksiazkaRepository;
        private CzasopismoRepository czasopismoRepository;
        private KartaCzytelnikaRepository kartaCzytelnikaRepository;
        private WCRepository wcRepository;
        private WKRepository wkRepository;
        private ECRepository ecRepository;
        private EKRepository ekRepository;

        private static KartaCzytelnika karta;
        public KartaCzytelnika Karta
        {
            get { return karta; }
            set { karta = value; }
        }

        private static IEnumerable<Wypozyczenie> wypozyczeniaCzytelnika;
        public IEnumerable<Wypozyczenie> WypozyczeniaCzytelnika
        {
            get { return wypozyczeniaCzytelnika; }
            set { wypozyczeniaCzytelnika = value; }
        }

        public PanelCzytelnika(string gosc)
        {
            InitializeComponent();

            ksiazkaRepository = new KsiazkaRepository();
            czasopismoRepository = new CzasopismoRepository();
            kartaCzytelnikaRepository = new KartaCzytelnikaRepository();
            wcRepository = new WCRepository();
            wkRepository = new WKRepository();
            ecRepository = new ECRepository();
            ekRepository = new EKRepository();

            PobierzDane(); //Pobieramy dane wszystkich repozytoriów

            //Pobieramy dane do kontrolek 
            LoadKsiazkiGridView();
            LoadCzasopismaGridView();

            LoadKategoriaComboBox();
            LoadTematykaComboBox();
            LoadCzestotliwoscComboBox();
            LoadOdbiorcaComboBox();

            this.toolStripStatusLabel2.Text = String.Format("Zalogowano jako {0}", gosc);
            this.imieLabel.Text = "Gość";
            this.nazwiskoLabel.Text = "Gość";
            this.peselLabel.Text = "";
            this.plecLabel.Text = "";
            this.dataUrodzeniaLabel.Text = Convert.ToString(DateTime.Today.ToShortDateString()); //ustawione na dziś że niby gość sie urodził w tej chwili
            this.label5.Visible = false;
            this.label4.Visible = false;

            this.groupBox2.Visible = false;
            this.groupBox3.Visible = false;
            this.szczegolyKsiazkiButton.Visible = false;
            this.szczegolyCzasopismaButton.Visible = false;
            this.zmianaHaslaMenuItem.Visible = false;   

            this.wypozyczeniaGridView.Visible = false; // ukrywa data griedView z wypozyczeniami
            this.label6.Text = @"Gość nie może mieć wypożyczonych ksiażek i czasopism, 
            musisz być zalogowany aby to uczynić"; //ustawia text
            this.label6.Font = new Font(this.Font.FontFamily, 20); // ustawia czcionke labela
            this.label6.Location = new Point(150, 55); //ustawia na jakiej pozycji ma się znajdować label6
        }

        public PanelCzytelnika(Czytelnik czytelnik)
        {
            InitializeComponent();

            ksiazkaRepository = new KsiazkaRepository();
            czasopismoRepository = new CzasopismoRepository();
            kartaCzytelnikaRepository = new KartaCzytelnikaRepository();
            wcRepository = new WCRepository();
            wkRepository = new WKRepository();
            ecRepository = new ECRepository();
            ekRepository = new EKRepository();

            PobierzDane();

            KartaCzytelnika karta = kartaCzytelnikaRepository.PoCzytelniku(czytelnik);
            this.Karta = karta;

            //Pobieramy dane do kontrolek
            LoadKsiazkiGridView();
            LoadCzasopismaGridView();
            LoadWypozyczeniaGridView();

            LoadKategoriaComboBox();
            LoadTematykaComboBox();
            LoadCzestotliwoscComboBox();
            LoadOdbiorcaComboBox();

            this.toolStripStatusLabel2.Text = String.Format("Zalogowano jako {0}", czytelnik.Imie);

            //Dane czytelnika
            this.imieLabel.Text = czytelnik.Imie;
            this.nazwiskoLabel.Text = czytelnik.Nazwisko;
            this.peselLabel.Text = czytelnik.Pesel;
            if (czytelnik.Plec.ToString().Equals("M"))
                this.plecLabel.Text = "Mężczyzna";
            else
                this.plecLabel.Text = "Kobieta";
            this.dataUrodzeniaLabel.Text = Convert.ToString(czytelnik.DataUrodzenia.ToShortDateString());

            //Dane dot. adresu czytelnika
            Adres adresCzytelnika = czytelnik.Adres;
            this.ulicaLabel.Text = adresCzytelnika.Ulica;
            this.kodPocztowyLabel.Text = adresCzytelnika.KodPocztowy;
            this.miejscowoscLabel.Text = adresCzytelnika.Miejscowosc;
            this.telefonLabel.Text = adresCzytelnika.Telefon;
            this.statusLabel.Text = karta.Status;
            this.dataWystawieniaLabel.Text = Convert.ToString(karta.DataWystawienia.ToShortDateString());
            this.terminWaznosciLabel.Text = Convert.ToString(karta.DataWystawienia.ToShortDateString());

            this.label6.Visible = false;
        }

        public PanelCzytelnika()
        {
            //InitializeComponent();
        }

        public void PobierzDane()
        {
            ksiazkaRepository.PobierzDane();
            czasopismoRepository.PobierzDane();
            kartaCzytelnikaRepository.PobierzDane();
            ekRepository.PobierzDane();
            ecRepository.PobierzDane();
            wkRepository.PobierzDane();
            wcRepository.PobierzDane();
        }

        private void LoadKategoriaComboBox()
        {
            this.kategoriaComboBox.DataSource = ZagadnieniaRepository.WszystkieKategorie();
            this.kategoriaComboBox.SelectedValue = -1;
        }

        private void LoadTematykaComboBox()
        {
            this.tematykaComboBox.DataSource = ZagadnieniaRepository.WszystkieTematyki();
            this.tematykaComboBox.SelectedValue = -1;
        }

        private void LoadCzestotliwoscComboBox()
        {
            this.czestotliwoscComboBox.DataSource = ZagadnieniaRepository.WszystkieCzestotliwosci();
            this.czestotliwoscComboBox.SelectedValue = -1;
        }

        private void LoadOdbiorcaComboBox()
        {
            this.odbiorcaComboBox.DataSource = ZagadnieniaRepository.WszyscyOdbiorcy();
            this.odbiorcaComboBox.SelectedValue = -1;
        }

        //Dane do GridView Książek
        private void LoadKsiazkiGridView()
        {
            this.ksiazkiGridView.AutoGenerateColumns = false;
            this.ksiazkiGridView.DataSource = ksiazkaRepository.Lista;
            this.ksiazkiGridView.DoubleBuffered(true); //Poprawienie płynności przewijania listy
        }
        //Dane do GridView Czasopism
        private void LoadCzasopismaGridView()
        {
            this.czasopismaGridView.AutoGenerateColumns = false;
            this.czasopismaGridView.DataSource = czasopismoRepository.Lista;
            this.czasopismaGridView.DoubleBuffered(true); //Poprawienie płynności przewijania listy
        }

        private void LoadWypozyczeniaGridView()
        {
            this.wypozyczeniaGridView.Rows.Clear();

            IEnumerable<Wypozyczenie> lista1 = wkRepository.WypozyczonePoCzytelniku(karta); //Pobieramy jego wypożyczenia
            IEnumerable<Wypozyczenie> lista2 = wcRepository.WypozyczonePoCzytelniku(karta);
            var lista = lista1.Cast<Wypozyczenie>().Concat(lista2.Cast<Wypozyczenie>());

            this.WypozyczeniaCzytelnika = lista;

            foreach (var obj in lista)
            {
                DataGridViewRow row = this.wypozyczeniaGridView.RowTemplate.Clone() as DataGridViewRow;
                row.CreateCells(this.wypozyczeniaGridView);

                row.Cells[0].Value = obj.Tytul();
                row.Cells[1].Value = obj.Sygnatura();
                row.Cells[2].Value = obj.DataWypozyczenia.ToShortDateString();
                row.Cells[3].Value = obj.TerminZwrotu.ToShortDateString();

                this.wypozyczeniaGridView.Rows.Add(row);
            }

            this.wypozyczeniaGridView.DoubleBuffered(true); //Poprawienie płynności przewijania listy
        }

        private void SzczegolyKsiazkiButtonClick(object sender, EventArgs e)
        {
            Ksiazka selected = (Ksiazka)this.ksiazkiGridView.CurrentRow.DataBoundItem; //Zaznaczona książka
            List<EgzemplarzKsiazki> egzemplarze = this.ekRepository.PoKsiazce(selected); //Pobieramy jej egzemplarze

            SzczegolyKsiazki szczegolyKsiazki = new SzczegolyKsiazki();
            szczegolyKsiazki.PobierzDaneKsiazki(selected, egzemplarze);

            if (szczegolyKsiazki.ShowDialog() == DialogResult.OK)
            {
                wkRepository.PobierzDane();

                LoadWypozyczeniaGridView();
            }

            szczegolyKsiazki.TopMost = true;
        }

        private void SzukajKsiazekButtonClick(object sender, EventArgs e)
        {
            string isbn = this.isbnKsiazkiTextBox.Text;
            string tytul = this.tytulKsiazkiTextBox.Text;
            string autor = this.autorKsiazkiTextBox.Text;
            int? rok = ParseNInt(this.rokKsiazkiTextBox.Text);
            string wydawnictwo = this.wydawnictwoKsiazkiTextBox.Text;

            Kategoria kategoria = (Kategoria)this.kategoriaComboBox.SelectedItem;

            List<Ksiazka> lista = this.ksiazkaRepository.Wyszukaj(isbn, tytul, autor, rok, wydawnictwo, kategoria);

            this.ksiazkiGridView.DataSource = lista;
        }

        private void ResetujKsiazkiButtonClick(object sender, EventArgs e)
        {
            this.tytulKsiazkiTextBox.Text = "";
            this.isbnKsiazkiTextBox.Text = "";
            this.autorKsiazkiTextBox.Text = "";
            this.rokKsiazkiTextBox.Text = "";
            this.wydawnictwoKsiazkiTextBox.Text = "";

            this.kategoriaComboBox.SelectedValue = -1;

            LoadKsiazkiGridView();
        }

        private void SzukajCzasopismButtonClick(object sender, EventArgs e)
        {
            string issn = this.issnCzasopismaTextBex.Text;
            string tytul = this.tytulCzasopismaTextBox.Text;
            string wydawnictwo = "";

            Tematyka tematyka = (Tematyka)this.tematykaComboBox.SelectedItem;
            Czestotliwosc czestotliwosc = (Czestotliwosc)this.czestotliwoscComboBox.SelectedItem;
            Odbiorca odbiorca = (Odbiorca)this.odbiorcaComboBox.SelectedItem;

            List<Czasopismo> lista = this.czasopismoRepository.Wyszukaj(issn, tytul, dateTimePicker1.Value,
                dateTimePicker2.Value, wydawnictwo, tematyka, czestotliwosc, odbiorca);

            this.czasopismaGridView.DataSource = lista;
        }

        private void ResetujCzasopismaButtonClick(object sender, EventArgs e)
        {
            this.tytulCzasopismaTextBox.Text = "";
            this.issnCzasopismaTextBex.Text = "";

            dateTimePicker1.Value = new DateTime(1900, 1, 1);
            dateTimePicker2.Value = DateTime.Now;

            this.tematykaComboBox.SelectedValue = -1;
            this.czestotliwoscComboBox.SelectedValue = -1;
            this.odbiorcaComboBox.SelectedValue = -1;

            LoadCzasopismaGridView();
        }

        private void SzczegolyCzasopismaButtonClick(object sender, EventArgs e)
        {
            Czasopismo selected = (Czasopismo)this.czasopismaGridView.CurrentRow.DataBoundItem; //Zaznaczone czasopismo
            List<EgzemplarzCzasopisma> egzemplarze = this.ecRepository.PoCzasopismie(selected); //Pobieramy jego egzemplarze

            SzczegolyCzasopisma szczegolyCzasopisma = new SzczegolyCzasopisma();
            szczegolyCzasopisma.PobierzDaneCzasopisma(selected, egzemplarze);

            if (szczegolyCzasopisma.ShowDialog() == DialogResult.OK)
            {
                wcRepository.PobierzDane();

                LoadWypozyczeniaGridView();
            }

            szczegolyCzasopisma.TopMost = true;
        }

        private void WylogujMenuItemClick(object sender, EventArgs e)
        {
            Application.Exit();

            ProcessStartInfo startInfo = new ProcessStartInfo("Biblioteka.exe");
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = true;
            Process.Start(startInfo);
        }

        private void ZakonczMenuItemClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private int? ParseNInt(string val)
        {
            int i;
            return int.TryParse(val, out i) ? (int?)i : null;
        }

        private void ZmianaHaslaMenuItemClick(object sender, EventArgs e)
        {
            ZmianaHasla zmianaHasla = new ZmianaHasla(karta.Czytelnik);

            if (zmianaHasla.ShowDialog() == DialogResult.OK)
            {
                this.toolStripStatusLabel2.Text = String.Format("Zmieniono hasło");
            }
        }
    }
}
