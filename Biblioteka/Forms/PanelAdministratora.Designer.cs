﻿namespace Biblioteka.Forms
{
    partial class PanelAdministratora
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.wylogujMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zakończMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.edytujKsiazkeButton = new System.Windows.Forms.Button();
            this.edytujCzytelnikaButton = new System.Windows.Forms.Button();
            this.edytujCzasopismoButton = new System.Windows.Forms.Button();
            this.wypozyczenieButton = new System.Windows.Forms.Button();
            this.szukajLabel = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.resetujWypozyczeniaButton = new System.Windows.Forms.Button();
            this.szukajWypozyczenButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.wypozyczeniaGridView = new System.Windows.Forms.DataGridView();
            this.daneColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tytulWypozyczeniaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sygnaturaWypozyczeniaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataWypozyczeniaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.terminZwrWypozyczeniaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idCzytelnikaWypColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dodajKsiazkeButton = new System.Windows.Forms.Button();
            this.resetujKsiazkiButton = new System.Windows.Forms.Button();
            this.szukajKsiazekButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.tytulKsiazkiTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.isbnKsiazkiTextBox = new System.Windows.Forms.TextBox();
            this.autorKsiazkiTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.wydawnictwoKsiazkiTextBox = new System.Windows.Forms.TextBox();
            this.rokKsiazkiTextBox = new System.Windows.Forms.TextBox();
            this.kategoriaComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ksiazkiGridView = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.resetujCzytelnikowButton = new System.Windows.Forms.Button();
            this.szukajCzytelnikowButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.imieLabel = new System.Windows.Forms.Label();
            this.imieTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.statusComboBox = new System.Windows.Forms.ComboBox();
            this.dodajCzytelnikaButton = new System.Windows.Forms.Button();
            this.czytelnicyGridView = new System.Windows.Forms.DataGridView();
            this.IdCzytelnikaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImieCzytelnikaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazwiskoCzytelnikaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusKartyColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.liczbaWypozyczenColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.resetujCzasopismaButton = new System.Windows.Forms.Button();
            this.szukajCzasopismButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.tytulCzasopismaTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.issnCzasopismaTextBex = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.wydawnictwoCzasopismaTextBox = new System.Windows.Forms.TextBox();
            this.tematykaComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.odbiorcaComboBox = new System.Windows.Forms.ComboBox();
            this.czestotliwoscComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.dodajCzasopismoButton = new System.Windows.Forms.Button();
            this.czasopismaGridView = new System.Windows.Forms.DataGridView();
            this.IdCzasopismaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TytulCzasopismaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IssnCzasopismaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nrCzasopismaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wydawnictwoCzasopismaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label21 = new System.Windows.Forms.Label();
            this.calkowitaLiczbaWypozyczenLabel = new System.Windows.Forms.Label();
            this.calkowitaLiczbaWypLabel = new System.Windows.Forms.Label();
            this.udzialOsobNieaktywnychLabel = new System.Windows.Forms.Label();
            this.Resetuj2Button = new System.Windows.Forms.Button();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.zbioryBiblioteczneLabel = new System.Windows.Forms.Label();
            this.dataKoncowaLabel = new System.Windows.Forms.Label();
            this.rodzajZbioruComboBox = new System.Windows.Forms.ComboBox();
            this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker6 = new System.Windows.Forms.DateTimePicker();
            this.dataPoczatkowaLabel = new System.Windows.Forms.Label();
            this.UdzialNieaktywnychButton = new System.Windows.Forms.Button();
            this.udzialNieaktywnychLabel = new System.Windows.Forms.Label();
            this.najczesciejWybieraneGridView = new System.Windows.Forms.DataGridView();
            this.PozycjaWRankinguColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NazwaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LiczbaWypozyczenCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resetujNajczesciejWybieraneButton = new System.Windows.Forms.Button();
            this.wyswietlNajczesciejWybieraneButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.dateTimePicker8 = new System.Windows.Forms.DateTimePicker();
            this.liczbaMiejscWRankinguComboBox = new System.Windows.Forms.ComboBox();
            this.dateTimePicker7 = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.najczesciejWybieraneComboBox = new System.Windows.Forms.ComboBox();
            this.odbiorcaComboBox2 = new System.Windows.Forms.ComboBox();
            this.dataKoncowa2Label = new System.Windows.Forms.Label();
            this.grupaWiekowaComboBox = new System.Windows.Forms.ComboBox();
            this.odbiorcaLabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.dataPoczatkowa2Label = new System.Windows.Forms.Label();
            this.plecComboBox = new System.Windows.Forms.ComboBox();
            this.czestotliwoscUkazywaniaSieComboBox = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.czestotliwoscUkazywaniaSieLabel = new System.Windows.Forms.Label();
            this.IdKsiazkiColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TytulKsiazkiColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsbnKsiazkiColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AutorKsiazkiColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WydawnictwoKsiazkiColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rokWydaniaKsiazkiColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wypozyczeniaGridView)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ksiazkiGridView)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.czytelnicyGridView)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.czasopismaGridView)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.najczesciejWybieraneGridView)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wylogujMenuItem,
            this.zakończMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip.TabIndex = 4;
            this.menuStrip.Text = "menuStrip1";
            // 
            // wylogujMenuItem
            // 
            this.wylogujMenuItem.Name = "wylogujMenuItem";
            this.wylogujMenuItem.Size = new System.Drawing.Size(63, 20);
            this.wylogujMenuItem.Text = "Wyloguj";
            this.wylogujMenuItem.Click += new System.EventHandler(this.WylogujMenuItemClick);
            // 
            // zakończMenuItem
            // 
            this.zakończMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.zakończMenuItem.Name = "zakończMenuItem";
            this.zakończMenuItem.Size = new System.Drawing.Size(63, 20);
            this.zakończMenuItem.Text = "Zakończ";
            this.zakończMenuItem.Click += new System.EventHandler(this.ZakończMenuItemClick);
            // 
            // edytujKsiazkeButton
            // 
            this.edytujKsiazkeButton.Location = new System.Drawing.Point(901, 448);
            this.edytujKsiazkeButton.Name = "edytujKsiazkeButton";
            this.edytujKsiazkeButton.Size = new System.Drawing.Size(75, 25);
            this.edytujKsiazkeButton.TabIndex = 1;
            this.edytujKsiazkeButton.Text = "Edytuj";
            this.toolTip1.SetToolTip(this.edytujKsiazkeButton, "Kliknij aby edytować wybraną książkę");
            this.edytujKsiazkeButton.UseVisualStyleBackColor = true;
            this.edytujKsiazkeButton.Click += new System.EventHandler(this.EdytujKsiazkeButtonClick);
            // 
            // edytujCzytelnikaButton
            // 
            this.edytujCzytelnikaButton.Location = new System.Drawing.Point(901, 448);
            this.edytujCzytelnikaButton.Name = "edytujCzytelnikaButton";
            this.edytujCzytelnikaButton.Size = new System.Drawing.Size(75, 25);
            this.edytujCzytelnikaButton.TabIndex = 6;
            this.edytujCzytelnikaButton.Text = "Edytuj";
            this.toolTip1.SetToolTip(this.edytujCzytelnikaButton, "Kliknij aby edytować wybranego czytelnika");
            this.edytujCzytelnikaButton.UseVisualStyleBackColor = true;
            this.edytujCzytelnikaButton.Click += new System.EventHandler(this.EdycjaCzytelnikaButtonClick);
            // 
            // edytujCzasopismoButton
            // 
            this.edytujCzasopismoButton.Location = new System.Drawing.Point(901, 448);
            this.edytujCzasopismoButton.Name = "edytujCzasopismoButton";
            this.edytujCzasopismoButton.Size = new System.Drawing.Size(75, 25);
            this.edytujCzasopismoButton.TabIndex = 1;
            this.edytujCzasopismoButton.Text = "Edytuj";
            this.toolTip1.SetToolTip(this.edytujCzasopismoButton, "Kliknij aby edytować wybrane czasopismo");
            this.edytujCzasopismoButton.UseVisualStyleBackColor = true;
            this.edytujCzasopismoButton.Click += new System.EventHandler(this.EdytujCzasopismoButtonClick);
            // 
            // wypozyczenieButton
            // 
            this.wypozyczenieButton.Location = new System.Drawing.Point(901, 448);
            this.wypozyczenieButton.Name = "wypozyczenieButton";
            this.wypozyczenieButton.Size = new System.Drawing.Size(75, 25);
            this.wypozyczenieButton.TabIndex = 16;
            this.wypozyczenieButton.Text = "Szczegóły";
            this.toolTip1.SetToolTip(this.wypozyczenieButton, "Kliknij aby edytować wybrane czasopismo");
            this.wypozyczenieButton.UseVisualStyleBackColor = true;
            this.wypozyczenieButton.Click += new System.EventHandler(this.WypozyczenieButtonClick);
            // 
            // szukajLabel
            // 
            this.szukajLabel.AutoSize = true;
            this.szukajLabel.Location = new System.Drawing.Point(3, 255);
            this.szukajLabel.Name = "szukajLabel";
            this.szukajLabel.Size = new System.Drawing.Size(42, 13);
            this.szukajLabel.TabIndex = 2;
            this.szukajLabel.Text = "Szukaj:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(54, 252);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(160, 20);
            this.textBox1.TabIndex = 7;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 644);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.Stretch = false;
            this.statusStrip.TabIndex = 5;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(112, 17);
            this.toolStripStatusLabel.Text = "toolStripStatusLabel";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.wypozyczenieButton);
            this.tabPage4.Controls.Add(this.resetujWypozyczeniaButton);
            this.tabPage4.Controls.Add(this.szukajWypozyczenButton);
            this.tabPage4.Controls.Add(this.tableLayoutPanel5);
            this.tabPage4.Controls.Add(this.wypozyczeniaGridView);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(978, 583);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Wypożyczenia";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // resetujWypozyczeniaButton
            // 
            this.resetujWypozyczeniaButton.Location = new System.Drawing.Point(329, 479);
            this.resetujWypozyczeniaButton.Name = "resetujWypozyczeniaButton";
            this.resetujWypozyczeniaButton.Size = new System.Drawing.Size(75, 25);
            this.resetujWypozyczeniaButton.TabIndex = 15;
            this.resetujWypozyczeniaButton.Text = "Resetuj";
            this.resetujWypozyczeniaButton.UseVisualStyleBackColor = true;
            this.resetujWypozyczeniaButton.Click += new System.EventHandler(this.ResetujWypozyczeniaButtonClick);
            // 
            // szukajWypozyczenButton
            // 
            this.szukajWypozyczenButton.Location = new System.Drawing.Point(329, 448);
            this.szukajWypozyczenButton.Name = "szukajWypozyczenButton";
            this.szukajWypozyczenButton.Size = new System.Drawing.Size(75, 25);
            this.szukajWypozyczenButton.TabIndex = 14;
            this.szukajWypozyczenButton.Text = "Szukaj";
            this.szukajWypozyczenButton.UseVisualStyleBackColor = true;
            this.szukajWypozyczenButton.Click += new System.EventHandler(this.SzukajWypozyczenButtonClick);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.AutoSize = true;
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.Controls.Add(this.dateTimePicker3, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.radioButton1, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label16, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.radioButton2, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.dateTimePicker4, 1, 1);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 447);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.Size = new System.Drawing.Size(320, 59);
            this.tableLayoutPanel5.TabIndex = 13;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker3.Location = new System.Drawing.Point(103, 3);
            this.dateTimePicker3.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(174, 20);
            this.dateTimePicker3.TabIndex = 16;
            // 
            // radioButton1
            // 
            this.radioButton1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(293, 8);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(14, 13);
            this.radioButton1.TabIndex = 16;
            this.radioButton1.TabStop = true;
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.RadioButton1CheckedChanged);
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 8);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 13);
            this.label15.TabIndex = 7;
            this.label15.Text = "Data wypożycz.";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 13);
            this.label16.TabIndex = 10;
            this.label16.Text = "Termin zwrotu";
            // 
            // radioButton2
            // 
            this.radioButton2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(293, 37);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(14, 13);
            this.radioButton2.TabIndex = 17;
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.RadioButton2CheckedChanged);
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Enabled = false;
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker4.Location = new System.Drawing.Point(103, 32);
            this.dateTimePicker4.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(174, 20);
            this.dateTimePicker4.TabIndex = 18;
            // 
            // wypozyczeniaGridView
            // 
            this.wypozyczeniaGridView.AllowUserToAddRows = false;
            this.wypozyczeniaGridView.AllowUserToDeleteRows = false;
            this.wypozyczeniaGridView.AllowUserToResizeColumns = false;
            this.wypozyczeniaGridView.AllowUserToResizeRows = false;
            this.wypozyczeniaGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.wypozyczeniaGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.wypozyczeniaGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.wypozyczeniaGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.daneColumn,
            this.tytulWypozyczeniaColumn,
            this.sygnaturaWypozyczeniaColumn,
            this.dataWypozyczeniaColumn,
            this.terminZwrWypozyczeniaColumn,
            this.idCzytelnikaWypColumn});
            this.wypozyczeniaGridView.Location = new System.Drawing.Point(0, 3);
            this.wypozyczeniaGridView.MultiSelect = false;
            this.wypozyczeniaGridView.Name = "wypozyczeniaGridView";
            this.wypozyczeniaGridView.ReadOnly = true;
            this.wypozyczeniaGridView.RowHeadersVisible = false;
            this.wypozyczeniaGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.wypozyczeniaGridView.Size = new System.Drawing.Size(975, 442);
            this.wypozyczeniaGridView.TabIndex = 0;
            // 
            // daneColumn
            // 
            this.daneColumn.DataPropertyName = "Wypozyczenia";
            this.daneColumn.HeaderText = "Czytelnik";
            this.daneColumn.Name = "daneColumn";
            this.daneColumn.ReadOnly = true;
            this.daneColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tytulWypozyczeniaColumn
            // 
            this.tytulWypozyczeniaColumn.HeaderText = "Tytuł";
            this.tytulWypozyczeniaColumn.Name = "tytulWypozyczeniaColumn";
            this.tytulWypozyczeniaColumn.ReadOnly = true;
            this.tytulWypozyczeniaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // sygnaturaWypozyczeniaColumn
            // 
            this.sygnaturaWypozyczeniaColumn.HeaderText = "Sygnatura";
            this.sygnaturaWypozyczeniaColumn.Name = "sygnaturaWypozyczeniaColumn";
            this.sygnaturaWypozyczeniaColumn.ReadOnly = true;
            this.sygnaturaWypozyczeniaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataWypozyczeniaColumn
            // 
            this.dataWypozyczeniaColumn.HeaderText = "Data wypożyczenia";
            this.dataWypozyczeniaColumn.Name = "dataWypozyczeniaColumn";
            this.dataWypozyczeniaColumn.ReadOnly = true;
            this.dataWypozyczeniaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // terminZwrWypozyczeniaColumn
            // 
            this.terminZwrWypozyczeniaColumn.HeaderText = "Termin zwrotu";
            this.terminZwrWypozyczeniaColumn.Name = "terminZwrWypozyczeniaColumn";
            this.terminZwrWypozyczeniaColumn.ReadOnly = true;
            this.terminZwrWypozyczeniaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // idCzytelnikaWypColumn
            // 
            this.idCzytelnikaWypColumn.HeaderText = "Id";
            this.idCzytelnikaWypColumn.Name = "idCzytelnikaWypColumn";
            this.idCzytelnikaWypColumn.ReadOnly = true;
            this.idCzytelnikaWypColumn.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dodajKsiazkeButton);
            this.tabPage2.Controls.Add(this.resetujKsiazkiButton);
            this.tabPage2.Controls.Add(this.szukajKsiazekButton);
            this.tabPage2.Controls.Add(this.tableLayoutPanel3);
            this.tabPage2.Controls.Add(this.edytujKsiazkeButton);
            this.tabPage2.Controls.Add(this.ksiazkiGridView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(978, 583);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Książki";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dodajKsiazkeButton
            // 
            this.dodajKsiazkeButton.Location = new System.Drawing.Point(901, 479);
            this.dodajKsiazkeButton.Name = "dodajKsiazkeButton";
            this.dodajKsiazkeButton.Size = new System.Drawing.Size(75, 25);
            this.dodajKsiazkeButton.TabIndex = 15;
            this.dodajKsiazkeButton.Text = "Dodaj";
            this.dodajKsiazkeButton.UseVisualStyleBackColor = true;
            this.dodajKsiazkeButton.Click += new System.EventHandler(this.DodajKsiazkeButtonClick);
            // 
            // resetujKsiazkiButton
            // 
            this.resetujKsiazkiButton.Location = new System.Drawing.Point(669, 479);
            this.resetujKsiazkiButton.Name = "resetujKsiazkiButton";
            this.resetujKsiazkiButton.Size = new System.Drawing.Size(75, 25);
            this.resetujKsiazkiButton.TabIndex = 14;
            this.resetujKsiazkiButton.Text = "Resetuj";
            this.resetujKsiazkiButton.UseVisualStyleBackColor = true;
            this.resetujKsiazkiButton.Click += new System.EventHandler(this.ResetujKsiazkiButtonClick);
            // 
            // szukajKsiazekButton
            // 
            this.szukajKsiazekButton.Location = new System.Drawing.Point(669, 448);
            this.szukajKsiazekButton.Name = "szukajKsiazekButton";
            this.szukajKsiazekButton.Size = new System.Drawing.Size(75, 25);
            this.szukajKsiazekButton.TabIndex = 13;
            this.szukajKsiazekButton.Text = "Szukaj";
            this.szukajKsiazekButton.UseVisualStyleBackColor = true;
            this.szukajKsiazekButton.Click += new System.EventHandler(this.SzukajKsiazekButtonClick);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tytulKsiazkiTextBox, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.isbnKsiazkiTextBox, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.autorKsiazkiTextBox, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label11, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.wydawnictwoKsiazkiTextBox, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.rokKsiazkiTextBox, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.kategoriaComboBox, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label5, 3, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 447);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(660, 113);
            this.tableLayoutPanel3.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Tytuł";
            // 
            // tytulKsiazkiTextBox
            // 
            this.tytulKsiazkiTextBox.Location = new System.Drawing.Point(103, 3);
            this.tytulKsiazkiTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.tytulKsiazkiTextBox.Name = "tytulKsiazkiTextBox";
            this.tytulKsiazkiTextBox.Size = new System.Drawing.Size(209, 20);
            this.tytulKsiazkiTextBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "ISBN";
            // 
            // isbnKsiazkiTextBox
            // 
            this.isbnKsiazkiTextBox.Location = new System.Drawing.Point(103, 32);
            this.isbnKsiazkiTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.isbnKsiazkiTextBox.Name = "isbnKsiazkiTextBox";
            this.isbnKsiazkiTextBox.Size = new System.Drawing.Size(209, 20);
            this.isbnKsiazkiTextBox.TabIndex = 11;
            // 
            // autorKsiazkiTextBox
            // 
            this.autorKsiazkiTextBox.Location = new System.Drawing.Point(103, 61);
            this.autorKsiazkiTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.autorKsiazkiTextBox.Name = "autorKsiazkiTextBox";
            this.autorKsiazkiTextBox.Size = new System.Drawing.Size(209, 20);
            this.autorKsiazkiTextBox.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Autor";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(343, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Wydawnictwo";
            // 
            // wydawnictwoKsiazkiTextBox
            // 
            this.wydawnictwoKsiazkiTextBox.Location = new System.Drawing.Point(443, 3);
            this.wydawnictwoKsiazkiTextBox.Name = "wydawnictwoKsiazkiTextBox";
            this.wydawnictwoKsiazkiTextBox.Size = new System.Drawing.Size(209, 20);
            this.wydawnictwoKsiazkiTextBox.TabIndex = 16;
            // 
            // rokKsiazkiTextBox
            // 
            this.rokKsiazkiTextBox.Location = new System.Drawing.Point(103, 90);
            this.rokKsiazkiTextBox.Name = "rokKsiazkiTextBox";
            this.rokKsiazkiTextBox.Size = new System.Drawing.Size(209, 20);
            this.rokKsiazkiTextBox.TabIndex = 17;
            // 
            // kategoriaComboBox
            // 
            this.kategoriaComboBox.DisplayMember = "Nazwa";
            this.kategoriaComboBox.FormattingEnabled = true;
            this.kategoriaComboBox.Location = new System.Drawing.Point(443, 32);
            this.kategoriaComboBox.Name = "kategoriaComboBox";
            this.kategoriaComboBox.Size = new System.Drawing.Size(209, 21);
            this.kategoriaComboBox.TabIndex = 8;
            this.kategoriaComboBox.ValueMember = "Id";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "Rok wydania";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(343, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Kategoria";
            // 
            // ksiazkiGridView
            // 
            this.ksiazkiGridView.AllowUserToAddRows = false;
            this.ksiazkiGridView.AllowUserToDeleteRows = false;
            this.ksiazkiGridView.AllowUserToResizeColumns = false;
            this.ksiazkiGridView.AllowUserToResizeRows = false;
            this.ksiazkiGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ksiazkiGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ksiazkiGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ksiazkiGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdKsiazkiColumn,
            this.TytulKsiazkiColumn,
            this.IsbnKsiazkiColumn,
            this.AutorKsiazkiColumn,
            this.WydawnictwoKsiazkiColumn,
            this.rokWydaniaKsiazkiColumn});
            this.ksiazkiGridView.Location = new System.Drawing.Point(0, 3);
            this.ksiazkiGridView.MultiSelect = false;
            this.ksiazkiGridView.Name = "ksiazkiGridView";
            this.ksiazkiGridView.ReadOnly = true;
            this.ksiazkiGridView.RowHeadersVisible = false;
            this.ksiazkiGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ksiazkiGridView.Size = new System.Drawing.Size(975, 442);
            this.ksiazkiGridView.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.resetujCzytelnikowButton);
            this.tabPage1.Controls.Add(this.szukajCzytelnikowButton);
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Controls.Add(this.dodajCzytelnikaButton);
            this.tabPage1.Controls.Add(this.edytujCzytelnikaButton);
            this.tabPage1.Controls.Add(this.czytelnicyGridView);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(978, 583);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Czytelnicy";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // resetujCzytelnikowButton
            // 
            this.resetujCzytelnikowButton.Location = new System.Drawing.Point(329, 479);
            this.resetujCzytelnikowButton.Name = "resetujCzytelnikowButton";
            this.resetujCzytelnikowButton.Size = new System.Drawing.Size(75, 25);
            this.resetujCzytelnikowButton.TabIndex = 12;
            this.resetujCzytelnikowButton.Text = "Resetuj";
            this.resetujCzytelnikowButton.UseVisualStyleBackColor = true;
            this.resetujCzytelnikowButton.Click += new System.EventHandler(this.ResetujCzytelnikowButtonClick);
            // 
            // szukajCzytelnikowButton
            // 
            this.szukajCzytelnikowButton.Location = new System.Drawing.Point(329, 448);
            this.szukajCzytelnikowButton.Name = "szukajCzytelnikowButton";
            this.szukajCzytelnikowButton.Size = new System.Drawing.Size(75, 25);
            this.szukajCzytelnikowButton.TabIndex = 11;
            this.szukajCzytelnikowButton.Text = "Szukaj";
            this.szukajCzytelnikowButton.UseVisualStyleBackColor = true;
            this.szukajCzytelnikowButton.Click += new System.EventHandler(this.SzukajCzytelnikowButtonClick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel1.Controls.Add(this.imieLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.imieTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.statusComboBox, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 447);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(320, 59);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // imieLabel
            // 
            this.imieLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.imieLabel.AutoSize = true;
            this.imieLabel.Location = new System.Drawing.Point(3, 8);
            this.imieLabel.Name = "imieLabel";
            this.imieLabel.Size = new System.Drawing.Size(26, 13);
            this.imieLabel.TabIndex = 7;
            this.imieLabel.Text = "Imię";
            // 
            // imieTextBox
            // 
            this.imieTextBox.Location = new System.Drawing.Point(103, 3);
            this.imieTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.imieTextBox.Name = "imieTextBox";
            this.imieTextBox.Size = new System.Drawing.Size(209, 20);
            this.imieTextBox.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Status karty";
            // 
            // statusComboBox
            // 
            this.statusComboBox.FormattingEnabled = true;
            this.statusComboBox.Items.AddRange(new object[] {
            "aktywna",
            "nieaktywna"});
            this.statusComboBox.Location = new System.Drawing.Point(103, 32);
            this.statusComboBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.statusComboBox.Name = "statusComboBox";
            this.statusComboBox.Size = new System.Drawing.Size(209, 21);
            this.statusComboBox.TabIndex = 11;
            // 
            // dodajCzytelnikaButton
            // 
            this.dodajCzytelnikaButton.Location = new System.Drawing.Point(901, 479);
            this.dodajCzytelnikaButton.Name = "dodajCzytelnikaButton";
            this.dodajCzytelnikaButton.Size = new System.Drawing.Size(75, 25);
            this.dodajCzytelnikaButton.TabIndex = 7;
            this.dodajCzytelnikaButton.Text = "Dodaj";
            this.dodajCzytelnikaButton.UseVisualStyleBackColor = true;
            this.dodajCzytelnikaButton.Click += new System.EventHandler(this.DodajCzytelnikaButtonClick);
            // 
            // czytelnicyGridView
            // 
            this.czytelnicyGridView.AllowUserToAddRows = false;
            this.czytelnicyGridView.AllowUserToDeleteRows = false;
            this.czytelnicyGridView.AllowUserToResizeColumns = false;
            this.czytelnicyGridView.AllowUserToResizeRows = false;
            this.czytelnicyGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.czytelnicyGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.czytelnicyGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.czytelnicyGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdCzytelnikaColumn,
            this.ImieCzytelnikaColumn,
            this.NazwiskoCzytelnikaColumn,
            this.statusKartyColumn,
            this.liczbaWypozyczenColumn});
            this.czytelnicyGridView.Location = new System.Drawing.Point(0, 3);
            this.czytelnicyGridView.MultiSelect = false;
            this.czytelnicyGridView.Name = "czytelnicyGridView";
            this.czytelnicyGridView.ReadOnly = true;
            this.czytelnicyGridView.RowHeadersVisible = false;
            this.czytelnicyGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.czytelnicyGridView.Size = new System.Drawing.Size(975, 442);
            this.czytelnicyGridView.TabIndex = 0;
            // 
            // IdCzytelnikaColumn
            // 
            this.IdCzytelnikaColumn.DataPropertyName = "Id";
            this.IdCzytelnikaColumn.FillWeight = 20.655F;
            this.IdCzytelnikaColumn.HeaderText = "#";
            this.IdCzytelnikaColumn.Name = "IdCzytelnikaColumn";
            this.IdCzytelnikaColumn.ReadOnly = true;
            this.IdCzytelnikaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ImieCzytelnikaColumn
            // 
            this.ImieCzytelnikaColumn.DataPropertyName = "Imie";
            this.ImieCzytelnikaColumn.FillWeight = 134.7716F;
            this.ImieCzytelnikaColumn.HeaderText = "Imię";
            this.ImieCzytelnikaColumn.Name = "ImieCzytelnikaColumn";
            this.ImieCzytelnikaColumn.ReadOnly = true;
            this.ImieCzytelnikaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NazwiskoCzytelnikaColumn
            // 
            this.NazwiskoCzytelnikaColumn.DataPropertyName = "Nazwisko";
            this.NazwiskoCzytelnikaColumn.FillWeight = 134.7716F;
            this.NazwiskoCzytelnikaColumn.HeaderText = "Nazwisko";
            this.NazwiskoCzytelnikaColumn.Name = "NazwiskoCzytelnikaColumn";
            this.NazwiskoCzytelnikaColumn.ReadOnly = true;
            this.NazwiskoCzytelnikaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // statusKartyColumn
            // 
            this.statusKartyColumn.HeaderText = "Status karty";
            this.statusKartyColumn.Name = "statusKartyColumn";
            this.statusKartyColumn.ReadOnly = true;
            this.statusKartyColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // liczbaWypozyczenColumn
            // 
            this.liczbaWypozyczenColumn.HeaderText = "Liczba wypożyczeń";
            this.liczbaWypozyczenColumn.Name = "liczbaWypozyczenColumn";
            this.liczbaWypozyczenColumn.ReadOnly = true;
            this.liczbaWypozyczenColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 27);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(986, 609);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.resetujCzasopismaButton);
            this.tabPage3.Controls.Add(this.szukajCzasopismButton);
            this.tabPage3.Controls.Add(this.tableLayoutPanel4);
            this.tabPage3.Controls.Add(this.dodajCzasopismoButton);
            this.tabPage3.Controls.Add(this.edytujCzasopismoButton);
            this.tabPage3.Controls.Add(this.czasopismaGridView);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(978, 583);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Czasopisma";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // resetujCzasopismaButton
            // 
            this.resetujCzasopismaButton.Location = new System.Drawing.Point(669, 479);
            this.resetujCzasopismaButton.Name = "resetujCzasopismaButton";
            this.resetujCzasopismaButton.Size = new System.Drawing.Size(75, 25);
            this.resetujCzasopismaButton.TabIndex = 17;
            this.resetujCzasopismaButton.Text = "Resetuj";
            this.resetujCzasopismaButton.UseVisualStyleBackColor = true;
            this.resetujCzasopismaButton.Click += new System.EventHandler(this.ResetujCzasopismaButtonClick);
            // 
            // szukajCzasopismButton
            // 
            this.szukajCzasopismButton.Location = new System.Drawing.Point(669, 448);
            this.szukajCzasopismButton.Name = "szukajCzasopismButton";
            this.szukajCzasopismButton.Size = new System.Drawing.Size(75, 25);
            this.szukajCzasopismButton.TabIndex = 16;
            this.szukajCzasopismButton.Text = "Szukaj";
            this.szukajCzasopismButton.UseVisualStyleBackColor = true;
            this.szukajCzasopismButton.Click += new System.EventHandler(this.SzukajCzasopismButtonClick);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.AutoSize = true;
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel4.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tytulCzasopismaTextBox, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.issnCzasopismaTextBex, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label7, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.wydawnictwoCzasopismaTextBox, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.tematykaComboBox, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.label13, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel2, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.odbiorcaComboBox, 4, 2);
            this.tableLayoutPanel4.Controls.Add(this.czestotliwoscComboBox, 4, 1);
            this.tableLayoutPanel4.Controls.Add(this.label9, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.label14, 3, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 448);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(660, 113);
            this.tableLayoutPanel4.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Tytuł";
            // 
            // tytulCzasopismaTextBox
            // 
            this.tytulCzasopismaTextBox.Location = new System.Drawing.Point(103, 3);
            this.tytulCzasopismaTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.tytulCzasopismaTextBox.Name = "tytulCzasopismaTextBox";
            this.tytulCzasopismaTextBox.Size = new System.Drawing.Size(209, 20);
            this.tytulCzasopismaTextBox.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "ISSN";
            // 
            // issnCzasopismaTextBex
            // 
            this.issnCzasopismaTextBex.Location = new System.Drawing.Point(103, 33);
            this.issnCzasopismaTextBex.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.issnCzasopismaTextBex.Name = "issnCzasopismaTextBex";
            this.issnCzasopismaTextBex.Size = new System.Drawing.Size(209, 20);
            this.issnCzasopismaTextBex.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Wydawnictwo";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(343, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Tematyka";
            // 
            // wydawnictwoCzasopismaTextBox
            // 
            this.wydawnictwoCzasopismaTextBox.Location = new System.Drawing.Point(103, 62);
            this.wydawnictwoCzasopismaTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.wydawnictwoCzasopismaTextBox.Name = "wydawnictwoCzasopismaTextBox";
            this.wydawnictwoCzasopismaTextBox.Size = new System.Drawing.Size(209, 20);
            this.wydawnictwoCzasopismaTextBox.TabIndex = 13;
            // 
            // tematykaComboBox
            // 
            this.tematykaComboBox.DisplayMember = "Nazwa";
            this.tematykaComboBox.FormattingEnabled = true;
            this.tematykaComboBox.Location = new System.Drawing.Point(443, 3);
            this.tematykaComboBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.tematykaComboBox.Name = "tematykaComboBox";
            this.tematykaComboBox.Size = new System.Drawing.Size(209, 21);
            this.tematykaComboBox.TabIndex = 8;
            this.tematykaComboBox.ValueMember = "Id";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 94);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "Zakres dat";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.dateTimePicker1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dateTimePicker2, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(100, 88);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(215, 24);
            this.tableLayoutPanel2.TabIndex = 16;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(3, 3);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(102, 20);
            this.dateTimePicker1.TabIndex = 14;
            this.dateTimePicker1.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(111, 3);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(101, 20);
            this.dateTimePicker2.TabIndex = 15;
            // 
            // odbiorcaComboBox
            // 
            this.odbiorcaComboBox.DisplayMember = "Nazwa";
            this.odbiorcaComboBox.Location = new System.Drawing.Point(443, 62);
            this.odbiorcaComboBox.Name = "odbiorcaComboBox";
            this.odbiorcaComboBox.Size = new System.Drawing.Size(209, 21);
            this.odbiorcaComboBox.TabIndex = 0;
            this.odbiorcaComboBox.ValueMember = "Id";
            // 
            // czestotliwoscComboBox
            // 
            this.czestotliwoscComboBox.DisplayMember = "Nazwa";
            this.czestotliwoscComboBox.FormattingEnabled = true;
            this.czestotliwoscComboBox.Location = new System.Drawing.Point(443, 33);
            this.czestotliwoscComboBox.Name = "czestotliwoscComboBox";
            this.czestotliwoscComboBox.Size = new System.Drawing.Size(209, 21);
            this.czestotliwoscComboBox.TabIndex = 17;
            this.czestotliwoscComboBox.ValueMember = "Id";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.Location = new System.Drawing.Point(343, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Odbiorca";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(343, 38);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "Częstotliwość";
            // 
            // dodajCzasopismoButton
            // 
            this.dodajCzasopismoButton.Location = new System.Drawing.Point(901, 479);
            this.dodajCzasopismoButton.Name = "dodajCzasopismoButton";
            this.dodajCzasopismoButton.Size = new System.Drawing.Size(75, 25);
            this.dodajCzasopismoButton.TabIndex = 11;
            this.dodajCzasopismoButton.Text = "Dodaj";
            this.dodajCzasopismoButton.UseVisualStyleBackColor = true;
            this.dodajCzasopismoButton.Click += new System.EventHandler(this.DodajCzasopismoButtonClick);
            // 
            // czasopismaGridView
            // 
            this.czasopismaGridView.AllowUserToAddRows = false;
            this.czasopismaGridView.AllowUserToDeleteRows = false;
            this.czasopismaGridView.AllowUserToResizeColumns = false;
            this.czasopismaGridView.AllowUserToResizeRows = false;
            this.czasopismaGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.czasopismaGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.czasopismaGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.czasopismaGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdCzasopismaColumn,
            this.TytulCzasopismaColumn,
            this.IssnCzasopismaColumn,
            this.nrCzasopismaColumn,
            this.wydawnictwoCzasopismaColumn});
            this.czasopismaGridView.Location = new System.Drawing.Point(0, 3);
            this.czasopismaGridView.MultiSelect = false;
            this.czasopismaGridView.Name = "czasopismaGridView";
            this.czasopismaGridView.ReadOnly = true;
            this.czasopismaGridView.RowHeadersVisible = false;
            this.czasopismaGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.czasopismaGridView.Size = new System.Drawing.Size(975, 442);
            this.czasopismaGridView.TabIndex = 0;
            // 
            // IdCzasopismaColumn
            // 
            this.IdCzasopismaColumn.DataPropertyName = "Id";
            this.IdCzasopismaColumn.FillWeight = 21.30457F;
            this.IdCzasopismaColumn.HeaderText = "#";
            this.IdCzasopismaColumn.Name = "IdCzasopismaColumn";
            this.IdCzasopismaColumn.ReadOnly = true;
            this.IdCzasopismaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TytulCzasopismaColumn
            // 
            this.TytulCzasopismaColumn.DataPropertyName = "Tytul";
            this.TytulCzasopismaColumn.FillWeight = 179.6954F;
            this.TytulCzasopismaColumn.HeaderText = "Tytuł";
            this.TytulCzasopismaColumn.Name = "TytulCzasopismaColumn";
            this.TytulCzasopismaColumn.ReadOnly = true;
            this.TytulCzasopismaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // IssnCzasopismaColumn
            // 
            this.IssnCzasopismaColumn.DataPropertyName = "ISSN";
            this.IssnCzasopismaColumn.HeaderText = "ISSN";
            this.IssnCzasopismaColumn.Name = "IssnCzasopismaColumn";
            this.IssnCzasopismaColumn.ReadOnly = true;
            this.IssnCzasopismaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // nrCzasopismaColumn
            // 
            this.nrCzasopismaColumn.DataPropertyName = "nrCzasopisma";
            this.nrCzasopismaColumn.HeaderText = "Numer";
            this.nrCzasopismaColumn.Name = "nrCzasopismaColumn";
            this.nrCzasopismaColumn.ReadOnly = true;
            this.nrCzasopismaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // wydawnictwoCzasopismaColumn
            // 
            this.wydawnictwoCzasopismaColumn.DataPropertyName = "Wydawnictwo";
            this.wydawnictwoCzasopismaColumn.HeaderText = "Wydawnictwo";
            this.wydawnictwoCzasopismaColumn.Name = "wydawnictwoCzasopismaColumn";
            this.wydawnictwoCzasopismaColumn.ReadOnly = true;
            this.wydawnictwoCzasopismaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tableLayoutPanel8);
            this.tabPage5.Controls.Add(this.Resetuj2Button);
            this.tabPage5.Controls.Add(this.tableLayoutPanel7);
            this.tabPage5.Controls.Add(this.UdzialNieaktywnychButton);
            this.tabPage5.Controls.Add(this.udzialNieaktywnychLabel);
            this.tabPage5.Controls.Add(this.najczesciejWybieraneGridView);
            this.tabPage5.Controls.Add(this.resetujNajczesciejWybieraneButton);
            this.tabPage5.Controls.Add(this.wyswietlNajczesciejWybieraneButton);
            this.tabPage5.Controls.Add(this.tableLayoutPanel6);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(978, 583);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Statystyki";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel8.Controls.Add(this.label21, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.calkowitaLiczbaWypozyczenLabel, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.calkowitaLiczbaWypLabel, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.udzialOsobNieaktywnychLabel, 1, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(517, 412);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(378, 54);
            this.tableLayoutPanel8.TabIndex = 37;
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(302, 13);
            this.label21.TabIndex = 38;
            this.label21.Text = "Udział osób o nieaktywnej karcie w ogólnej liczbie czytelników";
            // 
            // calkowitaLiczbaWypozyczenLabel
            // 
            this.calkowitaLiczbaWypozyczenLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.calkowitaLiczbaWypozyczenLabel.AutoSize = true;
            this.calkowitaLiczbaWypozyczenLabel.Location = new System.Drawing.Point(3, 34);
            this.calkowitaLiczbaWypozyczenLabel.Name = "calkowitaLiczbaWypozyczenLabel";
            this.calkowitaLiczbaWypozyczenLabel.Size = new System.Drawing.Size(297, 13);
            this.calkowitaLiczbaWypozyczenLabel.TabIndex = 35;
            this.calkowitaLiczbaWypozyczenLabel.Text = "Całkowita liczba wypożyczeń w podanym zakresie czasowym";
            // 
            // calkowitaLiczbaWypLabel
            // 
            this.calkowitaLiczbaWypLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.calkowitaLiczbaWypLabel.AutoSize = true;
            this.calkowitaLiczbaWypLabel.Location = new System.Drawing.Point(334, 34);
            this.calkowitaLiczbaWypLabel.Name = "calkowitaLiczbaWypLabel";
            this.calkowitaLiczbaWypLabel.Size = new System.Drawing.Size(31, 13);
            this.calkowitaLiczbaWypLabel.TabIndex = 36;
            this.calkowitaLiczbaWypLabel.Text = "____";
            this.calkowitaLiczbaWypLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // udzialOsobNieaktywnychLabel
            // 
            this.udzialOsobNieaktywnychLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.udzialOsobNieaktywnychLabel.AutoSize = true;
            this.udzialOsobNieaktywnychLabel.Location = new System.Drawing.Point(334, 7);
            this.udzialOsobNieaktywnychLabel.Name = "udzialOsobNieaktywnychLabel";
            this.udzialOsobNieaktywnychLabel.Size = new System.Drawing.Size(31, 13);
            this.udzialOsobNieaktywnychLabel.TabIndex = 29;
            this.udzialOsobNieaktywnychLabel.Text = "____";
            this.udzialOsobNieaktywnychLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Resetuj2Button
            // 
            this.Resetuj2Button.Location = new System.Drawing.Point(901, 345);
            this.Resetuj2Button.Name = "Resetuj2Button";
            this.Resetuj2Button.Size = new System.Drawing.Size(75, 25);
            this.Resetuj2Button.TabIndex = 35;
            this.Resetuj2Button.Text = "Resetuj";
            this.Resetuj2Button.UseVisualStyleBackColor = true;
            this.Resetuj2Button.Click += new System.EventHandler(this.ResetujObliczeniaButtonClick);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.zbioryBiblioteczneLabel, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.dataKoncowaLabel, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.rodzajZbioruComboBox, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.dateTimePicker5, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.dateTimePicker6, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.dataPoczatkowaLabel, 0, 1);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(517, 314);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(378, 92);
            this.tableLayoutPanel7.TabIndex = 34;
            // 
            // zbioryBiblioteczneLabel
            // 
            this.zbioryBiblioteczneLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.zbioryBiblioteczneLabel.AutoSize = true;
            this.zbioryBiblioteczneLabel.Location = new System.Drawing.Point(3, 8);
            this.zbioryBiblioteczneLabel.Name = "zbioryBiblioteczneLabel";
            this.zbioryBiblioteczneLabel.Size = new System.Drawing.Size(95, 13);
            this.zbioryBiblioteczneLabel.TabIndex = 38;
            this.zbioryBiblioteczneLabel.Text = "Zbiory biblioteczne";
            // 
            // dataKoncowaLabel
            // 
            this.dataKoncowaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dataKoncowaLabel.AutoSize = true;
            this.dataKoncowaLabel.Location = new System.Drawing.Point(3, 69);
            this.dataKoncowaLabel.Name = "dataKoncowaLabel";
            this.dataKoncowaLabel.Size = new System.Drawing.Size(77, 13);
            this.dataKoncowaLabel.TabIndex = 37;
            this.dataKoncowaLabel.Text = "Data końcowa";
            // 
            // rodzajZbioruComboBox
            // 
            this.rodzajZbioruComboBox.FormattingEnabled = true;
            this.rodzajZbioruComboBox.Items.AddRange(new object[] {
            "książki",
            "czasopisma",
            "wszystkie"});
            this.rodzajZbioruComboBox.Location = new System.Drawing.Point(192, 3);
            this.rodzajZbioruComboBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.rodzajZbioruComboBox.Name = "rodzajZbioruComboBox";
            this.rodzajZbioruComboBox.Size = new System.Drawing.Size(183, 21);
            this.rodzajZbioruComboBox.TabIndex = 37;
            // 
            // dateTimePicker5
            // 
            this.dateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker5.Location = new System.Drawing.Point(192, 33);
            this.dateTimePicker5.Name = "dateTimePicker5";
            this.dateTimePicker5.Size = new System.Drawing.Size(183, 20);
            this.dateTimePicker5.TabIndex = 31;
            this.dateTimePicker5.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // dateTimePicker6
            // 
            this.dateTimePicker6.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker6.Location = new System.Drawing.Point(192, 63);
            this.dateTimePicker6.Name = "dateTimePicker6";
            this.dateTimePicker6.Size = new System.Drawing.Size(183, 20);
            this.dateTimePicker6.TabIndex = 32;
            // 
            // dataPoczatkowaLabel
            // 
            this.dataPoczatkowaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dataPoczatkowaLabel.AutoSize = true;
            this.dataPoczatkowaLabel.Location = new System.Drawing.Point(3, 38);
            this.dataPoczatkowaLabel.Name = "dataPoczatkowaLabel";
            this.dataPoczatkowaLabel.Size = new System.Drawing.Size(91, 13);
            this.dataPoczatkowaLabel.TabIndex = 33;
            this.dataPoczatkowaLabel.Text = "Data początkowa";
            // 
            // UdzialNieaktywnychButton
            // 
            this.UdzialNieaktywnychButton.Location = new System.Drawing.Point(901, 314);
            this.UdzialNieaktywnychButton.Name = "UdzialNieaktywnychButton";
            this.UdzialNieaktywnychButton.Size = new System.Drawing.Size(75, 25);
            this.UdzialNieaktywnychButton.TabIndex = 24;
            this.UdzialNieaktywnychButton.Text = "Oblicz";
            this.UdzialNieaktywnychButton.UseVisualStyleBackColor = true;
            this.UdzialNieaktywnychButton.Click += new System.EventHandler(this.ObliczButtonClick);
            // 
            // udzialNieaktywnychLabel
            // 
            this.udzialNieaktywnychLabel.AutoSize = true;
            this.udzialNieaktywnychLabel.Location = new System.Drawing.Point(389, 358);
            this.udzialNieaktywnychLabel.Name = "udzialNieaktywnychLabel";
            this.udzialNieaktywnychLabel.Size = new System.Drawing.Size(0, 13);
            this.udzialNieaktywnychLabel.TabIndex = 23;
            // 
            // najczesciejWybieraneGridView
            // 
            this.najczesciejWybieraneGridView.AllowUserToAddRows = false;
            this.najczesciejWybieraneGridView.AllowUserToDeleteRows = false;
            this.najczesciejWybieraneGridView.AllowUserToResizeColumns = false;
            this.najczesciejWybieraneGridView.AllowUserToResizeRows = false;
            this.najczesciejWybieraneGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.najczesciejWybieraneGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.najczesciejWybieraneGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.najczesciejWybieraneGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PozycjaWRankinguColumn,
            this.NazwaColumn,
            this.LiczbaWypozyczenCol});
            this.najczesciejWybieraneGridView.Location = new System.Drawing.Point(0, 3);
            this.najczesciejWybieraneGridView.MultiSelect = false;
            this.najczesciejWybieraneGridView.Name = "najczesciejWybieraneGridView";
            this.najczesciejWybieraneGridView.ReadOnly = true;
            this.najczesciejWybieraneGridView.RowHeadersVisible = false;
            this.najczesciejWybieraneGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.najczesciejWybieraneGridView.Size = new System.Drawing.Size(975, 308);
            this.najczesciejWybieraneGridView.TabIndex = 0;
            // 
            // PozycjaWRankinguColumn
            // 
            this.PozycjaWRankinguColumn.HeaderText = "Pozycja w rankingu";
            this.PozycjaWRankinguColumn.Name = "PozycjaWRankinguColumn";
            this.PozycjaWRankinguColumn.ReadOnly = true;
            this.PozycjaWRankinguColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NazwaColumn
            // 
            this.NazwaColumn.HeaderText = "Nazwa";
            this.NazwaColumn.Name = "NazwaColumn";
            this.NazwaColumn.ReadOnly = true;
            this.NazwaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // LiczbaWypozyczenCol
            // 
            this.LiczbaWypozyczenCol.HeaderText = "Liczba wypożyczeń";
            this.LiczbaWypozyczenCol.Name = "LiczbaWypozyczenCol";
            this.LiczbaWypozyczenCol.ReadOnly = true;
            this.LiczbaWypozyczenCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // resetujNajczesciejWybieraneButton
            // 
            this.resetujNajczesciejWybieraneButton.Location = new System.Drawing.Point(366, 345);
            this.resetujNajczesciejWybieraneButton.Name = "resetujNajczesciejWybieraneButton";
            this.resetujNajczesciejWybieraneButton.Size = new System.Drawing.Size(75, 25);
            this.resetujNajczesciejWybieraneButton.TabIndex = 21;
            this.resetujNajczesciejWybieraneButton.Text = "Resetuj";
            this.resetujNajczesciejWybieraneButton.UseVisualStyleBackColor = true;
            this.resetujNajczesciejWybieraneButton.Click += new System.EventHandler(this.ResetujNajczesciejWybieraneButtonClick);
            // 
            // wyswietlNajczesciejWybieraneButton
            // 
            this.wyswietlNajczesciejWybieraneButton.Location = new System.Drawing.Point(366, 314);
            this.wyswietlNajczesciejWybieraneButton.Name = "wyswietlNajczesciejWybieraneButton";
            this.wyswietlNajczesciejWybieraneButton.Size = new System.Drawing.Size(75, 25);
            this.wyswietlNajczesciejWybieraneButton.TabIndex = 20;
            this.wyswietlNajczesciejWybieraneButton.Text = "Wyświetl";
            this.wyswietlNajczesciejWybieraneButton.UseVisualStyleBackColor = true;
            this.wyswietlNajczesciejWybieraneButton.Click += new System.EventHandler(this.WyswietlNajczesciejWybieraneButtonClick);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.AutoSize = true;
            this.tableLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.39682F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.60318F));
            this.tableLayoutPanel6.Controls.Add(this.dateTimePicker8, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.liczbaMiejscWRankinguComboBox, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.dateTimePicker7, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.label20, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label17, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.najczesciejWybieraneComboBox, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.odbiorcaComboBox2, 1, 7);
            this.tableLayoutPanel6.Controls.Add(this.dataKoncowa2Label, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.grupaWiekowaComboBox, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.odbiorcaLabel, 0, 7);
            this.tableLayoutPanel6.Controls.Add(this.label18, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.dataPoczatkowa2Label, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.plecComboBox, 1, 5);
            this.tableLayoutPanel6.Controls.Add(this.czestotliwoscUkazywaniaSieComboBox, 1, 6);
            this.tableLayoutPanel6.Controls.Add(this.label19, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.czestotliwoscUkazywaniaSieLabel, 0, 6);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 314);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 8;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.Size = new System.Drawing.Size(357, 238);
            this.tableLayoutPanel6.TabIndex = 19;
            // 
            // dateTimePicker8
            // 
            this.dateTimePicker8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dateTimePicker8.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker8.Location = new System.Drawing.Point(165, 92);
            this.dateTimePicker8.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.dateTimePicker8.Name = "dateTimePicker8";
            this.dateTimePicker8.Size = new System.Drawing.Size(189, 20);
            this.dateTimePicker8.TabIndex = 38;
            // 
            // liczbaMiejscWRankinguComboBox
            // 
            this.liczbaMiejscWRankinguComboBox.FormattingEnabled = true;
            this.liczbaMiejscWRankinguComboBox.Items.AddRange(new object[] {
            "1",
            "3",
            "5",
            "10",
            "20",
            "30"});
            this.liczbaMiejscWRankinguComboBox.Location = new System.Drawing.Point(165, 33);
            this.liczbaMiejscWRankinguComboBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.liczbaMiejscWRankinguComboBox.Name = "liczbaMiejscWRankinguComboBox";
            this.liczbaMiejscWRankinguComboBox.Size = new System.Drawing.Size(189, 21);
            this.liczbaMiejscWRankinguComboBox.TabIndex = 22;
            // 
            // dateTimePicker7
            // 
            this.dateTimePicker7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dateTimePicker7.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker7.Location = new System.Drawing.Point(165, 63);
            this.dateTimePicker7.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.dateTimePicker7.Name = "dateTimePicker7";
            this.dateTimePicker7.Size = new System.Drawing.Size(189, 20);
            this.dateTimePicker7.TabIndex = 38;
            this.dateTimePicker7.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(3, 38);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(125, 13);
            this.label20.TabIndex = 22;
            this.label20.Text = "Liczba miejsc w rankingu";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(112, 13);
            this.label17.TabIndex = 15;
            this.label17.Text = "Najczęściej wybierane";
            // 
            // najczesciejWybieraneComboBox
            // 
            this.najczesciejWybieraneComboBox.FormattingEnabled = true;
            this.najczesciejWybieraneComboBox.Items.AddRange(new object[] {
            "kategoria książek",
            "tematyka czasopism",
            "tytuł książki",
            "tytuł czasopisma"});
            this.najczesciejWybieraneComboBox.Location = new System.Drawing.Point(165, 3);
            this.najczesciejWybieraneComboBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.najczesciejWybieraneComboBox.Name = "najczesciejWybieraneComboBox";
            this.najczesciejWybieraneComboBox.Size = new System.Drawing.Size(189, 21);
            this.najczesciejWybieraneComboBox.TabIndex = 16;
            this.najczesciejWybieraneComboBox.SelectedIndexChanged += new System.EventHandler(this.najczesciejWybieraneComboBox_SelectedIndexChanged);
            // 
            // odbiorcaComboBox2
            // 
            this.odbiorcaComboBox2.DisplayMember = "Nazwa";
            this.odbiorcaComboBox2.Enabled = false;
            this.odbiorcaComboBox2.FormattingEnabled = true;
            this.odbiorcaComboBox2.Location = new System.Drawing.Point(165, 211);
            this.odbiorcaComboBox2.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.odbiorcaComboBox2.Name = "odbiorcaComboBox2";
            this.odbiorcaComboBox2.Size = new System.Drawing.Size(189, 21);
            this.odbiorcaComboBox2.TabIndex = 40;
            this.odbiorcaComboBox2.ValueMember = "Id";
            // 
            // dataKoncowa2Label
            // 
            this.dataKoncowa2Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dataKoncowa2Label.AutoSize = true;
            this.dataKoncowa2Label.Location = new System.Drawing.Point(3, 97);
            this.dataKoncowa2Label.Name = "dataKoncowa2Label";
            this.dataKoncowa2Label.Size = new System.Drawing.Size(77, 13);
            this.dataKoncowa2Label.TabIndex = 38;
            this.dataKoncowa2Label.Text = "Data końcowa";
            // 
            // grupaWiekowaComboBox
            // 
            this.grupaWiekowaComboBox.FormattingEnabled = true;
            this.grupaWiekowaComboBox.Items.AddRange(new object[] {
            "0  - 19",
            "20 - 24",
            "25 - 44",
            "45 - 60",
            "60 - 99"});
            this.grupaWiekowaComboBox.Location = new System.Drawing.Point(165, 121);
            this.grupaWiekowaComboBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.grupaWiekowaComboBox.Name = "grupaWiekowaComboBox";
            this.grupaWiekowaComboBox.Size = new System.Drawing.Size(189, 21);
            this.grupaWiekowaComboBox.TabIndex = 20;
            // 
            // odbiorcaLabel
            // 
            this.odbiorcaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.odbiorcaLabel.AutoSize = true;
            this.odbiorcaLabel.Location = new System.Drawing.Point(3, 216);
            this.odbiorcaLabel.Name = "odbiorcaLabel";
            this.odbiorcaLabel.Size = new System.Drawing.Size(50, 13);
            this.odbiorcaLabel.TabIndex = 38;
            this.odbiorcaLabel.Text = "Odbiorca";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 126);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(81, 13);
            this.label18.TabIndex = 16;
            this.label18.Text = "Grupa wiekowa";
            // 
            // dataPoczatkowa2Label
            // 
            this.dataPoczatkowa2Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dataPoczatkowa2Label.AutoSize = true;
            this.dataPoczatkowa2Label.Location = new System.Drawing.Point(3, 68);
            this.dataPoczatkowa2Label.Name = "dataPoczatkowa2Label";
            this.dataPoczatkowa2Label.Size = new System.Drawing.Size(91, 13);
            this.dataPoczatkowa2Label.TabIndex = 38;
            this.dataPoczatkowa2Label.Text = "Data początkowa";
            // 
            // plecComboBox
            // 
            this.plecComboBox.FormattingEnabled = true;
            this.plecComboBox.Items.AddRange(new object[] {
            "kobieta",
            "mężczyzna"});
            this.plecComboBox.Location = new System.Drawing.Point(165, 151);
            this.plecComboBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.plecComboBox.Name = "plecComboBox";
            this.plecComboBox.Size = new System.Drawing.Size(189, 21);
            this.plecComboBox.TabIndex = 21;
            // 
            // czestotliwoscUkazywaniaSieComboBox
            // 
            this.czestotliwoscUkazywaniaSieComboBox.DisplayMember = "Nazwa";
            this.czestotliwoscUkazywaniaSieComboBox.Enabled = false;
            this.czestotliwoscUkazywaniaSieComboBox.FormattingEnabled = true;
            this.czestotliwoscUkazywaniaSieComboBox.Location = new System.Drawing.Point(165, 181);
            this.czestotliwoscUkazywaniaSieComboBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.czestotliwoscUkazywaniaSieComboBox.Name = "czestotliwoscUkazywaniaSieComboBox";
            this.czestotliwoscUkazywaniaSieComboBox.Size = new System.Drawing.Size(189, 21);
            this.czestotliwoscUkazywaniaSieComboBox.TabIndex = 39;
            this.czestotliwoscUkazywaniaSieComboBox.ValueMember = "Id";
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 156);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "Płeć";
            // 
            // czestotliwoscUkazywaniaSieLabel
            // 
            this.czestotliwoscUkazywaniaSieLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.czestotliwoscUkazywaniaSieLabel.AutoSize = true;
            this.czestotliwoscUkazywaniaSieLabel.Location = new System.Drawing.Point(3, 186);
            this.czestotliwoscUkazywaniaSieLabel.Name = "czestotliwoscUkazywaniaSieLabel";
            this.czestotliwoscUkazywaniaSieLabel.Size = new System.Drawing.Size(146, 13);
            this.czestotliwoscUkazywaniaSieLabel.TabIndex = 38;
            this.czestotliwoscUkazywaniaSieLabel.Text = "Częstotliwość ukazywania się";
            // 
            // IdKsiazkiColumn
            // 
            this.IdKsiazkiColumn.DataPropertyName = "Id";
            this.IdKsiazkiColumn.FillWeight = 22.655F;
            this.IdKsiazkiColumn.HeaderText = "#";
            this.IdKsiazkiColumn.Name = "IdKsiazkiColumn";
            this.IdKsiazkiColumn.ReadOnly = true;
            this.IdKsiazkiColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TytulKsiazkiColumn
            // 
            this.TytulKsiazkiColumn.DataPropertyName = "Tytul";
            this.TytulKsiazkiColumn.FillWeight = 182.7968F;
            this.TytulKsiazkiColumn.HeaderText = "Tytuł";
            this.TytulKsiazkiColumn.Name = "TytulKsiazkiColumn";
            this.TytulKsiazkiColumn.ReadOnly = true;
            this.TytulKsiazkiColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // IsbnKsiazkiColumn
            // 
            this.IsbnKsiazkiColumn.DataPropertyName = "Isbn";
            this.IsbnKsiazkiColumn.FillWeight = 70.72589F;
            this.IsbnKsiazkiColumn.HeaderText = "ISBN";
            this.IsbnKsiazkiColumn.Name = "IsbnKsiazkiColumn";
            this.IsbnKsiazkiColumn.ReadOnly = true;
            this.IsbnKsiazkiColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AutorKsiazkiColumn
            // 
            this.AutorKsiazkiColumn.DataPropertyName = "Autor";
            this.AutorKsiazkiColumn.FillWeight = 101.7259F;
            this.AutorKsiazkiColumn.HeaderText = "Autor";
            this.AutorKsiazkiColumn.Name = "AutorKsiazkiColumn";
            this.AutorKsiazkiColumn.ReadOnly = true;
            this.AutorKsiazkiColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // WydawnictwoKsiazkiColumn
            // 
            this.WydawnictwoKsiazkiColumn.DataPropertyName = "Wydawnictwo";
            this.WydawnictwoKsiazkiColumn.FillWeight = 101.7259F;
            this.WydawnictwoKsiazkiColumn.HeaderText = "Wydawnictwo";
            this.WydawnictwoKsiazkiColumn.Name = "WydawnictwoKsiazkiColumn";
            this.WydawnictwoKsiazkiColumn.ReadOnly = true;
            this.WydawnictwoKsiazkiColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // rokWydaniaKsiazkiColumn
            // 
            this.rokWydaniaKsiazkiColumn.DataPropertyName = "RokWydania";
            this.rokWydaniaKsiazkiColumn.FillWeight = 40.37056F;
            this.rokWydaniaKsiazkiColumn.HeaderText = "Rok";
            this.rokWydaniaKsiazkiColumn.Name = "rokWydaniaKsiazkiColumn";
            this.rokWydaniaKsiazkiColumn.ReadOnly = true;
            this.rokWydaniaKsiazkiColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PanelAdministratora
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 666);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PanelAdministratora";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Panel administratora";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wypozyczeniaGridView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ksiazkiGridView)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.czytelnicyGridView)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.czasopismaGridView)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.najczesciejWybieraneGridView)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label szukajLabel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem wylogujMenuItem;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView wypozyczeniaGridView;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button dodajKsiazkeButton;
        private System.Windows.Forms.Button resetujKsiazkiButton;
        private System.Windows.Forms.Button szukajKsiazekButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tytulKsiazkiTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox kategoriaComboBox;
        private System.Windows.Forms.TextBox isbnKsiazkiTextBox;
        private System.Windows.Forms.Button edytujKsiazkeButton;
        private System.Windows.Forms.DataGridView ksiazkiGridView;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button resetujCzytelnikowButton;
        private System.Windows.Forms.Button szukajCzytelnikowButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label imieLabel;
        private System.Windows.Forms.TextBox imieTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox statusComboBox;
        private System.Windows.Forms.Button dodajCzytelnikaButton;
        private System.Windows.Forms.Button edytujCzytelnikaButton;
        private System.Windows.Forms.DataGridView czytelnicyGridView;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button resetujCzasopismaButton;
        private System.Windows.Forms.Button szukajCzasopismButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tytulCzasopismaTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox tematykaComboBox;
        private System.Windows.Forms.TextBox issnCzasopismaTextBex;
        private System.Windows.Forms.ComboBox odbiorcaComboBox;
        private System.Windows.Forms.Button dodajCzasopismoButton;
        private System.Windows.Forms.Button edytujCzasopismoButton;
        private System.Windows.Forms.DataGridView czasopismaGridView;
        private System.Windows.Forms.TextBox autorKsiazkiTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox wydawnictwoKsiazkiTextBox;
        private System.Windows.Forms.TextBox rokKsiazkiTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox wydawnictwoCzasopismaTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.ComboBox czestotliwoscComboBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button resetujWypozyczeniaButton;
        private System.Windows.Forms.Button szukajWypozyczenButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.ToolStripMenuItem zakończMenuItem;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.ComboBox plecComboBox;
        private System.Windows.Forms.ComboBox grupaWiekowaComboBox;
        private System.Windows.Forms.ComboBox najczesciejWybieraneComboBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox liczbaMiejscWRankinguComboBox;
        private System.Windows.Forms.Button resetujNajczesciejWybieraneButton;
        private System.Windows.Forms.Button wyswietlNajczesciejWybieraneButton;
        private System.Windows.Forms.DataGridView najczesciejWybieraneGridView;
        private System.Windows.Forms.Label udzialNieaktywnychLabel;
        private System.Windows.Forms.Button UdzialNieaktywnychButton;
        private System.Windows.Forms.Button Resetuj2Button;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label udzialOsobNieaktywnychLabel;
        private System.Windows.Forms.Label calkowitaLiczbaWypozyczenLabel;
        private System.Windows.Forms.Label dataPoczatkowaLabel;
        private System.Windows.Forms.ComboBox rodzajZbioruComboBox;
        private System.Windows.Forms.Label calkowitaLiczbaWypLabel;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.DateTimePicker dateTimePicker6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label dataKoncowaLabel;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label zbioryBiblioteczneLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn PozycjaWRankinguColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazwaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LiczbaWypozyczenCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn daneColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tytulWypozyczeniaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sygnaturaWypozyczeniaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataWypozyczeniaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn terminZwrWypozyczeniaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idCzytelnikaWypColumn;
        private System.Windows.Forms.Button wypozyczenieButton;
        private System.Windows.Forms.Label odbiorcaLabel;
        private System.Windows.Forms.Label czestotliwoscUkazywaniaSieLabel;
        private System.Windows.Forms.ComboBox czestotliwoscUkazywaniaSieComboBox;
        private System.Windows.Forms.ComboBox odbiorcaComboBox2;
        private System.Windows.Forms.Label dataPoczatkowa2Label;
        private System.Windows.Forms.Label dataKoncowa2Label;
        private System.Windows.Forms.DateTimePicker dateTimePicker7;
        private System.Windows.Forms.DateTimePicker dateTimePicker8;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdCzytelnikaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImieCzytelnikaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NazwiskoCzytelnikaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusKartyColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn liczbaWypozyczenColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdCzasopismaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TytulCzasopismaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IssnCzasopismaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nrCzasopismaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wydawnictwoCzasopismaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdKsiazkiColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TytulKsiazkiColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsbnKsiazkiColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AutorKsiazkiColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn WydawnictwoKsiazkiColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rokWydaniaKsiazkiColumn;
    }
}