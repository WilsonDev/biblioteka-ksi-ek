﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Diagnostics;
using BibliotekaKsiazek;

namespace Biblioteka.Forms
{
    public partial class Logowanie : Form
    {
        public Czytelnik czytelnikLogowany;
        private Permission czyZalogowano;
        public Permission CzyZalogowano
        {
            get
            {
                return czyZalogowano;
            }
            set
            {
                czyZalogowano = value;
            }
        }

        public enum Permission
        {
            Admin = 1,
            User = 2,
            Guest = 3
        }

        public Logowanie()
        {
            InitializeComponent();
        }

        private void AnulujButtonClick(object sender, EventArgs e) //zamyka program
        {
            Close();
        }

        private void LogujButtonClick(object sender, EventArgs e) //loguje do programu
        {
            //tu będą metody sprawdzające loginy i hasła z bazy
            //włącznie odpowiedniego trybu (admin/czytelnik) po sprawdzeniu loginu

            CzytelnikRepository czytelnikRepository = new CzytelnikRepository();
            PracownikRepository pracownikRepository = new PracownikRepository();

            pracownikRepository.PobierzDane();
            czytelnikRepository.PobierzDane();

            this.czytelnikLogowany = czytelnikRepository.Lista.Find(k => k.Pesel.Equals(loginTextBox.Text));
            Pracownik pracownik = pracownikRepository.Lista.Find(k => k.Login.Equals(loginTextBox.Text));

            if (this.czytelnikLogowany != null && this.czytelnikLogowany.Haslo.Equals(hasloTextBox.Text))
            {
                czyZalogowano = Permission.User;
                this.Close();
            }
            else if (pracownik != null && pracownik.Haslo.Equals(hasloTextBox.Text))
            {
                czyZalogowano = Permission.Admin;
                this.Close();
            }
            else
                MessageBox.Show("Błędny login i hasło", "Ostrzeżenie", MessageBoxButtons.OK);
        }

        //Loguje do panelu gościa
        private void GoscButtonClick(object sender, EventArgs e)
        {
            czyZalogowano = Permission.Guest;

            this.Close();
        }
    }
}
