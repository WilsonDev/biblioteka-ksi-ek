﻿using Biblioteka.Baza;
using BibliotekaKsiazek;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka.Forms
{
    public partial class EdycjaCzytelnika : Form
    {
        private Czytelnik czytelnik;
        private KartaCzytelnika karta;
        private IEnumerable<Wypozyczenie> wypozyczenia;

        public EdycjaCzytelnika()
        {
            InitializeComponent();

            this.wypozyczeniaGridView.AutoGenerateColumns = false;

            this.usunButton.Visible = false;
            this.statusKartyButton.Visible = false;
            this.Text = "Dodaj czytelnika";
            this.czytelnikTabControl.TabPages.Remove(wypozyczeniaTabPage);
        }

        public void PobierzDaneCzytelnika(Czytelnik czytelnik, KartaCzytelnika karta, IEnumerable<Wypozyczenie> wypozyczenia, int flag = 0)
        {
            this.usunButton.Visible = true;
            this.statusKartyButton.Visible = true;
            this.czytelnikTabControl.TabPages.Add(wypozyczeniaTabPage);
            this.Text = "Edycja czytelnika";

            if (flag == 1)
                this.czytelnikTabControl.SelectedTab = wypozyczeniaTabPage;

            if (karta.Status.Equals("aktywna"))
                this.statusKartyButton.Text = "Dezaktywuj";

            this.czytelnik = czytelnik;
            this.karta = karta;
            this.wypozyczenia = wypozyczenia;

            foreach (var obj in wypozyczenia)
            {
                DataGridViewRow row = this.wypozyczeniaGridView.RowTemplate.Clone() as DataGridViewRow;
                row.CreateCells(this.wypozyczeniaGridView);

                row.Cells[0].Value = obj.Tytul();
                row.Cells[1].Value = obj.Sygnatura();
                row.Cells[2].Value = obj.DataWypozyczenia.ToShortDateString();
                row.Cells[3].Value = obj.TerminZwrotu.ToShortDateString();
                row.Cells[4].Value = obj.Id(); //Kolumna ukryta. Wartość potrzebna to operacji dodawania/edycji/usuwania
                row.Cells[5].Value = obj.GetType().Name; //Kolumna ukryta. Wartość potrzebna to operacji dodawania/edycji/usuwania

                this.wypozyczeniaGridView.Rows.Add(row);
            }

            //Dane czytelnika
            this.imieTextBox.Text = czytelnik.Imie;
            this.nazwiskoTextBox.Text = czytelnik.Nazwisko;
            this.peselTextBox.Text = czytelnik.Pesel;
            this.dataTimePicker.Value = czytelnik.DataUrodzenia;

            if (czytelnik.Plec.ToString().Equals("K"))
                this.kobietaRadioButton.Checked = true;

            //Adres czytelnika
            this.adresTextBox.Text = czytelnik.Adres.Ulica;
            this.kodPocztowyTextBox.Text = czytelnik.Adres.KodPocztowy;
            this.miejscowoscTextBox.Text = czytelnik.Adres.Miejscowosc;
            this.telefonTextBox.Text = czytelnik.Adres.Telefon;

            //Karta czytelnika
            this.statusLabel.Text = karta.Status;
            this.dataLabel.Text = karta.DataWystawienia.ToShortDateString();
            this.terminLabel.Text = karta.TerminWaznosci.ToShortDateString();
        }

        private void AnulujButtonClick(object sender, EventArgs e)
        {
            Close();
        }

        //Zapisujemy edytowanego czytelnika lub dodajemy nowego
        //Trzeba by to rodzielić dla większej przejrzystości ale skoro działa? to niech tak zostanie
        private void ZapiszButtonClick(object sender, EventArgs e)
        {
            CzytelnikRepository czytelnikRepository = new CzytelnikRepository();
            AdresRepository adresRepository = new AdresRepository();
            KartaCzytelnikaRepository kartaCzytelnikaRepository = new KartaCzytelnikaRepository();

            Czytelnik edited;
            Adres adres;
            KartaCzytelnika kartaCzyt;

            if (czytelnik == null)
            {
                edited = new Czytelnik();
                adres = new Adres();
                kartaCzyt = new KartaCzytelnika();

                //Domyślne dane nowo dodawanej karty czytelnika
                kartaCzyt.Status = "aktywna";
                kartaCzyt.DataWystawienia = DateTime.Now;
                kartaCzyt.TerminWaznosci = DateTime.Now.AddYears(5);
            }
            else
            {
                edited = czytelnik;
                adres = czytelnik.Adres;
                kartaCzyt = karta;
            }

            //Przypisujemy wartości z textboxów do obiektów
            edited.Imie = this.imieTextBox.Text;
            edited.Nazwisko = this.nazwiskoTextBox.Text;
            edited.Pesel = this.peselTextBox.Text;
            edited.DataUrodzenia = this.dataTimePicker.Value;
            if (kobietaRadioButton.Checked)
                edited.Plec = 'K';
            else
                edited.Plec = 'M';
            adres.Ulica = this.adresTextBox.Text;
            adres.Miejscowosc = this.miejscowoscTextBox.Text;
            adres.KodPocztowy = this.kodPocztowyTextBox.Text;
            adres.Telefon = this.telefonTextBox.Text;

            //Ustalamy domyślny kolor textbox'ów
            this.imieTextBox.BackColor = SystemColors.Window;
            this.nazwiskoTextBox.BackColor = SystemColors.Window;
            this.peselTextBox.BackColor = SystemColors.Window;
            this.adresTextBox.BackColor = SystemColors.Window;
            this.kodPocztowyTextBox.BackColor = SystemColors.Window;
            this.miejscowoscTextBox.BackColor = SystemColors.Window;
            this.telefonTextBox.BackColor = SystemColors.Window;

            this.errorProvider.Clear();

            //Zapisujemy nowego czytelnika
            if (czytelnik == null)
            {
                if (this.CzyDanePoprawne() && MessageBox.Show("Zapisać czytelnika?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    edited.Haslo = RandomString(8);

                    adresRepository.Dodaj(adres); //Dodajemy najpierw adres do bazy

                    adres.Id = BazaSQLite.LastId(); //Pobieramy jego id
                    edited.Adres = adres;
                    czytelnikRepository.Dodaj(edited); //Znając id adresu dodajemy czytelnika

                    edited.Id = BazaSQLite.LastId(); //id czytelnika dodanego do bazy
                    kartaCzyt.Czytelnik = edited;
                    kartaCzytelnikaRepository.Dodaj(kartaCzyt); //Ostatecznie zapisujemy kartę czytelnika

                    MessageBox.Show("Tymczasowe hasło: " + edited.Haslo);

                    this.DialogResult = DialogResult.OK;
                }
            }
            else //Edytujemy obecnego czytelnika  
            {
                if (this.CzyDanePoprawne() && MessageBox.Show("Zaktualizować dane czytelnika?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    adresRepository.Edytuj(adres);
                    czytelnikRepository.Edytuj(edited);
                    this.DialogResult = DialogResult.OK;
                }
            }   
        }

        //Usuwamy czytelnika
        private void UsunButtonClick(object sender, EventArgs e)
        {
            WKRepository wkRepository = new WKRepository();
            WCRepository wcRepository = new WCRepository();
            KartaCzytelnikaRepository kartaCzytelnikaRepository = new KartaCzytelnikaRepository();

            if (MessageBox.Show("Usunąć czytelnika?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                foreach (var obj in wypozyczenia)
                {
                    if (obj.GetType().Name.Equals("WypozyczenieKsiazki"))
                    {
                        obj.DataZwrotu = DateTime.Now;
                        wkRepository.Edytuj((WypozyczenieKsiazki)obj);
                    }
                    else
                    {
                        obj.DataZwrotu = DateTime.Now;
                        wcRepository.Edytuj((WypozyczenieCzasopisma)obj);
                    }
                }

                karta.Status = "RIP";
                kartaCzytelnikaRepository.Edytuj(karta);

                this.DialogResult = DialogResult.OK;
            }
        }

        //Zwracamy wypożyczenia
        private void ZwrocButtonClick(object sender, EventArgs e)
        {
            if (wypozyczeniaGridView.SelectedRows.Count > 0 && MessageBox.Show("Oddać?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //Jeżeli wypożyczenie dotyczy książki
                if (wypozyczeniaGridView.SelectedCells[5].Value.Equals("WypozyczenieKsiazki"))
                {
                    WKRepository wkRepository = new WKRepository();

                    WypozyczenieKsiazki wypozyczenie = new WypozyczenieKsiazki();
                    EgzemplarzKsiazki egzemplarz = new EgzemplarzKsiazki();

                    wypozyczenie.EgzemplarzKsiazki = egzemplarz;
                    wypozyczenie.EgzemplarzKsiazki.Id = Convert.ToInt64(wypozyczeniaGridView.SelectedCells[4].Value);
                    wypozyczenie.KartaCzytelnika = karta;

                    wkRepository.Zwroc(wypozyczenie);
                    this.DialogResult = DialogResult.OK;
                }
                else //Jeżeli wypożyczenie dotyczy czasopisma
                {
                    WCRepository wcRepository = new WCRepository();

                    WypozyczenieCzasopisma wypozyczenie = new WypozyczenieCzasopisma();
                    EgzemplarzCzasopisma egzemplarz = new EgzemplarzCzasopisma();

                    wypozyczenie.EgzemplarzCzasopisma = egzemplarz;
                    wypozyczenie.EgzemplarzCzasopisma.Id = Convert.ToInt64(wypozyczeniaGridView.SelectedCells[4].Value);
                    wypozyczenie.KartaCzytelnika = karta;

                    wcRepository.Zwroc(wypozyczenie);
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private bool CzyDanePoprawne()
        {
            bool result = true;

            Regex regImie;
            regImie = new Regex("^[A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]{1,39}([ ])?([A-ZZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]{1,39})?$");
            if (!regImie.IsMatch(imieTextBox.Text)) {
                imieTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(imieTextBox, "Niepoprawne imię");
            }

            Regex regNazwisko;
            regNazwisko = new Regex("^[A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]{1,39}(( - )?[A-Z]{1}[a-ząćęłóńśżź]{1,39})?$");
            if (!regNazwisko.IsMatch(nazwiskoTextBox.Text)) {
                nazwiskoTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(nazwiskoTextBox, "Niepoprawne nazwisko");
            }

            Regex regPESEL;
            regPESEL = new Regex("^[0-9]{11}$");
            if (!regPESEL.IsMatch(peselTextBox.Text)) {
                peselTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(peselTextBox, "Niepoprawny PESEL");
            }

            if (dataTimePicker.Value > DateTime.Now) {
                result = false;
                errorProvider.SetError(dataTimePicker, "Niepoprawna data");
            }

            Regex regUlica;
            regUlica = new Regex("^([A-ZŁŃŚŻŹĘÓ]{1})|([1-9]{1}[0-9]* [A-ZŁŃŚŻŹĘÓ]{1})[a-ząćęłóńśżź]+[ ]?[ - ]?[i]?[z]?[u]?(([A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]+)|([1-9]{1}[0-9]*)|([1-9]{1}[0-9]* [A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]+))*$");
            if (!regUlica.IsMatch(adresTextBox.Text)) {
                adresTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(adresTextBox, "Niepoprawny adres");
            }

            Regex regKodpocztowy;
            regKodpocztowy = new Regex("^[0-9]{2}-[0-9]{2}[0-9]$");
            if (!regKodpocztowy.IsMatch(kodPocztowyTextBox.Text)) {
                kodPocztowyTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(kodPocztowyTextBox, "Niepoprawny kod pocztowy");
            }

            Regex regMiejscowosc;
            regMiejscowosc = new Regex("^[A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]{2,39}([ ])?([A-ZZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]{2,39})?$");
            if (!regMiejscowosc.IsMatch(miejscowoscTextBox.Text)) {
                miejscowoscTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(miejscowoscTextBox, "Niepoprawna miejscowość");
            }

            Regex regTelefon;
            regTelefon = new Regex("^[0-9]{9}$");
            if (!regTelefon.IsMatch(telefonTextBox.Text)) {
                telefonTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(telefonTextBox, "Niepoprawny numer telefonu");
            }

            return result;
        }

        private void StatusKartyButtonClick(object sender, EventArgs e)
        {
            KartaCzytelnikaRepository kartaCzytelnikaRepository = new KartaCzytelnikaRepository();
            KartaCzytelnika aktywuj = karta;

            if (this.statusKartyButton.Text.Equals("Aktywuj"))
            {
                aktywuj.Status = "aktywna";
                this.statusLabel.Text = "aktywna";
            }
            else
            {
                aktywuj.Status = "nieaktywna";
                this.statusLabel.Text = "nieaktywna";
            }

            kartaCzytelnikaRepository.Edytuj(aktywuj);
        }

        //generator losowego ciągu znaków
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}
