﻿namespace Biblioteka.Forms
{
    partial class PanelCzytelnika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.wypozyczoneTabControl = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.wypozyczeniaGridView = new System.Windows.Forms.DataGridView();
            this.tytulWypozyczeniaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sygnaturaWypozyczeniaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataWypColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.terminZwrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.szczegolyKsiazkiButton = new System.Windows.Forms.Button();
            this.ResetujKsiazkiButton = new System.Windows.Forms.Button();
            this.SzukajKsiazekButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.tytulKsiazkiTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.isbnKsiazkiTextBox = new System.Windows.Forms.TextBox();
            this.autorKsiazkiTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.wydawnictwoKsiazkiTextBox = new System.Windows.Forms.TextBox();
            this.kategoriaComboBox = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.rokKsiazkiTextBox = new System.Windows.Forms.TextBox();
            this.ksiazkiGridView = new System.Windows.Forms.DataGridView();
            this.tytulKsiazkiColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isbKsiazkiColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.autorKsiazkiColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.szczegolyCzasopismaButton = new System.Windows.Forms.Button();
            this.resetujCzasopismaButton = new System.Windows.Forms.Button();
            this.szukajCzasopismButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label20 = new System.Windows.Forms.Label();
            this.tytulCzasopismaTextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.issnCzasopismaTextBex = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tematykaComboBox = new System.Windows.Forms.ComboBox();
            this.odbiorcaComboBox = new System.Windows.Forms.ComboBox();
            this.czestotliwoscComboBox = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.czasopismaGridView = new System.Windows.Forms.DataGridView();
            this.tytulCzasopismaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.issnCzasopismaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Numer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefonLabel = new System.Windows.Forms.Label();
            this.miejscowoscLabel = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.wylogujMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zakonczMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zmianaHaslaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.peselLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.plecLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dataUrodzeniaLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.nazwiskoLabel = new System.Windows.Forms.Label();
            this.imieLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.kodPocztowyLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.ulicaLabel = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            this.dataWystawieniaLabel = new System.Windows.Forms.Label();
            this.terminWaznosciLabel = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.wypozyczoneTabControl.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wypozyczeniaGridView)).BeginInit();
            this.tabControl3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ksiazkiGridView)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.czasopismaGridView)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 639);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel2.Text = "toolStripStatusLabel2";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel1.Text = "Status";
            // 
            // wypozyczoneTabControl
            // 
            this.wypozyczoneTabControl.Controls.Add(this.tabPage3);
            this.wypozyczoneTabControl.Location = new System.Drawing.Point(8, 447);
            this.wypozyczoneTabControl.Name = "wypozyczoneTabControl";
            this.wypozyczoneTabControl.SelectedIndex = 0;
            this.wypozyczoneTabControl.Size = new System.Drawing.Size(988, 189);
            this.wypozyczoneTabControl.TabIndex = 3;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.wypozyczeniaGridView);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(980, 163);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Wypożyczone";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(314, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "label6";
            // 
            // wypozyczeniaGridView
            // 
            this.wypozyczeniaGridView.AllowUserToAddRows = false;
            this.wypozyczeniaGridView.AllowUserToDeleteRows = false;
            this.wypozyczeniaGridView.AllowUserToResizeColumns = false;
            this.wypozyczeniaGridView.AllowUserToResizeRows = false;
            this.wypozyczeniaGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.wypozyczeniaGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.wypozyczeniaGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.wypozyczeniaGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tytulWypozyczeniaColumn,
            this.sygnaturaWypozyczeniaColumn,
            this.dataWypColumn,
            this.terminZwrColumn});
            this.wypozyczeniaGridView.Location = new System.Drawing.Point(0, 3);
            this.wypozyczeniaGridView.MultiSelect = false;
            this.wypozyczeniaGridView.Name = "wypozyczeniaGridView";
            this.wypozyczeniaGridView.ReadOnly = true;
            this.wypozyczeniaGridView.RowHeadersVisible = false;
            this.wypozyczeniaGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.wypozyczeniaGridView.Size = new System.Drawing.Size(977, 157);
            this.wypozyczeniaGridView.TabIndex = 0;
            // 
            // tytulWypozyczeniaColumn
            // 
            this.tytulWypozyczeniaColumn.HeaderText = "Tytuł";
            this.tytulWypozyczeniaColumn.Name = "tytulWypozyczeniaColumn";
            this.tytulWypozyczeniaColumn.ReadOnly = true;
            // 
            // sygnaturaWypozyczeniaColumn
            // 
            this.sygnaturaWypozyczeniaColumn.HeaderText = "Sygnatura";
            this.sygnaturaWypozyczeniaColumn.Name = "sygnaturaWypozyczeniaColumn";
            this.sygnaturaWypozyczeniaColumn.ReadOnly = true;
            // 
            // dataWypColumn
            // 
            this.dataWypColumn.HeaderText = "Data wypożyczenia";
            this.dataWypColumn.Name = "dataWypColumn";
            this.dataWypColumn.ReadOnly = true;
            // 
            // terminZwrColumn
            // 
            this.terminZwrColumn.HeaderText = "Termin zwrotu";
            this.terminZwrColumn.Name = "terminZwrColumn";
            this.terminZwrColumn.ReadOnly = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage5);
            this.tabControl3.Controls.Add(this.tabPage2);
            this.tabControl3.Location = new System.Drawing.Point(325, 27);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(671, 415);
            this.tabControl3.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl3.TabIndex = 4;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.szczegolyKsiazkiButton);
            this.tabPage5.Controls.Add(this.ResetujKsiazkiButton);
            this.tabPage5.Controls.Add(this.SzukajKsiazekButton);
            this.tabPage5.Controls.Add(this.tableLayoutPanel4);
            this.tabPage5.Controls.Add(this.ksiazkiGridView);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(663, 389);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "Książki";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // szczegolyKsiazkiButton
            // 
            this.szczegolyKsiazkiButton.BackColor = System.Drawing.Color.Transparent;
            this.szczegolyKsiazkiButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.szczegolyKsiazkiButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.szczegolyKsiazkiButton.Location = new System.Drawing.Point(586, 357);
            this.szczegolyKsiazkiButton.Name = "szczegolyKsiazkiButton";
            this.szczegolyKsiazkiButton.Size = new System.Drawing.Size(75, 25);
            this.szczegolyKsiazkiButton.TabIndex = 14;
            this.szczegolyKsiazkiButton.Text = "Szczegóły";
            this.szczegolyKsiazkiButton.UseVisualStyleBackColor = false;
            this.szczegolyKsiazkiButton.Click += new System.EventHandler(this.SzczegolyKsiazkiButtonClick);
            // 
            // ResetujKsiazkiButton
            // 
            this.ResetujKsiazkiButton.Location = new System.Drawing.Point(586, 326);
            this.ResetujKsiazkiButton.Name = "ResetujKsiazkiButton";
            this.ResetujKsiazkiButton.Size = new System.Drawing.Size(75, 25);
            this.ResetujKsiazkiButton.TabIndex = 13;
            this.ResetujKsiazkiButton.Text = "Resetuj";
            this.ResetujKsiazkiButton.UseVisualStyleBackColor = true;
            this.ResetujKsiazkiButton.Click += new System.EventHandler(this.ResetujKsiazkiButtonClick);
            // 
            // SzukajKsiazekButton
            // 
            this.SzukajKsiazekButton.Location = new System.Drawing.Point(586, 295);
            this.SzukajKsiazekButton.Name = "SzukajKsiazekButton";
            this.SzukajKsiazekButton.Size = new System.Drawing.Size(75, 25);
            this.SzukajKsiazekButton.TabIndex = 12;
            this.SzukajKsiazekButton.Text = "Szukaj";
            this.SzukajKsiazekButton.UseVisualStyleBackColor = true;
            this.SzukajKsiazekButton.Click += new System.EventHandler(this.SzukajKsiazekButtonClick);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.AutoSize = true;
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel4.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tytulKsiazkiTextBox, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label15, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.isbnKsiazkiTextBox, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.autorKsiazkiTextBox, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.label16, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label17, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.wydawnictwoKsiazkiTextBox, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.kategoriaComboBox, 4, 1);
            this.tableLayoutPanel4.Controls.Add(this.label19, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.label18, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.rokKsiazkiTextBox, 4, 2);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 295);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(540, 88);
            this.tableLayoutPanel4.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Tytuł";
            // 
            // tytulKsiazkiTextBox
            // 
            this.tytulKsiazkiTextBox.Location = new System.Drawing.Point(93, 3);
            this.tytulKsiazkiTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.tytulKsiazkiTextBox.Name = "tytulKsiazkiTextBox";
            this.tytulKsiazkiTextBox.Size = new System.Drawing.Size(159, 20);
            this.tytulKsiazkiTextBox.TabIndex = 7;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 37);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 13);
            this.label15.TabIndex = 9;
            this.label15.Text = "ISBN";
            // 
            // isbnKsiazkiTextBox
            // 
            this.isbnKsiazkiTextBox.Location = new System.Drawing.Point(93, 32);
            this.isbnKsiazkiTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.isbnKsiazkiTextBox.Name = "isbnKsiazkiTextBox";
            this.isbnKsiazkiTextBox.Size = new System.Drawing.Size(159, 20);
            this.isbnKsiazkiTextBox.TabIndex = 11;
            // 
            // autorKsiazkiTextBox
            // 
            this.autorKsiazkiTextBox.Location = new System.Drawing.Point(93, 61);
            this.autorKsiazkiTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.autorKsiazkiTextBox.Name = "autorKsiazkiTextBox";
            this.autorKsiazkiTextBox.Size = new System.Drawing.Size(159, 20);
            this.autorKsiazkiTextBox.TabIndex = 12;
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 66);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 13);
            this.label16.TabIndex = 13;
            this.label16.Text = "Autor";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(283, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "Wydawnictwo";
            // 
            // wydawnictwoKsiazkiTextBox
            // 
            this.wydawnictwoKsiazkiTextBox.Location = new System.Drawing.Point(373, 3);
            this.wydawnictwoKsiazkiTextBox.Name = "wydawnictwoKsiazkiTextBox";
            this.wydawnictwoKsiazkiTextBox.Size = new System.Drawing.Size(159, 20);
            this.wydawnictwoKsiazkiTextBox.TabIndex = 16;
            // 
            // kategoriaComboBox
            // 
            this.kategoriaComboBox.DisplayMember = "Nazwa";
            this.kategoriaComboBox.FormattingEnabled = true;
            this.kategoriaComboBox.Location = new System.Drawing.Point(373, 32);
            this.kategoriaComboBox.Name = "kategoriaComboBox";
            this.kategoriaComboBox.Size = new System.Drawing.Size(159, 21);
            this.kategoriaComboBox.TabIndex = 8;
            this.kategoriaComboBox.ValueMember = "Id";
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(283, 37);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(52, 13);
            this.label19.TabIndex = 10;
            this.label19.Text = "Kategoria";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(283, 66);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(69, 13);
            this.label18.TabIndex = 15;
            this.label18.Text = "Rok wydania";
            // 
            // rokKsiazkiTextBox
            // 
            this.rokKsiazkiTextBox.Location = new System.Drawing.Point(373, 61);
            this.rokKsiazkiTextBox.Name = "rokKsiazkiTextBox";
            this.rokKsiazkiTextBox.Size = new System.Drawing.Size(159, 20);
            this.rokKsiazkiTextBox.TabIndex = 17;
            // 
            // ksiazkiGridView
            // 
            this.ksiazkiGridView.AllowUserToAddRows = false;
            this.ksiazkiGridView.AllowUserToDeleteRows = false;
            this.ksiazkiGridView.AllowUserToResizeColumns = false;
            this.ksiazkiGridView.AllowUserToResizeRows = false;
            this.ksiazkiGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ksiazkiGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ksiazkiGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ksiazkiGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tytulKsiazkiColumn,
            this.isbKsiazkiColumn,
            this.autorKsiazkiColumn});
            this.ksiazkiGridView.Location = new System.Drawing.Point(0, 3);
            this.ksiazkiGridView.Name = "ksiazkiGridView";
            this.ksiazkiGridView.ReadOnly = true;
            this.ksiazkiGridView.RowHeadersVisible = false;
            this.ksiazkiGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ksiazkiGridView.Size = new System.Drawing.Size(660, 290);
            this.ksiazkiGridView.TabIndex = 0;
            // 
            // tytulKsiazkiColumn
            // 
            this.tytulKsiazkiColumn.DataPropertyName = "Tytul";
            this.tytulKsiazkiColumn.HeaderText = "Tytuł";
            this.tytulKsiazkiColumn.Name = "tytulKsiazkiColumn";
            this.tytulKsiazkiColumn.ReadOnly = true;
            this.tytulKsiazkiColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // isbKsiazkiColumn
            // 
            this.isbKsiazkiColumn.DataPropertyName = "ISBN";
            this.isbKsiazkiColumn.FillWeight = 60F;
            this.isbKsiazkiColumn.HeaderText = "ISBN";
            this.isbKsiazkiColumn.Name = "isbKsiazkiColumn";
            this.isbKsiazkiColumn.ReadOnly = true;
            this.isbKsiazkiColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // autorKsiazkiColumn
            // 
            this.autorKsiazkiColumn.DataPropertyName = "Autor";
            this.autorKsiazkiColumn.FillWeight = 60F;
            this.autorKsiazkiColumn.HeaderText = "Autor";
            this.autorKsiazkiColumn.Name = "autorKsiazkiColumn";
            this.autorKsiazkiColumn.ReadOnly = true;
            this.autorKsiazkiColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.szczegolyCzasopismaButton);
            this.tabPage2.Controls.Add(this.resetujCzasopismaButton);
            this.tabPage2.Controls.Add(this.szukajCzasopismButton);
            this.tabPage2.Controls.Add(this.tableLayoutPanel5);
            this.tabPage2.Controls.Add(this.czasopismaGridView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(663, 389);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Czasopisma";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // szczegolyCzasopismaButton
            // 
            this.szczegolyCzasopismaButton.BackColor = System.Drawing.Color.Transparent;
            this.szczegolyCzasopismaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.szczegolyCzasopismaButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.szczegolyCzasopismaButton.Location = new System.Drawing.Point(586, 357);
            this.szczegolyCzasopismaButton.Name = "szczegolyCzasopismaButton";
            this.szczegolyCzasopismaButton.Size = new System.Drawing.Size(75, 25);
            this.szczegolyCzasopismaButton.TabIndex = 21;
            this.szczegolyCzasopismaButton.Text = "Szczegóły";
            this.szczegolyCzasopismaButton.UseVisualStyleBackColor = false;
            this.szczegolyCzasopismaButton.Click += new System.EventHandler(this.SzczegolyCzasopismaButtonClick);
            // 
            // resetujCzasopismaButton
            // 
            this.resetujCzasopismaButton.Location = new System.Drawing.Point(586, 326);
            this.resetujCzasopismaButton.Name = "resetujCzasopismaButton";
            this.resetujCzasopismaButton.Size = new System.Drawing.Size(75, 25);
            this.resetujCzasopismaButton.TabIndex = 20;
            this.resetujCzasopismaButton.Text = "Resetuj";
            this.resetujCzasopismaButton.UseVisualStyleBackColor = true;
            this.resetujCzasopismaButton.Click += new System.EventHandler(this.ResetujCzasopismaButtonClick);
            // 
            // szukajCzasopismButton
            // 
            this.szukajCzasopismButton.Location = new System.Drawing.Point(586, 295);
            this.szukajCzasopismButton.Name = "szukajCzasopismButton";
            this.szukajCzasopismButton.Size = new System.Drawing.Size(75, 25);
            this.szukajCzasopismButton.TabIndex = 19;
            this.szukajCzasopismButton.Text = "Szukaj";
            this.szukajCzasopismButton.UseVisualStyleBackColor = true;
            this.szukajCzasopismButton.Click += new System.EventHandler(this.SzukajCzasopismButtonClick);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.AutoSize = true;
            this.tableLayoutPanel5.ColumnCount = 5;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel5.Controls.Add(this.label20, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tytulCzasopismaTextBox, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label21, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.issnCzasopismaTextBex, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.label23, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.tematykaComboBox, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.odbiorcaComboBox, 4, 2);
            this.tableLayoutPanel5.Controls.Add(this.czestotliwoscComboBox, 4, 1);
            this.tableLayoutPanel5.Controls.Add(this.label25, 3, 2);
            this.tableLayoutPanel5.Controls.Add(this.label26, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.label24, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 1, 2);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 295);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.Size = new System.Drawing.Size(540, 89);
            this.tableLayoutPanel5.TabIndex = 18;
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(3, 8);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 7;
            this.label20.Text = "Tytuł";
            // 
            // tytulCzasopismaTextBox
            // 
            this.tytulCzasopismaTextBox.Location = new System.Drawing.Point(93, 3);
            this.tytulCzasopismaTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.tytulCzasopismaTextBox.Name = "tytulCzasopismaTextBox";
            this.tytulCzasopismaTextBox.Size = new System.Drawing.Size(159, 20);
            this.tytulCzasopismaTextBox.TabIndex = 7;
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 38);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(32, 13);
            this.label21.TabIndex = 9;
            this.label21.Text = "ISSN";
            // 
            // issnCzasopismaTextBex
            // 
            this.issnCzasopismaTextBex.Location = new System.Drawing.Point(93, 33);
            this.issnCzasopismaTextBex.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.issnCzasopismaTextBex.Name = "issnCzasopismaTextBex";
            this.issnCzasopismaTextBex.Size = new System.Drawing.Size(159, 20);
            this.issnCzasopismaTextBex.TabIndex = 11;
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(283, 8);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 13);
            this.label23.TabIndex = 10;
            this.label23.Text = "Tematyka";
            // 
            // tematykaComboBox
            // 
            this.tematykaComboBox.DisplayMember = "Nazwa";
            this.tematykaComboBox.FormattingEnabled = true;
            this.tematykaComboBox.Location = new System.Drawing.Point(373, 3);
            this.tematykaComboBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.tematykaComboBox.Name = "tematykaComboBox";
            this.tematykaComboBox.Size = new System.Drawing.Size(159, 21);
            this.tematykaComboBox.TabIndex = 8;
            this.tematykaComboBox.ValueMember = "Id";
            // 
            // odbiorcaComboBox
            // 
            this.odbiorcaComboBox.DisplayMember = "Nazwa";
            this.odbiorcaComboBox.Location = new System.Drawing.Point(373, 62);
            this.odbiorcaComboBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.odbiorcaComboBox.Name = "odbiorcaComboBox";
            this.odbiorcaComboBox.Size = new System.Drawing.Size(159, 21);
            this.odbiorcaComboBox.TabIndex = 0;
            this.odbiorcaComboBox.ValueMember = "Id";
            // 
            // czestotliwoscComboBox
            // 
            this.czestotliwoscComboBox.DisplayMember = "Nazwa";
            this.czestotliwoscComboBox.FormattingEnabled = true;
            this.czestotliwoscComboBox.Location = new System.Drawing.Point(373, 33);
            this.czestotliwoscComboBox.Name = "czestotliwoscComboBox";
            this.czestotliwoscComboBox.Size = new System.Drawing.Size(159, 21);
            this.czestotliwoscComboBox.TabIndex = 17;
            this.czestotliwoscComboBox.ValueMember = "Id";
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label25.Location = new System.Drawing.Point(283, 67);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(50, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Odbiorca";
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(283, 38);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 13);
            this.label26.TabIndex = 18;
            this.label26.Text = "Częstotliwość";
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(3, 67);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(58, 13);
            this.label24.TabIndex = 15;
            this.label24.Text = "Zakres dat";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.Controls.Add(this.dateTimePicker2, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.dateTimePicker1, 0, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(90, 59);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(170, 24);
            this.tableLayoutPanel6.TabIndex = 16;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(85, 3);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(77, 20);
            this.dateTimePicker2.TabIndex = 15;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(3, 3);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(76, 20);
            this.dateTimePicker1.TabIndex = 14;
            this.dateTimePicker1.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // czasopismaGridView
            // 
            this.czasopismaGridView.AllowUserToAddRows = false;
            this.czasopismaGridView.AllowUserToDeleteRows = false;
            this.czasopismaGridView.AllowUserToResizeColumns = false;
            this.czasopismaGridView.AllowUserToResizeRows = false;
            this.czasopismaGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.czasopismaGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.czasopismaGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.czasopismaGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tytulCzasopismaColumn,
            this.issnCzasopismaColumn,
            this.Numer});
            this.czasopismaGridView.Location = new System.Drawing.Point(0, 3);
            this.czasopismaGridView.Name = "czasopismaGridView";
            this.czasopismaGridView.ReadOnly = true;
            this.czasopismaGridView.RowHeadersVisible = false;
            this.czasopismaGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.czasopismaGridView.Size = new System.Drawing.Size(660, 290);
            this.czasopismaGridView.TabIndex = 0;
            // 
            // tytulCzasopismaColumn
            // 
            this.tytulCzasopismaColumn.DataPropertyName = "Tytul";
            this.tytulCzasopismaColumn.HeaderText = "Tytuł";
            this.tytulCzasopismaColumn.Name = "tytulCzasopismaColumn";
            this.tytulCzasopismaColumn.ReadOnly = true;
            this.tytulCzasopismaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // issnCzasopismaColumn
            // 
            this.issnCzasopismaColumn.DataPropertyName = "ISSN";
            this.issnCzasopismaColumn.FillWeight = 60F;
            this.issnCzasopismaColumn.HeaderText = "ISSN";
            this.issnCzasopismaColumn.Name = "issnCzasopismaColumn";
            this.issnCzasopismaColumn.ReadOnly = true;
            this.issnCzasopismaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Numer
            // 
            this.Numer.DataPropertyName = "nrCzasopisma";
            this.Numer.FillWeight = 60F;
            this.Numer.HeaderText = "Numer";
            this.Numer.Name = "Numer";
            this.Numer.ReadOnly = true;
            this.Numer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // telefonLabel
            // 
            this.telefonLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.telefonLabel.AutoSize = true;
            this.telefonLabel.Location = new System.Drawing.Point(122, 84);
            this.telefonLabel.Name = "telefonLabel";
            this.telefonLabel.Size = new System.Drawing.Size(45, 13);
            this.telefonLabel.TabIndex = 17;
            this.telefonLabel.Text = "telefon_";
            // 
            // miejscowoscLabel
            // 
            this.miejscowoscLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.miejscowoscLabel.AutoSize = true;
            this.miejscowoscLabel.Location = new System.Drawing.Point(122, 58);
            this.miejscowoscLabel.Name = "miejscowoscLabel";
            this.miejscowoscLabel.Size = new System.Drawing.Size(73, 13);
            this.miejscowoscLabel.TabIndex = 16;
            this.miejscowoscLabel.Text = "miejscowosc_";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wylogujMenuItem,
            this.zakonczMenuItem,
            this.zmianaHaslaMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // wylogujMenuItem
            // 
            this.wylogujMenuItem.Name = "wylogujMenuItem";
            this.wylogujMenuItem.Size = new System.Drawing.Size(63, 20);
            this.wylogujMenuItem.Text = "Wyloguj";
            this.wylogujMenuItem.Click += new System.EventHandler(this.WylogujMenuItemClick);
            // 
            // zakonczMenuItem
            // 
            this.zakonczMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.zakonczMenuItem.Name = "zakonczMenuItem";
            this.zakonczMenuItem.Size = new System.Drawing.Size(63, 20);
            this.zakonczMenuItem.Text = "Zakończ";
            this.zakonczMenuItem.Click += new System.EventHandler(this.ZakonczMenuItemClick);
            // 
            // zmianaHaslaMenuItem
            // 
            this.zmianaHaslaMenuItem.Name = "zmianaHaslaMenuItem";
            this.zmianaHaslaMenuItem.Size = new System.Drawing.Size(89, 20);
            this.zmianaHaslaMenuItem.Text = "Zmiana hasła";
            this.zmianaHaslaMenuItem.Click += new System.EventHandler(this.ZmianaHaslaMenuItemClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Location = new System.Drawing.Point(8, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(311, 156);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dane";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Controls.Add(this.peselLabel, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.plecLabel, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.dataUrodzeniaLabel, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.nazwiskoLabel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.imieLabel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(298, 130);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // peselLabel
            // 
            this.peselLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.peselLabel.AutoSize = true;
            this.peselLabel.Location = new System.Drawing.Point(122, 110);
            this.peselLabel.Name = "peselLabel";
            this.peselLabel.Size = new System.Drawing.Size(32, 13);
            this.peselLabel.TabIndex = 9;
            this.peselLabel.Text = "pesel";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Imie";
            // 
            // plecLabel
            // 
            this.plecLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.plecLabel.AutoSize = true;
            this.plecLabel.Location = new System.Drawing.Point(122, 84);
            this.plecLabel.Name = "plecLabel";
            this.plecLabel.Size = new System.Drawing.Size(33, 13);
            this.plecLabel.TabIndex = 8;
            this.plecLabel.Text = "plec_";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Płeć";
            // 
            // dataUrodzeniaLabel
            // 
            this.dataUrodzeniaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dataUrodzeniaLabel.AutoSize = true;
            this.dataUrodzeniaLabel.Location = new System.Drawing.Point(122, 58);
            this.dataUrodzeniaLabel.Name = "dataUrodzeniaLabel";
            this.dataUrodzeniaLabel.Size = new System.Drawing.Size(82, 13);
            this.dataUrodzeniaLabel.TabIndex = 7;
            this.dataUrodzeniaLabel.Text = "dataUrodzenia_";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "PESEL";
            // 
            // nazwiskoLabel
            // 
            this.nazwiskoLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nazwiskoLabel.AutoSize = true;
            this.nazwiskoLabel.Location = new System.Drawing.Point(122, 32);
            this.nazwiskoLabel.Name = "nazwiskoLabel";
            this.nazwiskoLabel.Size = new System.Drawing.Size(57, 13);
            this.nazwiskoLabel.TabIndex = 6;
            this.nazwiskoLabel.Text = "nazwisko_";
            // 
            // imieLabel
            // 
            this.imieLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.imieLabel.AutoSize = true;
            this.imieLabel.Location = new System.Drawing.Point(122, 6);
            this.imieLabel.Name = "imieLabel";
            this.imieLabel.Size = new System.Drawing.Size(31, 13);
            this.imieLabel.TabIndex = 5;
            this.imieLabel.Text = "imie_";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nazwisko";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data urodzenia";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Location = new System.Drawing.Point(8, 189);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(311, 131);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Adres";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.Controls.Add(this.telefonLabel, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.miejscowoscLabel, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.kodPocztowyLabel, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label12, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label13, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.ulicaLabel, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label14, 0, 3);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(299, 104);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Ulica";
            // 
            // kodPocztowyLabel
            // 
            this.kodPocztowyLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.kodPocztowyLabel.AutoSize = true;
            this.kodPocztowyLabel.Location = new System.Drawing.Point(122, 32);
            this.kodPocztowyLabel.Name = "kodPocztowyLabel";
            this.kodPocztowyLabel.Size = new System.Drawing.Size(31, 13);
            this.kodPocztowyLabel.TabIndex = 15;
            this.kodPocztowyLabel.Text = "kod_";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 32);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Kod pocztowy";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Miejscowość";
            // 
            // ulicaLabel
            // 
            this.ulicaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ulicaLabel.AutoSize = true;
            this.ulicaLabel.Location = new System.Drawing.Point(122, 6);
            this.ulicaLabel.Name = "ulicaLabel";
            this.ulicaLabel.Size = new System.Drawing.Size(35, 13);
            this.ulicaLabel.TabIndex = 14;
            this.ulicaLabel.Text = "ulica_";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 84);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Telefon";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel3);
            this.groupBox3.Location = new System.Drawing.Point(8, 326);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(311, 115);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Karta Czytelnika";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label9, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.statusLabel, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.dataWystawieniaLabel, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.terminWaznosciLabel, 1, 2);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(5, 19);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(299, 78);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Status";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Data wystawienia";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Termin ważności";
            // 
            // statusLabel
            // 
            this.statusLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(122, 6);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(41, 13);
            this.statusLabel.TabIndex = 3;
            this.statusLabel.Text = "status_";
            // 
            // dataWystawieniaLabel
            // 
            this.dataWystawieniaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dataWystawieniaLabel.AutoSize = true;
            this.dataWystawieniaLabel.Location = new System.Drawing.Point(122, 32);
            this.dataWystawieniaLabel.Name = "dataWystawieniaLabel";
            this.dataWystawieniaLabel.Size = new System.Drawing.Size(94, 13);
            this.dataWystawieniaLabel.TabIndex = 4;
            this.dataWystawieniaLabel.Text = "data wystawienia_";
            // 
            // terminWaznosciLabel
            // 
            this.terminWaznosciLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.terminWaznosciLabel.AutoSize = true;
            this.terminWaznosciLabel.Location = new System.Drawing.Point(122, 58);
            this.terminWaznosciLabel.Name = "terminWaznosciLabel";
            this.terminWaznosciLabel.Size = new System.Drawing.Size(88, 13);
            this.terminWaznosciLabel.TabIndex = 5;
            this.terminWaznosciLabel.Text = "terminWaznosci_";
            // 
            // PanelCzytelnika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 661);
            this.Controls.Add(this.wypozyczoneTabControl);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl3);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PanelCzytelnika";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Panel czytelnika";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.wypozyczoneTabControl.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wypozyczeniaGridView)).EndInit();
            this.tabControl3.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ksiazkiGridView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.czasopismaGridView)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.TabControl wypozyczoneTabControl;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.DataGridView wypozyczeniaGridView;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label peselLabel;
        private System.Windows.Forms.Label plecLabel;
        private System.Windows.Forms.Label dataUrodzeniaLabel;
        private System.Windows.Forms.Label nazwiskoLabel;
        private System.Windows.Forms.Label imieLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label telefonLabel;
        private System.Windows.Forms.Label miejscowoscLabel;
        private System.Windows.Forms.Label kodPocztowyLabel;
        private System.Windows.Forms.Label ulicaLabel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Label dataWystawieniaLabel;
        private System.Windows.Forms.Label terminWaznosciLabel;
        private System.Windows.Forms.DataGridView ksiazkiGridView;
        private System.Windows.Forms.DataGridView czasopismaGridView;
        private System.Windows.Forms.ToolStripMenuItem wylogujMenuItem;
        private System.Windows.Forms.Button SzukajKsiazekButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tytulKsiazkiTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox isbnKsiazkiTextBox;
        private System.Windows.Forms.TextBox autorKsiazkiTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox wydawnictwoKsiazkiTextBox;
        private System.Windows.Forms.ComboBox kategoriaComboBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox rokKsiazkiTextBox;
        private System.Windows.Forms.Button ResetujKsiazkiButton;
        private System.Windows.Forms.Button szczegolyKsiazkiButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn tytulWypozyczeniaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sygnaturaWypozyczeniaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataWypColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn terminZwrColumn;
        private System.Windows.Forms.Button resetujCzasopismaButton;
        private System.Windows.Forms.Button szukajCzasopismButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tytulCzasopismaTextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox issnCzasopismaTextBex;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox tematykaComboBox;
        private System.Windows.Forms.ComboBox odbiorcaComboBox;
        private System.Windows.Forms.ComboBox czestotliwoscComboBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button szczegolyCzasopismaButton;
        private System.Windows.Forms.ToolStripMenuItem zakonczMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zmianaHaslaMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn tytulKsiazkiColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn isbKsiazkiColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn autorKsiazkiColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tytulCzasopismaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn issnCzasopismaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numer;
    }
}