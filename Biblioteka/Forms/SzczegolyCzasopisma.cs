﻿using BibliotekaKsiazek;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka.Forms
{
    public partial class SzczegolyCzasopisma : Form
    {
        private Czasopismo czasopismo;
        private List<EgzemplarzCzasopisma> egzemplarze;

        private WCRepository wcRepository;

        public SzczegolyCzasopisma()
        {
            InitializeComponent();
        }

        public void PobierzDaneCzasopisma(Czasopismo czasopismo, List<EgzemplarzCzasopisma> egzemplarze)
        {
            wcRepository = new WCRepository();
            wcRepository.PobierzDane();

            this.czasopismo = czasopismo;
            this.egzemplarze = egzemplarze;

            this.issnCzasopismaLabel.Text = czasopismo.Issn;
            this.tytulCzasopismaLabel.Text = czasopismo.Tytul;
            this.nrCzasopismaLabel.Text = czasopismo.NrCzasopisma;
            this.dataWydLabel.Text = czasopismo.DataWydania.ToShortDateString();
            this.wydawnictwoCzasopismaLabel.Text = czasopismo.Wydawnictwo;
            this.tematykaCzasopismaLabel.Text = czasopismo.Tematyka.Nazwa;
            this.odbiorcaCzasopismaLabel.Text = czasopismo.Odbiorca.Nazwa;
            this.czestotliwoscCzasopismaLabel.Text = czasopismo.Czestotliwosc.Nazwa;

            foreach (var obj in egzemplarze)
            {
                DataGridViewRow row = this.egzemplarzeGridView.RowTemplate.Clone() as DataGridViewRow;
                row.CreateCells(this.egzemplarzeGridView);

                row.Cells[0].Value = obj.Id;
                row.Cells[1].Value = obj.Sygnatura;
                row.Cells[2].Value = (wcRepository.czyEgzemplarzWypozyczony(obj)) ? "Nie" : "Tak";

                this.egzemplarzeGridView.Rows.Add(row);
            }
        }

        private void WypozyczEgzemplarzButtonClick(object sender, EventArgs e)
        {
            PanelCzytelnika czytelnik = new PanelCzytelnika();

            //Zaznaczony egzemplarz
            if (this.egzemplarzeGridView.SelectedRows.Count > 0 || czytelnik.WypozyczeniaCzytelnika.Count() <= 3)
            {
                EgzemplarzCzasopisma selected = this.egzemplarze.Find(k => k.Id == Convert.ToInt64(this.egzemplarzeGridView.SelectedCells[0].Value));

                //Sprawdzamy czy egzemplarz jest dostępny
                if (!wcRepository.czyEgzemplarzWypozyczony(selected) && MessageBox.Show("Wypożyczyć?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    WypozyczenieCzasopisma wypozyczenie = new WypozyczenieCzasopisma();

                    wypozyczenie.EgzemplarzCzasopisma = selected;
                    wypozyczenie.DataWypozyczenia = DateTime.Now;
                    wypozyczenie.TerminZwrotu = DateTime.Now.AddMonths(1);
                    wypozyczenie.KartaCzytelnika = czytelnik.Karta;

                    wcRepository.Dodaj(wypozyczenie);
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Nie można wypożyczyć egzemplarza", "Ostrzeżenie", MessageBoxButtons.OK);
                }
            }
        }

        private void AnulujButtonClick(object sender, EventArgs e)
        {
            Close();
        }
    }
}
