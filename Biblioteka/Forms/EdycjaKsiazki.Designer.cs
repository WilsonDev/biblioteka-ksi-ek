﻿namespace Biblioteka.Forms
{
    partial class EdycjaKsiazki
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.daneTabPage = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.kategoriaComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.autorTextBox = new System.Windows.Forms.TextBox();
            this.rokTextBox = new System.Windows.Forms.TextBox();
            this.wydawnictwoTextBox = new System.Windows.Forms.TextBox();
            this.tytulTextBox = new System.Windows.Forms.TextBox();
            this.isbnTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.egzemplarzeTabPage = new System.Windows.Forms.TabPage();
            this.dodajEgzemplarzButton = new System.Windows.Forms.Button();
            this.usunEgzemplarzButton = new System.Windows.Forms.Button();
            this.egzemplarzeGridView = new System.Windows.Forms.DataGridView();
            this.idColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sygnaturaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dostepnoscColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.anulujButton = new System.Windows.Forms.Button();
            this.zapiszKsiazkeButton = new System.Windows.Forms.Button();
            this.usunKsiazkeButton = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabControl1.SuspendLayout();
            this.daneTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.egzemplarzeTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.egzemplarzeGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.daneTabPage);
            this.tabControl1.Controls.Add(this.egzemplarzeTabPage);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(560, 306);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // daneTabPage
            // 
            this.daneTabPage.Controls.Add(this.pictureBox1);
            this.daneTabPage.Controls.Add(this.tableLayoutPanel1);
            this.daneTabPage.Location = new System.Drawing.Point(4, 22);
            this.daneTabPage.Name = "daneTabPage";
            this.daneTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.daneTabPage.Size = new System.Drawing.Size(552, 280);
            this.daneTabPage.TabIndex = 0;
            this.daneTabPage.Text = "Dane";
            this.daneTabPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Biblioteka.Properties.Resources.book;
            this.pictureBox1.Location = new System.Drawing.Point(442, 168);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.kategoriaComboBox, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.autorTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.rokTextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.wydawnictwoTextBox, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tytulTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.isbnTextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(540, 152);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // kategoriaComboBox
            // 
            this.kategoriaComboBox.DisplayMember = "Nazwa";
            this.kategoriaComboBox.FormattingEnabled = true;
            this.kategoriaComboBox.Location = new System.Drawing.Point(367, 42);
            this.kategoriaComboBox.Name = "kategoriaComboBox";
            this.kategoriaComboBox.Size = new System.Drawing.Size(158, 21);
            this.kategoriaComboBox.TabIndex = 11;
            this.kategoriaComboBox.ValueMember = "Id";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(277, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Kategoria";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Autor";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Rok wydania";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Wydawnictwo";
            // 
            // autorTextBox
            // 
            this.autorTextBox.Location = new System.Drawing.Point(93, 71);
            this.autorTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.autorTextBox.Name = "autorTextBox";
            this.autorTextBox.Size = new System.Drawing.Size(158, 20);
            this.autorTextBox.TabIndex = 8;
            // 
            // rokTextBox
            // 
            this.rokTextBox.Location = new System.Drawing.Point(93, 100);
            this.rokTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.rokTextBox.MaxLength = 4;
            this.rokTextBox.Name = "rokTextBox";
            this.rokTextBox.Size = new System.Drawing.Size(51, 20);
            this.rokTextBox.TabIndex = 9;
            // 
            // wydawnictwoTextBox
            // 
            this.wydawnictwoTextBox.Location = new System.Drawing.Point(93, 129);
            this.wydawnictwoTextBox.Name = "wydawnictwoTextBox";
            this.wydawnictwoTextBox.Size = new System.Drawing.Size(158, 20);
            this.wydawnictwoTextBox.TabIndex = 10;
            // 
            // tytulTextBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tytulTextBox, 4);
            this.tytulTextBox.Location = new System.Drawing.Point(93, 13);
            this.tytulTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.tytulTextBox.Name = "tytulTextBox";
            this.tytulTextBox.Size = new System.Drawing.Size(432, 20);
            this.tytulTextBox.TabIndex = 7;
            // 
            // isbnTextBox
            // 
            this.isbnTextBox.Location = new System.Drawing.Point(93, 42);
            this.isbnTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.isbnTextBox.Name = "isbnTextBox";
            this.isbnTextBox.Size = new System.Drawing.Size(158, 20);
            this.isbnTextBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ISBN";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tytuł";
            // 
            // egzemplarzeTabPage
            // 
            this.egzemplarzeTabPage.Controls.Add(this.dodajEgzemplarzButton);
            this.egzemplarzeTabPage.Controls.Add(this.usunEgzemplarzButton);
            this.egzemplarzeTabPage.Controls.Add(this.egzemplarzeGridView);
            this.egzemplarzeTabPage.Location = new System.Drawing.Point(4, 22);
            this.egzemplarzeTabPage.Name = "egzemplarzeTabPage";
            this.egzemplarzeTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.egzemplarzeTabPage.Size = new System.Drawing.Size(552, 280);
            this.egzemplarzeTabPage.TabIndex = 1;
            this.egzemplarzeTabPage.Text = "Egzemplarze";
            this.egzemplarzeTabPage.UseVisualStyleBackColor = true;
            // 
            // dodajEgzemplarzButton
            // 
            this.dodajEgzemplarzButton.Location = new System.Drawing.Point(394, 252);
            this.dodajEgzemplarzButton.Name = "dodajEgzemplarzButton";
            this.dodajEgzemplarzButton.Size = new System.Drawing.Size(75, 25);
            this.dodajEgzemplarzButton.TabIndex = 2;
            this.dodajEgzemplarzButton.Text = "Dodaj";
            this.dodajEgzemplarzButton.UseVisualStyleBackColor = true;
            this.dodajEgzemplarzButton.Click += new System.EventHandler(this.DodajEgzemplarzButtonClick);
            // 
            // usunEgzemplarzButton
            // 
            this.usunEgzemplarzButton.Location = new System.Drawing.Point(475, 252);
            this.usunEgzemplarzButton.Name = "usunEgzemplarzButton";
            this.usunEgzemplarzButton.Size = new System.Drawing.Size(75, 25);
            this.usunEgzemplarzButton.TabIndex = 1;
            this.usunEgzemplarzButton.Text = "Usuń";
            this.usunEgzemplarzButton.UseVisualStyleBackColor = true;
            this.usunEgzemplarzButton.Click += new System.EventHandler(this.UsunEgzemplarzButtonClick);
            // 
            // egzemplarzeGridView
            // 
            this.egzemplarzeGridView.AllowUserToAddRows = false;
            this.egzemplarzeGridView.AllowUserToDeleteRows = false;
            this.egzemplarzeGridView.AllowUserToResizeColumns = false;
            this.egzemplarzeGridView.AllowUserToResizeRows = false;
            this.egzemplarzeGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.egzemplarzeGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.egzemplarzeGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.egzemplarzeGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idColumn,
            this.sygnaturaColumn,
            this.dostepnoscColumn});
            this.egzemplarzeGridView.Location = new System.Drawing.Point(0, 3);
            this.egzemplarzeGridView.Name = "egzemplarzeGridView";
            this.egzemplarzeGridView.ReadOnly = true;
            this.egzemplarzeGridView.RowHeadersVisible = false;
            this.egzemplarzeGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.egzemplarzeGridView.Size = new System.Drawing.Size(549, 246);
            this.egzemplarzeGridView.TabIndex = 0;
            // 
            // idColumn
            // 
            this.idColumn.HeaderText = "#";
            this.idColumn.Name = "idColumn";
            this.idColumn.ReadOnly = true;
            // 
            // sygnaturaColumn
            // 
            this.sygnaturaColumn.HeaderText = "Sygnatura";
            this.sygnaturaColumn.Name = "sygnaturaColumn";
            this.sygnaturaColumn.ReadOnly = true;
            // 
            // dostepnoscColumn
            // 
            this.dostepnoscColumn.HeaderText = "Dostępny";
            this.dostepnoscColumn.Name = "dostepnoscColumn";
            this.dostepnoscColumn.ReadOnly = true;
            // 
            // anulujButton
            // 
            this.anulujButton.Location = new System.Drawing.Point(11, 324);
            this.anulujButton.Name = "anulujButton";
            this.anulujButton.Size = new System.Drawing.Size(75, 25);
            this.anulujButton.TabIndex = 2;
            this.anulujButton.Text = "Anuluj";
            this.anulujButton.UseVisualStyleBackColor = true;
            this.anulujButton.Click += new System.EventHandler(this.AnulujButtonClick);
            // 
            // zapiszKsiazkeButton
            // 
            this.zapiszKsiazkeButton.Location = new System.Drawing.Point(496, 324);
            this.zapiszKsiazkeButton.Name = "zapiszKsiazkeButton";
            this.zapiszKsiazkeButton.Size = new System.Drawing.Size(75, 25);
            this.zapiszKsiazkeButton.TabIndex = 3;
            this.zapiszKsiazkeButton.Text = "Zapisz";
            this.zapiszKsiazkeButton.UseVisualStyleBackColor = true;
            this.zapiszKsiazkeButton.Click += new System.EventHandler(this.ZapiszKsiazkeButtonClick);
            // 
            // usunKsiazkeButton
            // 
            this.usunKsiazkeButton.Location = new System.Drawing.Point(415, 324);
            this.usunKsiazkeButton.Name = "usunKsiazkeButton";
            this.usunKsiazkeButton.Size = new System.Drawing.Size(75, 25);
            this.usunKsiazkeButton.TabIndex = 4;
            this.usunKsiazkeButton.Text = "Usuń";
            this.usunKsiazkeButton.UseVisualStyleBackColor = true;
            this.usunKsiazkeButton.Click += new System.EventHandler(this.UsunKsiazkeButtonClick);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // EdycjaKsiazki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.usunKsiazkeButton);
            this.Controls.Add(this.zapiszKsiazkeButton);
            this.Controls.Add(this.anulujButton);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EdycjaKsiazki";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edycja książki";
            this.tabControl1.ResumeLayout(false);
            this.daneTabPage.ResumeLayout(false);
            this.daneTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.egzemplarzeTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.egzemplarzeGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage daneTabPage;
        private System.Windows.Forms.TabPage egzemplarzeTabPage;
        private System.Windows.Forms.Button anulujButton;
        private System.Windows.Forms.Button zapiszKsiazkeButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox isbnTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tytulTextBox;
        private System.Windows.Forms.TextBox autorTextBox;
        private System.Windows.Forms.TextBox rokTextBox;
        private System.Windows.Forms.TextBox wydawnictwoTextBox;
        private System.Windows.Forms.ComboBox kategoriaComboBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView egzemplarzeGridView;
        private System.Windows.Forms.Button usunKsiazkeButton;
        private System.Windows.Forms.Button usunEgzemplarzButton;
        private System.Windows.Forms.Button dodajEgzemplarzButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn idColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sygnaturaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dostepnoscColumn;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}