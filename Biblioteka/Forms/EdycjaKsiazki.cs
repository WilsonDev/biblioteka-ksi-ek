﻿using BibliotekaKsiazek;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka.Forms
{
    public partial class EdycjaKsiazki : Form
    {
        private Ksiazka ksiazka;
        private List<EgzemplarzKsiazki> egzemplarze;

        private List<EgzemplarzKsiazki> noweEgzemplarze;
        private List<EgzemplarzKsiazki> usunieteEgzemplarze;

        public EdycjaKsiazki()
        {
            InitializeComponent();

            this.kategoriaComboBox.DataSource = ZagadnieniaRepository.WszystkieKategorie();
            noweEgzemplarze = new List<EgzemplarzKsiazki>();
            usunieteEgzemplarze = new List<EgzemplarzKsiazki>();
        }

        public void PobierzDaneKsiazki(Ksiazka ksiazka, List<EgzemplarzKsiazki> egzemplarze)
        {
            WKRepository wkRepository = new WKRepository();
            wkRepository.PobierzDane();

            this.ksiazka = ksiazka;
            this.egzemplarze = egzemplarze;

            this.isbnTextBox.Text = ksiazka.Isbn;
            this.tytulTextBox.Text = ksiazka.Tytul;
            this.autorTextBox.Text = ksiazka.Autor;
            this.rokTextBox.Text = ksiazka.RokWydania.ToString();
            this.wydawnictwoTextBox.Text = ksiazka.Wydawnictwo;
            this.kategoriaComboBox.SelectedValue = ksiazka.Kategoria.Id;

            this.egzemplarzeGridView.Rows.Clear();

            foreach (var obj in egzemplarze)
            {
                DataGridViewRow row = this.egzemplarzeGridView.RowTemplate.Clone() as DataGridViewRow;
                row.CreateCells(this.egzemplarzeGridView);

                row.Cells[0].Value = obj.Id;
                row.Cells[1].Value = obj.Sygnatura;
                row.Cells[2].Value = (wkRepository.czyEgzemplarzWypozyczony(obj))?"Nie":"Tak";

                this.egzemplarzeGridView.Rows.Add(row);      
            }
        }

        private void ZapiszKsiazkeButtonClick(object sender, EventArgs e)
        {
            KsiazkaRepository ksiazkaRepository = new KsiazkaRepository();
            Ksiazka edited;

            if (ksiazka == null)
                edited = new Ksiazka();
            else
                edited = ksiazka;

            edited.Tytul = this.tytulTextBox.Text;
            edited.Isbn = this.isbnTextBox.Text;
            edited.Autor = this.autorTextBox.Text;
            edited.RokWydania = Convert.ToInt32(this.rokTextBox.Text);
            edited.Wydawnictwo = this.wydawnictwoTextBox.Text;
            edited.Kategoria = (Kategoria)this.kategoriaComboBox.SelectedItem;

            this.tytulTextBox.BackColor = SystemColors.Window;
            this.isbnTextBox.BackColor = SystemColors.Window;
            this.autorTextBox.BackColor = SystemColors.Window;
            this.rokTextBox.BackColor = SystemColors.Window;
            this.wydawnictwoTextBox.BackColor = SystemColors.Window;

            this.errorProvider.Clear();

            if (ksiazka == null)
            {
                if (this.CzyDanePoprawne() && MessageBox.Show("Zapisać książkę", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ksiazkaRepository.Dodaj(edited);
                    this.DialogResult = DialogResult.OK;
                }
            }
            else
            {
                if (this.CzyDanePoprawne() && MessageBox.Show("Zaktualizować dane książki?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ksiazkaRepository.Edytuj(edited);
                    this.DialogResult = DialogResult.OK;

                    EKRepository ekRepository = new EKRepository();
                    WKRepository wkRepository = new WKRepository();
                    
                    foreach (var obj in noweEgzemplarze)
                    {
                        ekRepository.Dodaj(obj);   
                    }
                    foreach (var obj in usunieteEgzemplarze)
                    {
                        ekRepository.Usun(obj);
                        wkRepository.UsunWypozyczeniaEgzemplarza(obj);
                    }
                }
            }  
        }

        private void UsunKsiazkeButtonClick(object sender, EventArgs e)
        {
            KsiazkaRepository ksiazkaRepository = new KsiazkaRepository();
            EKRepository ekRepository = new EKRepository();
            WKRepository wkRepository = new WKRepository();

            if (MessageBox.Show("Usunąć książkę i wszystkie egzemplarze?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ksiazkaRepository.Usun(ksiazka);
                foreach (var obj in egzemplarze) {
                    ekRepository.Usun(obj);
                    wkRepository.UsunWypozyczeniaEgzemplarza(obj);
                }

                this.DialogResult = DialogResult.OK;
            }
        }

        private void UsunEgzemplarzButtonClick(object sender, EventArgs e)
        {
            EKRepository ekRepository = new EKRepository();

            if (MessageBox.Show("Usunąć egzemplarz?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                EgzemplarzKsiazki egzemplarz = egzemplarze.Find(k => k.Id == Convert.ToInt64(egzemplarzeGridView.SelectedCells[0].Value));
                this.usunieteEgzemplarze.Add(egzemplarz);

                egzemplarze.Remove(egzemplarz);
                egzemplarzeGridView.Rows.RemoveAt(this.egzemplarzeGridView.SelectedRows[0].Index);
            }
        }

        private void DodajEgzemplarzButtonClick(object sender, EventArgs e)
        {
            NowyEgzemplarz nowyEgzemplarz = new NowyEgzemplarz(ksiazka);
            if (nowyEgzemplarz.ShowDialog() == DialogResult.OK) //Otwieramy jako okno dialogowe
            {
                this.noweEgzemplarze.Add(nowyEgzemplarz.EgzemplarzKsiazki);

                DataGridViewRow row = this.egzemplarzeGridView.RowTemplate.Clone() as DataGridViewRow;
                row.CreateCells(this.egzemplarzeGridView);

                row.Cells[1].Value = nowyEgzemplarz.EgzemplarzKsiazki.Sygnatura;

                this.egzemplarzeGridView.Rows.Add(row);
            }                
        }

        private void AnulujButtonClick(object sender, EventArgs e)
        {
            Close();
        }

        private bool CzyDanePoprawne()
        {
            bool result = true;

            Regex regTytul;
            regTytul = new Regex("^([A-ZŁŃŚŻŹĘÓ]{1}|[0-9]{1})|([1-9]{1}[0-9]* [A-ZŁŃŚŻŹĘÓ]{1})[a-ząćęłóńśżź]+[ ]?[ - ]?[i]?[z]?[u]?(([A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]+)|([1-9]{1}[0-9]*)|([1-9]{1}[0-9]* [A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]+))*$");
            if (!regTytul.IsMatch(tytulTextBox.Text)) {
                tytulTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(tytulTextBox, "Niepoprawny tytuł");
            }

            Regex regISBN;
            regISBN = new Regex("(^[0-9]{10}$)|(^[0-9]{13}$)");
            if (!regISBN.IsMatch(isbnTextBox.Text)) {
                isbnTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(isbnTextBox, "Niepoprawny ISBN");
            }

            Regex regAutor;
            regAutor = new Regex("^[A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]{1,39}[ ][A-Z]{1}[a-ząćęłóńśżź]{1,39}(( - )?[ ][A-Z]{1}[a-ząćęłóńśżź]{1,39})?$");
            if (!regAutor.IsMatch(autorTextBox.Text)) {
                autorTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(autorTextBox, "Niepoprawny autor");
            }

            Regex regRokWydania;
            int biezacyRok = DateTime.Today.Year;
            if (ParseNInt(rokTextBox.Text) > biezacyRok) {
                regRokWydania = new Regex("^a$");
            }
            else {
                regRokWydania = new Regex("(^19[0-9]{2}$)|(^2[0-9]{3}$)");
            }
            if (!regRokWydania.IsMatch(rokTextBox.Text)) {
                rokTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(rokTextBox, "Niepoprawny rok wydania");
            }

            Regex regWydawnictwo;
            regWydawnictwo = new Regex("^([A-ZŁŃŚŻŹĘÓ]{1})|([1-9]{1}[0-9]* [A-ZŁŃŚŻŹĘÓ]{1})[a-ząćęłóńśżź]+[ ]?[ - ]?[i]?[z]?[u]?(([A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]+)|([1-9]{1}[0-9]*)|([1-9]{1}[0-9]* [A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]+))*$");
            if (!regWydawnictwo.IsMatch(wydawnictwoTextBox.Text)) {
                wydawnictwoTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(wydawnictwoTextBox, "Niepoprawne wydawnictwo");
            }

            return result;
        }

        private int? ParseNInt(string val)
        {
            int i;
            return int.TryParse(val, out i) ? (int?)i : null;
        }
    }
}
