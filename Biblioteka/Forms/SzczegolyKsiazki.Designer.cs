﻿namespace Biblioteka.Forms
{
    partial class SzczegolyKsiazki
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.daneTabPage = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tytulKsiazkiLabel = new System.Windows.Forms.Label();
            this.isbnKsiazkiLabel = new System.Windows.Forms.Label();
            this.autorKsiazkiLabel = new System.Windows.Forms.Label();
            this.rokKsiazkiLabel = new System.Windows.Forms.Label();
            this.wydawnictwoKsiazkiLabel = new System.Windows.Forms.Label();
            this.kategoriaKsiazkiLabel = new System.Windows.Forms.Label();
            this.egzemplarzeTabPage = new System.Windows.Forms.TabPage();
            this.wypozyczEgzemplarzButton = new System.Windows.Forms.Button();
            this.egzemplarzeGridView = new System.Windows.Forms.DataGridView();
            this.idColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sygnaturaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dostepnoscColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.anulujButton = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabControl1.SuspendLayout();
            this.daneTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.egzemplarzeTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.egzemplarzeGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.daneTabPage);
            this.tabControl1.Controls.Add(this.egzemplarzeTabPage);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(560, 306);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // daneTabPage
            // 
            this.daneTabPage.Controls.Add(this.pictureBox1);
            this.daneTabPage.Controls.Add(this.tableLayoutPanel1);
            this.daneTabPage.Location = new System.Drawing.Point(4, 22);
            this.daneTabPage.Name = "daneTabPage";
            this.daneTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.daneTabPage.Size = new System.Drawing.Size(552, 280);
            this.daneTabPage.TabIndex = 0;
            this.daneTabPage.Text = "Dane";
            this.daneTabPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Biblioteka.Properties.Resources.book;
            this.pictureBox1.Location = new System.Drawing.Point(442, 168);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 164F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label6, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tytulKsiazkiLabel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.isbnKsiazkiLabel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.autorKsiazkiLabel, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.rokKsiazkiLabel, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.wydawnictwoKsiazkiLabel, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.kategoriaKsiazkiLabel, 4, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(540, 155);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(277, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Kategoria";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Autor";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Rok wydania";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Wydawnictwo";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ISBN";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tytuł";
            // 
            // tytulKsiazkiLabel
            // 
            this.tytulKsiazkiLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tytulKsiazkiLabel.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.tytulKsiazkiLabel, 4);
            this.tytulKsiazkiLabel.Location = new System.Drawing.Point(93, 18);
            this.tytulKsiazkiLabel.Name = "tytulKsiazkiLabel";
            this.tytulKsiazkiLabel.Size = new System.Drawing.Size(32, 13);
            this.tytulKsiazkiLabel.TabIndex = 0;
            this.tytulKsiazkiLabel.Text = "_tytul";
            // 
            // isbnKsiazkiLabel
            // 
            this.isbnKsiazkiLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.isbnKsiazkiLabel.AutoSize = true;
            this.isbnKsiazkiLabel.Location = new System.Drawing.Point(93, 47);
            this.isbnKsiazkiLabel.Name = "isbnKsiazkiLabel";
            this.isbnKsiazkiLabel.Size = new System.Drawing.Size(32, 13);
            this.isbnKsiazkiLabel.TabIndex = 12;
            this.isbnKsiazkiLabel.Text = "_isbn";
            // 
            // autorKsiazkiLabel
            // 
            this.autorKsiazkiLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.autorKsiazkiLabel.AutoSize = true;
            this.autorKsiazkiLabel.Location = new System.Drawing.Point(93, 76);
            this.autorKsiazkiLabel.Name = "autorKsiazkiLabel";
            this.autorKsiazkiLabel.Size = new System.Drawing.Size(37, 13);
            this.autorKsiazkiLabel.TabIndex = 13;
            this.autorKsiazkiLabel.Text = "_autor";
            // 
            // rokKsiazkiLabel
            // 
            this.rokKsiazkiLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.rokKsiazkiLabel.AutoSize = true;
            this.rokKsiazkiLabel.Location = new System.Drawing.Point(93, 105);
            this.rokKsiazkiLabel.Name = "rokKsiazkiLabel";
            this.rokKsiazkiLabel.Size = new System.Drawing.Size(28, 13);
            this.rokKsiazkiLabel.TabIndex = 14;
            this.rokKsiazkiLabel.Text = "_rok";
            // 
            // wydawnictwoKsiazkiLabel
            // 
            this.wydawnictwoKsiazkiLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.wydawnictwoKsiazkiLabel.AutoSize = true;
            this.wydawnictwoKsiazkiLabel.Location = new System.Drawing.Point(93, 134);
            this.wydawnictwoKsiazkiLabel.Name = "wydawnictwoKsiazkiLabel";
            this.wydawnictwoKsiazkiLabel.Size = new System.Drawing.Size(77, 13);
            this.wydawnictwoKsiazkiLabel.TabIndex = 15;
            this.wydawnictwoKsiazkiLabel.Text = "_wydawnictwo";
            // 
            // kategoriaKsiazkiLabel
            // 
            this.kategoriaKsiazkiLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.kategoriaKsiazkiLabel.AutoSize = true;
            this.kategoriaKsiazkiLabel.Location = new System.Drawing.Point(367, 47);
            this.kategoriaKsiazkiLabel.Name = "kategoriaKsiazkiLabel";
            this.kategoriaKsiazkiLabel.Size = new System.Drawing.Size(57, 13);
            this.kategoriaKsiazkiLabel.TabIndex = 16;
            this.kategoriaKsiazkiLabel.Text = "_kategoria";
            // 
            // egzemplarzeTabPage
            // 
            this.egzemplarzeTabPage.Controls.Add(this.wypozyczEgzemplarzButton);
            this.egzemplarzeTabPage.Controls.Add(this.egzemplarzeGridView);
            this.egzemplarzeTabPage.Location = new System.Drawing.Point(4, 22);
            this.egzemplarzeTabPage.Name = "egzemplarzeTabPage";
            this.egzemplarzeTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.egzemplarzeTabPage.Size = new System.Drawing.Size(552, 280);
            this.egzemplarzeTabPage.TabIndex = 1;
            this.egzemplarzeTabPage.Text = "Egzemplarze";
            this.egzemplarzeTabPage.UseVisualStyleBackColor = true;
            // 
            // wypozyczEgzemplarzButton
            // 
            this.wypozyczEgzemplarzButton.Location = new System.Drawing.Point(475, 252);
            this.wypozyczEgzemplarzButton.Name = "wypozyczEgzemplarzButton";
            this.wypozyczEgzemplarzButton.Size = new System.Drawing.Size(75, 25);
            this.wypozyczEgzemplarzButton.TabIndex = 1;
            this.wypozyczEgzemplarzButton.Text = "Wypożycz";
            this.wypozyczEgzemplarzButton.UseVisualStyleBackColor = true;
            this.wypozyczEgzemplarzButton.Click += new System.EventHandler(this.WypozyczEgzemplarzButtonClick);
            // 
            // egzemplarzeGridView
            // 
            this.egzemplarzeGridView.AllowUserToAddRows = false;
            this.egzemplarzeGridView.AllowUserToDeleteRows = false;
            this.egzemplarzeGridView.AllowUserToResizeColumns = false;
            this.egzemplarzeGridView.AllowUserToResizeRows = false;
            this.egzemplarzeGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.egzemplarzeGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.egzemplarzeGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.egzemplarzeGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idColumn,
            this.sygnaturaColumn,
            this.dostepnoscColumn});
            this.egzemplarzeGridView.Location = new System.Drawing.Point(0, 3);
            this.egzemplarzeGridView.Name = "egzemplarzeGridView";
            this.egzemplarzeGridView.ReadOnly = true;
            this.egzemplarzeGridView.RowHeadersVisible = false;
            this.egzemplarzeGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.egzemplarzeGridView.Size = new System.Drawing.Size(549, 246);
            this.egzemplarzeGridView.TabIndex = 0;
            // 
            // idColumn
            // 
            this.idColumn.HeaderText = "#";
            this.idColumn.Name = "idColumn";
            this.idColumn.ReadOnly = true;
            // 
            // sygnaturaColumn
            // 
            this.sygnaturaColumn.HeaderText = "Sygnatura";
            this.sygnaturaColumn.Name = "sygnaturaColumn";
            this.sygnaturaColumn.ReadOnly = true;
            // 
            // dostepnoscColumn
            // 
            this.dostepnoscColumn.HeaderText = "Dostępny";
            this.dostepnoscColumn.Name = "dostepnoscColumn";
            this.dostepnoscColumn.ReadOnly = true;
            // 
            // anulujButton
            // 
            this.anulujButton.Location = new System.Drawing.Point(11, 324);
            this.anulujButton.Name = "anulujButton";
            this.anulujButton.Size = new System.Drawing.Size(75, 25);
            this.anulujButton.TabIndex = 2;
            this.anulujButton.Text = "Anuluj";
            this.anulujButton.UseVisualStyleBackColor = true;
            this.anulujButton.Click += new System.EventHandler(this.AnulujButtonClick);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // SzczegolyKsiazki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.anulujButton);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SzczegolyKsiazki";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Szczegóły książki";
            this.tabControl1.ResumeLayout(false);
            this.daneTabPage.ResumeLayout(false);
            this.daneTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.egzemplarzeTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.egzemplarzeGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage daneTabPage;
        private System.Windows.Forms.TabPage egzemplarzeTabPage;
        private System.Windows.Forms.Button anulujButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView egzemplarzeGridView;
        private System.Windows.Forms.Button wypozyczEgzemplarzButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn idColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sygnaturaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dostepnoscColumn;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Label tytulKsiazkiLabel;
        private System.Windows.Forms.Label isbnKsiazkiLabel;
        private System.Windows.Forms.Label autorKsiazkiLabel;
        private System.Windows.Forms.Label rokKsiazkiLabel;
        private System.Windows.Forms.Label wydawnictwoKsiazkiLabel;
        private System.Windows.Forms.Label kategoriaKsiazkiLabel;
    }
}