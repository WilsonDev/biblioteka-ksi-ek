﻿using BibliotekaKsiazek;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka.Forms
{
    public partial class EdycjaCzasopisma : Form
    {
        private Czasopismo czasopismo;
        private List<EgzemplarzCzasopisma> egzemplarze;

        private List<EgzemplarzCzasopisma> noweEgzemplarze;
        private List<EgzemplarzCzasopisma> usunieteEgzemplarze;

        public EdycjaCzasopisma()
        {
            InitializeComponent();

            this.tematykaComboBox.DataSource = ZagadnieniaRepository.WszystkieTematyki();
            this.odbiorcaComboBox.DataSource = ZagadnieniaRepository.WszyscyOdbiorcy();
            this.czestotliwoscComboBox.DataSource = ZagadnieniaRepository.WszystkieCzestotliwosci();
            noweEgzemplarze = new List<EgzemplarzCzasopisma>();
            usunieteEgzemplarze = new List<EgzemplarzCzasopisma>();
        }

        public void PobierzDaneCzasopisma(Czasopismo czasopismo, List<EgzemplarzCzasopisma> egzemplarze)
        {
            WCRepository wcRepository = new WCRepository();
            wcRepository.PobierzDane();

            this.czasopismo = czasopismo;
            this.egzemplarze = egzemplarze;

            this.issnTextBox.Text = czasopismo.Issn;
            this.tytulTextBox.Text = czasopismo.Tytul;
            this.nrCzasopismaTextBox.Text = czasopismo.NrCzasopisma;
            this.dateTimePicker.Value = czasopismo.DataWydania;
            this.wydawnictwoTextBox.Text = czasopismo.Wydawnictwo;
            this.tematykaComboBox.SelectedValue = czasopismo.Tematyka.Id;
            this.odbiorcaComboBox.SelectedValue = czasopismo.Odbiorca.Id;
            this.czestotliwoscComboBox.SelectedValue = czasopismo.Czestotliwosc.Id;

            foreach (var obj in egzemplarze)
            {
                DataGridViewRow row = this.egzemplarzeGridView.RowTemplate.Clone() as DataGridViewRow;
                row.CreateCells(this.egzemplarzeGridView);

                row.Cells[0].Value = obj.Id;
                row.Cells[1].Value = obj.Sygnatura;
                row.Cells[2].Value = (wcRepository.czyEgzemplarzWypozyczony(obj)) ? "Nie" : "Tak";

                this.egzemplarzeGridView.Rows.Add(row);
            }
        }

        private void ZapiszCzasopismoButtonClick(object sender, EventArgs e)
        {
            CzasopismoRepository czasopismoRepository = new CzasopismoRepository();
            Czasopismo edited;

            if (czasopismo == null)
                edited = new Czasopismo();
            else
                edited = czasopismo;

            edited.Tytul = this.tytulTextBox.Text;
            edited.Issn = this.issnTextBox.Text;
            edited.NrCzasopisma = this.nrCzasopismaTextBox.Text;
            edited.DataWydania = this.dateTimePicker.Value.Date;
            edited.Wydawnictwo = this.wydawnictwoTextBox.Text;
            edited.Tematyka = (Tematyka)this.tematykaComboBox.SelectedItem;
            edited.Czestotliwosc = (Czestotliwosc)this.czestotliwoscComboBox.SelectedItem;
            edited.Odbiorca = (Odbiorca)this.odbiorcaComboBox.SelectedItem;

            this.tytulTextBox.BackColor = SystemColors.Window;
            this.issnTextBox.BackColor = SystemColors.Window;
            this.nrCzasopismaTextBox.BackColor = SystemColors.Window;
            this.wydawnictwoTextBox.BackColor = SystemColors.Window;

            this.errorProvider.Clear();

            if (czasopismo == null)
            {
                if (this.CzyDanePoprawne() && MessageBox.Show("Zapisać czasopismo", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    czasopismoRepository.Dodaj(edited);
                    this.DialogResult = DialogResult.OK;
                }
            }
            else
            {
                if (this.CzyDanePoprawne() && MessageBox.Show("Zaktualizować dane czasopisma?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    czasopismoRepository.Edytuj(edited);
                    this.DialogResult = DialogResult.OK;

                    ECRepository ecRepository = new ECRepository();
                    WCRepository wcRepository = new WCRepository();

                    foreach (var obj in noweEgzemplarze)
                    {
                        ecRepository.Dodaj(obj);
                    }
                    foreach (var obj in usunieteEgzemplarze)
                    {
                        ecRepository.Usun(obj);
                        wcRepository.UsunWypozyczeniaEgzemplarza(obj);
                    }
                }
            }  
        }

        private void UsunCzasopismoButtonClick(object sender, EventArgs e)
        {
            CzasopismoRepository czasopismoRepository = new CzasopismoRepository();
            ECRepository ecRepository = new ECRepository();
            WCRepository wcRepository = new WCRepository();

            if (MessageBox.Show("Usunąć książkę i wszystkie egzemplarze?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                czasopismoRepository.Usun(czasopismo); //Usuwamy czasopismo
                foreach (var obj in egzemplarze) { //i wszystkie egzemplarze
                    ecRepository.Usun(obj);
                    wcRepository.UsunWypozyczeniaEgzemplarza(obj);
                }

                this.DialogResult = DialogResult.OK;
            }
        }

        private void UsunEgzemplarzButtonClick(object sender, EventArgs e)
        {
            ECRepository ecRepository = new ECRepository();

            if (MessageBox.Show("Usunąć egzemplarz?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                EgzemplarzCzasopisma egzemplarz = egzemplarze.Find(k => k.Id == Convert.ToInt64(egzemplarzeGridView.SelectedCells[0].Value));
                this.usunieteEgzemplarze.Add(egzemplarz);

                egzemplarze.Remove(egzemplarz);
                egzemplarzeGridView.Rows.RemoveAt(this.egzemplarzeGridView.SelectedRows[0].Index);
            }
        }

        private void DodajEgzemplarzButtonClick(object sender, EventArgs e)
        {
            NowyEgzemplarz nowyEgzemplarz = new NowyEgzemplarz(czasopismo);
            if (nowyEgzemplarz.ShowDialog() == DialogResult.OK) //Otwieramy jako okno dialogowe
            {
                this.noweEgzemplarze.Add(nowyEgzemplarz.EgzemplarzCzasopisma);

                DataGridViewRow row = this.egzemplarzeGridView.RowTemplate.Clone() as DataGridViewRow;
                row.CreateCells(this.egzemplarzeGridView);

                row.Cells[1].Value = nowyEgzemplarz.EgzemplarzCzasopisma.Sygnatura;

                this.egzemplarzeGridView.Rows.Add(row);
            }
        }

        private void AnulujButtonClick(object sender, EventArgs e)
        {
            Close();
        }

        private bool CzyDanePoprawne()
        {
            bool result = true;

            Regex regTytul;
            regTytul = new Regex("^([A-ZŁŃŚŻŹĘÓ]|[0-9]{1})|([1-9]{1}[0-9]* [A-ZŁŃŚŻŹĘÓ]{1})[a-ząćęłóńśżź]+[ ]?[ - ]?[i]?[z]?[u]?(([A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]+)|([1-9]{1}[0-9]*)|([1-9]{1}[0-9]* [A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]+))*$");
            if (!regTytul.IsMatch(tytulTextBox.Text)) {
                tytulTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(tytulTextBox, "Niepoprawne imię");
            }
            Regex regISSN;
            regISSN = new Regex("(^[0-9]{8}$)|(^[0-9]{7}$)|(^[0-9]{7}X{1}$)");
            if (!regISSN.IsMatch(issnTextBox.Text)) {
                issnTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(issnTextBox, "Niepoprawne imię");
            }
            Regex regNrCzasopisma;
            regNrCzasopisma = new Regex("(^[0-9/()]*$)");
            if (!regNrCzasopisma.IsMatch(nrCzasopismaTextBox.Text)) {
                nrCzasopismaTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(nrCzasopismaTextBox, "Niepoprawne imię");
            }
            Regex regWydawnictwo;
            regWydawnictwo = new Regex("^([A-ZŁŃŚŻŹĘÓ]{1})|([1-9]{1}[0-9]* [A-ZŁŃŚŻŹĘÓ]{1})[a-ząćęłóńśżź]+[ ]?[ - ]?[i]?[z]?[u]?(([A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]+)|([1-9]{1}[0-9]*)|([1-9]{1}[0-9]* [A-ZŁŃŚŻŹĘÓ]{1}[a-ząćęłóńśżź]+))*$");
            if (!regWydawnictwo.IsMatch(wydawnictwoTextBox.Text)) {
                wydawnictwoTextBox.BackColor = Color.LightPink;
                result = false;
                errorProvider.SetError(wydawnictwoTextBox, "Niepoprawne imię");
            }
            DateTime DataWydania = dateTimePicker.Value;
            DateTime biezacaData = DateTime.Today;
            if (DataWydania > biezacaData) {
                result = false;
                errorProvider.SetError(dateTimePicker, "Niepoprawne imię");
            }

            return result;
        }
    }
}
