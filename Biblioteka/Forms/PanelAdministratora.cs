﻿using BibliotekaKsiazek;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biblioteka.Baza;
using System.Globalization;

namespace Biblioteka.Forms
{
    public partial class PanelAdministratora : Form
    {
        private CzytelnikRepository czytelnikRepository;
        private KartaCzytelnikaRepository kartaCzytelnikaRepository;

        private KsiazkaRepository ksiazkaRepository;
        private CzasopismoRepository czasopismoRepository;

        private WCRepository wcRepository;
        private WKRepository wkRepository;

        private ECRepository ecRepository;
        private EKRepository ekRepository;

        public PanelAdministratora()
        {
            InitializeComponent();

            czytelnikRepository = new CzytelnikRepository();
            kartaCzytelnikaRepository = new KartaCzytelnikaRepository();
            ksiazkaRepository = new KsiazkaRepository();
            czasopismoRepository = new CzasopismoRepository();
            wcRepository = new WCRepository();
            wkRepository = new WKRepository();
            ecRepository = new ECRepository();
            ekRepository = new EKRepository();

            PobierzDaneRepozytorium(); //Pobieramy dane wszystkich repozytoriów

            //Pobieramy dane do kontrolek
            LoadKsiazkiGridView();
            LoadCzasopismaGridView();
            LoadCzytelnicyGridView();
            LoadWypozyczeniaGridView();
            LoadNajczesciejWybieraneGridView();

            LoadKategoriaComboBox();
            LoadTematykaComboBox();
            LoadCzestotliwoscComboBox();
            LoadOdbiorcaComboBox();
            LoadCzestotliwoscUkazywaniaSieComboBox();
            LoadOdbiorcaComboBox2();

            LoadGrupaWiekowaComboBox();
            LoadPlecComboBox();           

            this.toolStripStatusLabel.Text = "Zalogowano jako administrator";
        }

        //Pobieramy dane wszystkich Repository

        private void PobierzDaneRepozytorium()
        {
            czytelnikRepository.PobierzDane();
            kartaCzytelnikaRepository.PobierzDane();
            ksiazkaRepository.PobierzDane();
            czasopismoRepository.PobierzDane();
            wcRepository.PobierzDane();
            wkRepository.PobierzDane();
            ecRepository.PobierzDane();
            ekRepository.PobierzDane();
        }

        private void PobierzDaneGridBox()
        {
            LoadCzytelnicyGridView();
            LoadKsiazkiGridView();
            LoadCzasopismaGridView();
            LoadWypozyczeniaGridView();
            LoadNajczesciejWybieraneGridView();
        }

        private void ObslugaWidocznosciDlaCzasopism()
        {
            if (this.najczesciejWybieraneComboBox.SelectedItem != null)
            {
                string wyb = this.najczesciejWybieraneComboBox.SelectedItem.ToString();
                if (wyb == "tematyka czasopism" || wyb == "tytuł czasopisma")
                {
                    this.czestotliwoscUkazywaniaSieComboBox.Enabled = true;
                    this.odbiorcaComboBox2.Enabled = true;
                }
                else if (wyb == "kategoria książek" || wyb == "tytuł książki")
                {
                    this.czestotliwoscUkazywaniaSieComboBox.Enabled = false;
                    this.odbiorcaComboBox2.Enabled = false;
                }
            }
        }

        private void LoadKategoriaComboBox()
        {
            this.kategoriaComboBox.DataSource = ZagadnieniaRepository.WszystkieKategorie();
            this.kategoriaComboBox.SelectedValue = -1;
        }

        private void LoadTematykaComboBox()
        {
            this.tematykaComboBox.DataSource = ZagadnieniaRepository.WszystkieTematyki();
            this.tematykaComboBox.SelectedValue = -1;
        }

        private void LoadCzestotliwoscComboBox()
        {
            this.czestotliwoscComboBox.DataSource = ZagadnieniaRepository.WszystkieCzestotliwosci();
            this.czestotliwoscComboBox.SelectedValue = -1;
        }

        private void LoadOdbiorcaComboBox()
        {
            this.odbiorcaComboBox.DataSource = ZagadnieniaRepository.WszyscyOdbiorcy();
            this.odbiorcaComboBox.SelectedValue = -1;
        }

        private void LoadCzestotliwoscUkazywaniaSieComboBox()
        {
            this.czestotliwoscUkazywaniaSieComboBox.DataSource = ZagadnieniaRepository.WszystkieCzestotliwosci();
            this.czestotliwoscUkazywaniaSieComboBox.SelectedValue = -1;
        }

        private void LoadOdbiorcaComboBox2()
        {
            this.odbiorcaComboBox2.DataSource = ZagadnieniaRepository.WszyscyOdbiorcy();
            this.odbiorcaComboBox2.SelectedValue = -1;
        }

        private void LoadGrupaWiekowaComboBox()
        {
            this.grupaWiekowaComboBox.SelectedValue = -1;
        }

        private void LoadPlecComboBox()
        {
            this.plecComboBox.SelectedValue = -1;
        }

        //Dane do GridView Czytelników
        private void LoadCzytelnicyGridView()
        {
            this.czytelnicyGridView.Rows.Clear();

            foreach (var obj in kartaCzytelnikaRepository.Lista)
            {
                IEnumerable<Wypozyczenie> lista1 = wkRepository.WypozyczonePoCzytelniku(obj); //Pobieramy jego wypożyczenia
                IEnumerable<Wypozyczenie> lista2 = wcRepository.WypozyczonePoCzytelniku(obj);
                var lista = lista1.Cast<Wypozyczenie>().Concat(lista2.Cast<Wypozyczenie>()); //Łączymy listy

                DataGridViewRow row = this.czytelnicyGridView.RowTemplate.Clone() as DataGridViewRow;
                row.CreateCells(this.czytelnicyGridView);

                row.Cells[0].Value = obj.Czytelnik.Id;
                row.Cells[1].Value = obj.Czytelnik.Imie;
                row.Cells[2].Value = obj.Czytelnik.Nazwisko;
                row.Cells[3].Value = obj.Status;
                row.Cells[4].Value = lista.Count();

                if (obj.PrzekroczonoTermin())
                    row.DefaultCellStyle.BackColor = Color.Pink; 

                this.czytelnicyGridView.Rows.Add(row);
            }

            this.czytelnicyGridView.DoubleBuffered(true); //Poprawienie płynności przewijania listy
        }

        //Dane do GridView Książek
        private void LoadKsiazkiGridView()
        {
            this.ksiazkiGridView.AutoGenerateColumns = false;
            this.ksiazkiGridView.DataSource = ksiazkaRepository.Lista;
            this.ksiazkiGridView.DoubleBuffered(true); //Poprawienie płynności przewijania listy
        }

        //Dane do GridView Czasopism
        private void LoadCzasopismaGridView()
        {
            this.czasopismaGridView.AutoGenerateColumns = false;
            this.czasopismaGridView.DataSource = czasopismoRepository.Lista;
            this.czasopismaGridView.DoubleBuffered(true); //Poprawienie płynności przewijania listy
        }

        //Dane do GridView Wypozyczen
        private void LoadWypozyczeniaGridView()
        {
            this.wypozyczeniaGridView.Rows.Clear();

            IEnumerable<Wypozyczenie> lista1 = wkRepository.Wypozyczone();
            IEnumerable<Wypozyczenie> lista2 = wcRepository.Wypozyczone();
            var lista = lista1.Cast<Wypozyczenie>().Concat(lista2.Cast<Wypozyczenie>()); //Łączymy listy

            foreach (var obj in lista.OrderByDescending(k => k.TerminZwrotu).ToList())
            {
                DataGridViewRow row = this.wypozyczeniaGridView.RowTemplate.Clone() as DataGridViewRow;
                row.CreateCells(this.wypozyczeniaGridView);

                row.Cells[0].Value = obj.ToString();
                row.Cells[1].Value = obj.Tytul();
                row.Cells[2].Value = obj.Sygnatura();
                row.Cells[3].Value = obj.DataWypozyczenia.ToShortDateString();
                row.Cells[4].Value = obj.TerminZwrotu.ToShortDateString();
                row.Cells[5].Value = obj.IdCzytelnika(); //Kolumna ukryta. Wartość potrzebna to operacji dodawania/edycji/usuwania

                if (obj.PrzekroczonoTermin())
                    row.DefaultCellStyle.BackColor = Color.Pink; 

                this.wypozyczeniaGridView.Rows.Add(row);
            }

            this.wypozyczeniaGridView.DoubleBuffered(true);
        }

        private void LoadNajczesciejWybieraneGridView()
        {
            this.najczesciejWybieraneComboBox.SelectedIndex = -1;
            this.liczbaMiejscWRankinguComboBox.SelectedIndex = -1;
            this.grupaWiekowaComboBox.SelectedIndex = -1;
            this.plecComboBox.SelectedIndex = -1;
            this.czestotliwoscUkazywaniaSieComboBox.SelectedIndex = -1;
            this.odbiorcaComboBox2.SelectedIndex = -1;

            this.najczesciejWybieraneGridView.Rows.Clear();

            this.najczesciejWybieraneGridView.DoubleBuffered(true); //Poprawienie płynności przewijania listy
        }

        //Otwiera formatkę EdycjaCzytelnika
        private void EdycjaCzytelnikaButtonClick(object sender, EventArgs e)
        {
            EdycjaCzytelnika(Convert.ToInt64(this.czytelnicyGridView.SelectedCells[0].Value));
        }

        private void EdycjaCzytelnika(long id, int flag = 0)
        {
            Czytelnik selected = this.czytelnikRepository.PoId(id); //Zaznaczony czytelnik
            KartaCzytelnika karta = this.kartaCzytelnikaRepository.PoCzytelniku(selected); //Pobieramy jego kartę

            IEnumerable<Wypozyczenie> lista1 = wkRepository.WypozyczonePoCzytelniku(karta); //Pobieramy jego wypożyczenia
            IEnumerable<Wypozyczenie> lista2 = wcRepository.WypozyczonePoCzytelniku(karta);
            var lista = lista1.Cast<Wypozyczenie>().Concat(lista2.Cast<Wypozyczenie>()); //Łączymy listy

            EdycjaCzytelnika edycjaCzytelnika = new EdycjaCzytelnika();
            edycjaCzytelnika.PobierzDaneCzytelnika(selected, karta, lista, flag); //Przekazujemy dane do formatki

            if (edycjaCzytelnika.ShowDialog() == DialogResult.OK) //Otwieramy jako okno dialogowe
            {
                this.toolStripStatusLabel.Text = "Zaktualizowano dane czytelnika";

                PobierzDaneRepozytorium();
                PobierzDaneGridBox();
            }
            edycjaCzytelnika.TopMost = true;
        }

        //Otwiera formatkę EdycjaKsiazki
        private void EdytujKsiazkeButtonClick(object sender, EventArgs e)
        {
            Ksiazka selected = (Ksiazka)this.ksiazkiGridView.CurrentRow.DataBoundItem; //Zaznaczona książka
            List<EgzemplarzKsiazki> egzemplarze = this.ekRepository.PoKsiazce(selected); //Pobieramy jej egzemplarze

            EdycjaKsiazki edycjaKsiazki = new EdycjaKsiazki();
            edycjaKsiazki.PobierzDaneKsiazki(selected, egzemplarze);

            if (edycjaKsiazki.ShowDialog() == DialogResult.OK)
            {
                this.toolStripStatusLabel.Text = "Zaktualizowano dane książki";

                PobierzDaneRepozytorium();
                PobierzDaneGridBox();
            }
            edycjaKsiazki.TopMost = true;
        }

        //Otwiera formatkę EdycjęCzasopisma
        private void EdytujCzasopismoButtonClick(object sender, EventArgs e)
        {
            Czasopismo selected = (Czasopismo)this.czasopismaGridView.CurrentRow.DataBoundItem;
            List<EgzemplarzCzasopisma> egzemplarze = this.ecRepository.PoCzasopismie(selected);

            EdycjaCzasopisma edycjaCzasopisma = new EdycjaCzasopisma();
            edycjaCzasopisma.PobierzDaneCzasopisma(selected, egzemplarze);

            if (edycjaCzasopisma.ShowDialog() == DialogResult.OK)
            {
                this.toolStripStatusLabel.Text = "Zaktualizowano dane czasopisma";

                PobierzDaneRepozytorium();
                PobierzDaneGridBox();
            }
            edycjaCzasopisma.TopMost = true;
        }

        private void DodajCzytelnikaButtonClick(object sender, EventArgs e)
        {
            EdycjaCzytelnika edycjaCzytelnika = new EdycjaCzytelnika();

            if (edycjaCzytelnika.ShowDialog() == DialogResult.OK) //Otwieramy jako okno dialogowe
            {
                this.toolStripStatusLabel.Text = "Dodano czytelnika";

                PobierzDaneRepozytorium();
                PobierzDaneGridBox();
            }
        }

        private void DodajKsiazkeButtonClick(object sender, EventArgs e)
        {
            EdycjaKsiazki edycjaKsiazki = new EdycjaKsiazki();

            if (edycjaKsiazki.ShowDialog() == DialogResult.OK) //Otwieramy jako okno dialogowe
            {
                this.toolStripStatusLabel.Text = "Dodano książkę";

                PobierzDaneRepozytorium();
                PobierzDaneGridBox();
            }
        }

        private void DodajCzasopismoButtonClick(object sender, EventArgs e)
        {
            EdycjaCzasopisma edycjaCzasopisma = new EdycjaCzasopisma();

            if (edycjaCzasopisma.ShowDialog() == DialogResult.OK) //Otwieramy jako okno dialogowe
            {
                this.toolStripStatusLabel.Text = "Dodano czasopismo";

                PobierzDaneRepozytorium();
                PobierzDaneGridBox();
            }
        }

        private void WypozyczenieButtonClick(object sender, EventArgs e)
        {
            EdycjaCzytelnika(Convert.ToInt64(this.wypozyczeniaGridView.SelectedCells[5].Value), 1);
        }

        private void SzukajCzytelnikowButtonClick(object sender, EventArgs e)
        {
            this.czytelnicyGridView.Rows.Clear(); //Czyścimy listę

            string imie = this.imieTextBox.Text;
            string status = "";
            if (this.statusComboBox.SelectedItem != null)
                status = this.statusComboBox.SelectedItem.ToString();

            List<KartaCzytelnika> wynik = kartaCzytelnikaRepository.Wyszukaj(imie, status);

            foreach (var obj in wynik)
            {
                IEnumerable<Wypozyczenie> lista1 = wkRepository.WypozyczonePoCzytelniku(obj); //Pobieramy jego wypożyczenia
                IEnumerable<Wypozyczenie> lista2 = wcRepository.WypozyczonePoCzytelniku(obj);
                var lista = lista1.Cast<Wypozyczenie>().Concat(lista2.Cast<Wypozyczenie>()); //Łączymy listy

                DataGridViewRow row = this.czytelnicyGridView.RowTemplate.Clone() as DataGridViewRow;
                row.CreateCells(this.czytelnicyGridView);

                row.Cells[0].Value = obj.Czytelnik.Id;
                row.Cells[1].Value = obj.Czytelnik.Imie;
                row.Cells[2].Value = obj.Czytelnik.Nazwisko;
                row.Cells[3].Value = obj.Status;
                row.Cells[4].Value = lista.Count();

                if (obj.PrzekroczonoTermin())
                    row.DefaultCellStyle.BackColor = Color.Pink;

                this.czytelnicyGridView.Rows.Add(row);
            }

            //this.czytelnicyGridView.DataSource = lista;
        }

        private void ResetujCzytelnikowButtonClick(object sender, EventArgs e)
        {
            this.imieTextBox.Text = String.Empty;

            this.czytelnicyGridView.Rows.Clear(); //Czyścimy listę
            LoadCzytelnicyGridView();
        }

        private void SzukajKsiazekButtonClick(object sender, EventArgs e)
        {
            string isbn = this.isbnKsiazkiTextBox.Text;
            string tytul = this.tytulKsiazkiTextBox.Text;
            string autor = this.autorKsiazkiTextBox.Text;
            int? rok = ParseNInt(this.rokKsiazkiTextBox.Text);
            string wydawnictwo = this.wydawnictwoKsiazkiTextBox.Text;

            Kategoria kategoria = (Kategoria)this.kategoriaComboBox.SelectedItem; 

            List<Ksiazka> lista = this.ksiazkaRepository.Wyszukaj(isbn, tytul, autor, rok, wydawnictwo, kategoria);

            this.ksiazkiGridView.DataSource = lista;
        }

        private void ResetujKsiazkiButtonClick(object sender, EventArgs e)
        {
            this.tytulKsiazkiTextBox.Text = String.Empty;
            this.isbnKsiazkiTextBox.Text = String.Empty;
            this.autorKsiazkiTextBox.Text = String.Empty;
            this.rokKsiazkiTextBox.Text = String.Empty;
            this.wydawnictwoKsiazkiTextBox.Text = String.Empty;

            this.kategoriaComboBox.SelectedValue = -1;

            LoadKsiazkiGridView();
        }

        private void SzukajCzasopismButtonClick(object sender, EventArgs e)
        {
            string issn = this.issnCzasopismaTextBex.Text;
            string tytul = this.tytulCzasopismaTextBox.Text;
            string wydawnictwo = this.wydawnictwoCzasopismaTextBox.Text;

            Tematyka tematyka = (Tematyka)this.tematykaComboBox.SelectedItem;
            Czestotliwosc czestotliwosc = (Czestotliwosc)this.czestotliwoscComboBox.SelectedItem;
            Odbiorca odbiorca = (Odbiorca)this.odbiorcaComboBox.SelectedItem;

            List<Czasopismo> lista = this.czasopismoRepository.Wyszukaj(issn, tytul, dateTimePicker1.Value, 
                dateTimePicker2.Value, wydawnictwo, tematyka, czestotliwosc, odbiorca);

            this.czasopismaGridView.DataSource = lista;
        }

        private void ResetujCzasopismaButtonClick(object sender, EventArgs e)
        {
            this.tytulCzasopismaTextBox.Text = String.Empty;
            this.issnCzasopismaTextBex.Text = String.Empty;
            this.wydawnictwoCzasopismaTextBox.Text = String.Empty;

            dateTimePicker1.Value = new DateTime(1900, 1, 1);
            dateTimePicker2.Value = DateTime.Now;

            this.tematykaComboBox.SelectedValue = -1;
            this.czestotliwoscComboBox.SelectedValue = -1;
            this.odbiorcaComboBox.SelectedValue = -1;

            LoadCzasopismaGridView();
        }

        private void SzukajWypozyczenButtonClick(object sender, EventArgs e)
        {
            IEnumerable<Wypozyczenie> lista1;
            IEnumerable<Wypozyczenie> lista2;

            DateTime dataWyp = this.dateTimePicker3.Value;
            DateTime terminZwr = this.dateTimePicker4.Value;

            if (radioButton1.Enabled)
            {
                lista1 = wkRepository.PoDacieWypozyczenia(dataWyp);
                lista2 = wcRepository.PoDacieWypozyczenia(dataWyp);
            }
            else
            {
                lista1 = wkRepository.PoTerminieZwrotu(terminZwr);
                lista2 = wcRepository.PoTerminieZwrotu(terminZwr);
            }

            this.wypozyczeniaGridView.Rows.Clear();

            var lista = lista1.Cast<Wypozyczenie>().Concat(lista2.Cast<Wypozyczenie>()); //Łączymy listy

            foreach (var obj in lista.OrderByDescending(k => k.TerminZwrotu).ToList())
            {
                DataGridViewRow row = this.wypozyczeniaGridView.RowTemplate.Clone() as DataGridViewRow;
                row.CreateCells(this.wypozyczeniaGridView);

                row.Cells[0].Value = obj.ToString();
                row.Cells[1].Value = obj.Tytul();
                row.Cells[2].Value = obj.Sygnatura();
                row.Cells[3].Value = obj.DataWypozyczenia.ToShortDateString();
                row.Cells[4].Value = obj.TerminZwrotu.ToShortDateString();
                //row.Cells[5].Value = obj.Id(); //Kolumna ukryta. Wartość potrzebna to operacji dodawania/edycji/usuwania
                //row.Cells[6].Value = obj.GetType(); //Kolumna ukryta. Wartość potrzebna to operacji dodawania/edycji/usuwania

                if (obj.PrzekroczonoTermin())
                    row.DefaultCellStyle.BackColor = Color.Pink;

                this.wypozyczeniaGridView.Rows.Add(row);
            }
        }

        private void ResetujWypozyczeniaButtonClick(object sender, EventArgs e)
        {
            LoadWypozyczeniaGridView();
        }

        private void RadioButton2CheckedChanged(object sender, EventArgs e)
        {
            this.dateTimePicker3.Enabled = false;
            this.dateTimePicker4.Enabled = true;
        }

        private void RadioButton1CheckedChanged(object sender, EventArgs e)
        {
            this.dateTimePicker3.Enabled = true;
            this.dateTimePicker4.Enabled = false;
        }

        //Analiza stanu czytelnictwa
        private void WyswietlNajczesciejWybieraneButtonClick(object sender, EventArgs e)
        {
            this.najczesciejWybieraneGridView.Rows.Clear();

            int minRok = 0, maxRok = 0, i = 0, liczbaMiejscWRankingu = 0;
            string plec = "", grupaWiekowa = "", zapytanie = "", minData = "", maxData = "", najczesciejWybierane = "", czestotliwosc = "", odbiorca = "";
            DateTime dataPocz = this.dateTimePicker7.Value;
            DateTime dataKon = this.dateTimePicker8.Value;

            if (!string.IsNullOrEmpty(this.odbiorcaComboBox2.Text))
            {
                Odbiorca odb = (Odbiorca)this.odbiorcaComboBox2.SelectedItem;
                odbiorca = odb.Id.ToString();
            }

            if (!string.IsNullOrEmpty(this.czestotliwoscUkazywaniaSieComboBox.Text))
            {
                Czestotliwosc czest = (Czestotliwosc)this.czestotliwoscUkazywaniaSieComboBox.SelectedItem;
                czestotliwosc = czest.Id.ToString();
            }

            if (!string.IsNullOrEmpty(this.plecComboBox.Text))
            {
                plec = this.plecComboBox.SelectedItem.ToString();

                if (plec == "kobieta") {
                    plec = "K";
                }
                else {
                    plec = "M";
                }
            }

            if (!string.IsNullOrEmpty(this.grupaWiekowaComboBox.Text))
            {
                grupaWiekowa = this.grupaWiekowaComboBox.SelectedItem.ToString();
                string od_ = grupaWiekowa.Substring(0, 2), do_ = grupaWiekowa.Substring(5, 2);
                int biezacyRok = DateTime.Now.Year; //SQLite nie udostępnia funkcji do wyciągnięcia roku z przechowywanej w bazie daty
                int biezacyMiesiac = DateTime.Now.Month;
                int biezacyDzien = DateTime.Now.Day;
                minRok = biezacyRok - Convert.ToInt32(do_);
                maxRok = biezacyRok - Convert.ToInt32(od_);

                if (biezacyMiesiac >= 1 && biezacyMiesiac <= 9)
                {
                    minData = string.Concat(minRok + "-" + "0" + biezacyMiesiac + "-" + biezacyDzien);
                    maxData = string.Concat(maxRok + "-" + "0" + biezacyMiesiac + "-" + biezacyDzien);
                }
                else
                {
                    minData = string.Concat(minRok + "-" + biezacyMiesiac + "-" + biezacyDzien);
                    maxData = string.Concat(maxRok + "-" + biezacyMiesiac + "-" + biezacyDzien);
                }
            }

            if (string.IsNullOrEmpty(this.liczbaMiejscWRankinguComboBox.Text))
            {
                MessageBox.Show("Proszę wybrać liczbę miejsc w rankingu.", "Analiza stanu czytelnictwa");
            }
            else
            {
                liczbaMiejscWRankingu = Convert.ToInt32(this.liczbaMiejscWRankinguComboBox.SelectedItem.ToString());
            }

            if (string.IsNullOrEmpty(najczesciejWybieraneComboBox.Text))
            {
                MessageBox.Show("Proszę wybrać rodzaj zbiorów bibliotecznych.", "Analiza stanu czytelnictwa");
            }
            else
            {
                najczesciejWybierane = this.najczesciejWybieraneComboBox.SelectedItem.ToString();
            }

            List<NajczesciejWybierany> listaNajczesciejWybieranych = new List<NajczesciejWybierany>();

            BazaSQLite baza = new BazaSQLite();
            DataTable dt = new DataTable();

            switch(najczesciejWybierane)
            {
                case "kategoria książek":
                    zapytanie = String.Format("select kat.nazwa, count(kat.id_kategorii) as liczba from ((((Ksiazki as k inner join Karty_Egzemplarzy_Ksiazki as kek on k.id_ksiazki = kek.id_ksiazki) inner join Karty_Wypozyczen_Ksiazki as kwk on kek.id_egzemplarza = kwk.id_egzemplarza) inner join Kategorie as kat on k.id_kategorii = kat.id_kategorii) inner join Karty as kar on kwk.id_karty = kar.id_karty) inner join Czytelnicy as czyt on kar.id_czytelnika = czyt.id_czytelnika where {0} and {1} and {2} group by kat.id_kategorii order by liczba desc", (plec != "") ? "czyt.plec = \"" + plec + "\"" : "1=1", (grupaWiekowa != "") ? "czyt.data_urodzenia between date(\"" + minData + "\") and date(\"" + maxData + "\")" : "1=1", (dataPocz != null && dataKon != null) ? "kwk.data_wypozyczenia between date(\"" + dataPocz + "\") and date(\"" + dataKon + "\")" : "1=1");
                    break;
                case "tematyka czasopism":
                    zapytanie = String.Format("select tem.nazwa, count(tem.id_tematyki) as liczba from ((((Czasopisma as cz inner join Karty_Egzemplarzy_Czasopisma as kec on cz.id_czasopisma = kec.id_czasopisma) inner join Karty_Wypozyczen_Czasopisma as kwc on kec.id_egzemplarza = kwc.id_egzemplarza) inner join Tematyki as tem on cz.id_tematyki = tem.id_tematyki) inner join Karty as kar on kwc.id_karty = kar.id_karty) inner join Czytelnicy as czyt on kar.id_czytelnika = czyt.id_czytelnika where {0} and {1} and {2} and {3} and {4} group by tem.id_tematyki order by liczba desc", (plec != "") ? "czyt.plec = \"" + plec + "\"" : "1=1", (grupaWiekowa != "") ? "czyt.data_urodzenia between date(\"" + minData + "\") and date(\"" + maxData + "\")" : "1=1", (czestotliwosc != "") ? "cz.id_czestotliwosc = \"" + czestotliwosc + "\"" : "1=1", (odbiorca != "") ? "cz.id_odbiorcy = \"" + odbiorca + "\"" : "1=1", (dataPocz != null && dataKon != null) ? "kwc.data_wypozyczenia between date(\"" + dataPocz + "\") and date(\"" + dataKon + "\")" : "1=1");
                    break;
                case "tytuł książki":
                    zapytanie = String.Format("select k.tytul as nazwa, count(k.tytul) as liczba from (((Ksiazki as k inner join Karty_Egzemplarzy_Ksiazki as kek on k.id_ksiazki = kek.id_ksiazki) inner join Karty_Wypozyczen_Ksiazki as kwk on kek.id_egzemplarza = kwk.id_egzemplarza) inner join Karty as kar on kwk.id_karty = kar.id_karty) inner join Czytelnicy as czyt on kar.id_czytelnika = czyt.id_czytelnika where {0} and {1} and {2} group by k.tytul order by liczba desc", (plec != "") ? "czyt.plec = \"" + plec + "\"" : "1=1", (grupaWiekowa != "") ? "czyt.data_urodzenia between date(\"" + minData + "\") and date(\"" + maxData + "\")" : "1=1", (dataPocz != null && dataKon != null) ? "kwk.data_wypozyczenia between date(\"" + dataPocz + "\") and date(\"" + dataKon + "\")" : "1=1");
                    break;
                case "tytuł czasopisma":
                    zapytanie = String.Format("select cz.tytul as nazwa, count(cz.tytul) as liczba from (((Czasopisma as cz inner join Karty_Egzemplarzy_Czasopisma as kec on cz.id_czasopisma = kec.id_czasopisma) inner join Karty_Wypozyczen_Czasopisma as kwc on kec.id_egzemplarza = kwc.id_egzemplarza) inner join Karty as kar on kwc.id_karty = kar.id_karty) inner join Czytelnicy as czyt on kar.id_czytelnika = czyt.id_czytelnika where {0} and {1} and {2} and {3} and {4} group by cz.tytul order by liczba desc", (plec != "") ? "czyt.plec = \"" + plec + "\"" : "1=1", (grupaWiekowa != "") ? "czyt.data_urodzenia between date(\"" + minData + "\") and date(\"" + maxData + "\")" : "1=1", (czestotliwosc != "") ? "cz.id_czestotliwosc = \"" + czestotliwosc + "\"" : "1=1", (odbiorca != "") ? "cz.id_odbiorcy = \"" + odbiorca + "\"" : "1=1", (dataPocz != null && dataKon != null) ? "kwc.data_wypozyczenia between date(\"" + dataPocz + "\") and date(\"" + dataKon + "\")" : "1=1");
                    break;
            }

            dt = baza.GetDataTable(zapytanie);

            foreach (DataRow row in dt.Rows)
            {
                if (i < liczbaMiejscWRankingu)
                {
                    i++;
                    NajczesciejWybierany wyb = new NajczesciejWybierany();
                    wyb.Pozycja = i;
                    wyb.Nazwa = row["nazwa"].ToString();
                    wyb.LiczbaWypozyczen = Convert.ToInt32(row["liczba"]);

                    listaNajczesciejWybieranych.Add(wyb);

                    DataGridViewRow gridRow = this.najczesciejWybieraneGridView.RowTemplate.Clone() as DataGridViewRow;
                    gridRow.CreateCells(this.najczesciejWybieraneGridView);

                    gridRow.Cells[0].Value = wyb.Pozycja;
                    gridRow.Cells[1].Value = wyb.Nazwa;
                    gridRow.Cells[2].Value = wyb.LiczbaWypozyczen;

                    this.najczesciejWybieraneGridView.Rows.Add(gridRow);
                }
            }

            //this.najczesciejWybieraneGridView.DataSource = listaNajczesciejWybieranych;
        }

        private void ResetujNajczesciejWybieraneButtonClick(object sender, EventArgs e)
        {
            LoadNajczesciejWybieraneGridView();
        }

        private void ObliczButtonClick(object sender, EventArgs e)
        {
            string zapytanie = "", zapytanieNieaktywne = "", zapytanieLiczbaWypKsiazki = "", zapytanieLiczbaWypCzasopism = "", rodzajZbioru = "";
            double liczbaNieaktywnych = 0, liczbaWszystkich = 0, liczbaWypKsiazek = 0, liczbaWypCzasopism = 0, liczbaWypOgolem = 0;
            DateTime dataPocz = this.dateTimePicker5.Value;
            DateTime dataKon = this.dateTimePicker6.Value;

            List<KartaCzytelnika> listaKartCzytelnika = new List<KartaCzytelnika>(); 
            List<KartaCzytelnika> listaNieaktywnychKartCzytelnika = new List<KartaCzytelnika>();
            List<LiczbaWypozyczen> listaWypozyczenKsiazek = new List<LiczbaWypozyczen>();
            List<LiczbaWypozyczen> listaWypozyczenCzasopism = new List<LiczbaWypozyczen>();

            BazaSQLite baza = new BazaSQLite();

            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable(); 
            DataTable dt4 = new DataTable();

            zapytanie = String.Format("select id_karty as pom from Karty");
            zapytanieNieaktywne = String.Format("select id_karty as pom from Karty as k where k.status = \"nieaktywna\"");
            zapytanieLiczbaWypKsiazki = String.Format("select id_egzemplarza + id_karty as pom from Karty_Wypozyczen_Ksiazki as kwk where data_wypozyczenia between date(\"" + dataPocz + "\") and date(\"" + dataKon + "\")");
            zapytanieLiczbaWypCzasopism = String.Format("select id_egzemplarza + id_karty as pom from Karty_Wypozyczen_Czasopisma as kwc where data_wypozyczenia between date(\"" + dataPocz + "\") and date(\"" + dataKon + "\")");
      
            dt1 = baza.GetDataTable(zapytanie);
            dt2 = baza.GetDataTable(zapytanieNieaktywne);
            dt3 = baza.GetDataTable(zapytanieLiczbaWypKsiazki);
            dt4 = baza.GetDataTable(zapytanieLiczbaWypCzasopism);

            foreach (DataRow row in dt1.Rows) 
            {
                KartaCzytelnika rekord = new KartaCzytelnika();
                rekord.Id = Convert.ToInt32(row["pom"]);
                listaKartCzytelnika.Add(rekord);
            }    
            foreach (DataRow row in dt2.Rows)
            {
                KartaCzytelnika rekord = new KartaCzytelnika();
                rekord.Id = Convert.ToInt32(row["pom"]);
                listaNieaktywnychKartCzytelnika.Add(rekord);
            }
            foreach (DataRow row in dt3.Rows)
            {
                LiczbaWypozyczen rekord = new LiczbaWypozyczen();
                rekord.Id = Convert.ToInt32(row["pom"]);
                listaWypozyczenKsiazek.Add(rekord);
            }
            foreach (DataRow row in dt4.Rows)
            {
                LiczbaWypozyczen rekord = new LiczbaWypozyczen();
                rekord.Id = Convert.ToInt32(row["pom"]);
                listaWypozyczenCzasopism.Add(rekord);
            }

            liczbaNieaktywnych = listaNieaktywnychKartCzytelnika.Count();
            liczbaWszystkich = listaKartCzytelnika.Count();
            liczbaWypKsiazek = listaWypozyczenKsiazek.Count();
            liczbaWypCzasopism = listaWypozyczenCzasopism.Count();
            liczbaWypOgolem = liczbaWypKsiazek + liczbaWypCzasopism; 

            if ((liczbaWszystkich != 0))
            {
                udzialOsobNieaktywnychLabel.Text = String.Format("{0}%", Math.Round(liczbaNieaktywnych / liczbaWszystkich, 3).ToString());
            }

            if (string.IsNullOrEmpty(rodzajZbioruComboBox.Text))
            {
                MessageBox.Show("Proszę wybrać rodzaj zbiorów bibliotecznych.", "Analiza stanu czytelnictwa");
            }
            else
            {
                rodzajZbioru = this.rodzajZbioruComboBox.SelectedItem.ToString();
            }
            
            switch (rodzajZbioru)
            {
                case "książki":
                    this.calkowitaLiczbaWypLabel.Text = Convert.ToString(liczbaWypKsiazek);
                    break;
                case "czasopisma":
                    this.calkowitaLiczbaWypLabel.Text = Convert.ToString(liczbaWypCzasopism);
                    break;
                case "wszystkie":
                    this.calkowitaLiczbaWypLabel.Text = Convert.ToString(liczbaWypOgolem);
                    break;
            }
        }

        private void ResetujObliczeniaButtonClick(object sender, EventArgs e)
        {
            this.rodzajZbioruComboBox.SelectedIndex = -1;
            this.dateTimePicker5.Value = new DateTime(1900, 1, 1);
            this.dateTimePicker6.Value = DateTime.Now;
            this.udzialOsobNieaktywnychLabel.Text = "___";
            this.calkowitaLiczbaWypLabel.Text = "___";
        }

        //Wylogowujemy się z aplikacji i wracamy do formatki logowania
        private void WylogujMenuItemClick(object sender, EventArgs e)
        {
            Application.Exit();

            ProcessStartInfo startInfo = new ProcessStartInfo("Biblioteka.exe");
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = true;
            Process.Start(startInfo);
        }

        //Kończymy prace aplikacji (opcja w menu)
        private void ZakończMenuItemClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private int? ParseNInt(string val)
        {
            int i;
            return int.TryParse(val, out i) ? (int?)i : null;
        }

        private void najczesciejWybieraneComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObslugaWidocznosciDlaCzasopism();
        }
    }

    public static class ExtensionMethods
    {
        public static void DoubleBuffered(this DataGridView dgv, bool setting)
        {
            Type dgvType = dgv.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
            pi.SetValue(dgv, setting, null);
        }
    }
}
