﻿namespace Biblioteka.Forms
{
    partial class EdycjaCzytelnika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zapiszButton = new System.Windows.Forms.Button();
            this.anulujButton = new System.Windows.Forms.Button();
            this.wypozyczeniaTabPage = new System.Windows.Forms.TabPage();
            this.zwrocButton = new System.Windows.Forms.Button();
            this.wypozyczeniaGridView = new System.Windows.Forms.DataGridView();
            this.tytulColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sygnaturaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataWypColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.terminWypColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.daneCzytelnikaTabPage = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.telefonTextBox = new System.Windows.Forms.TextBox();
            this.kodPocztowyTextBox = new System.Windows.Forms.TextBox();
            this.miejscowoscTextBox = new System.Windows.Forms.TextBox();
            this.dataTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.adresTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.imieTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nazwiskoTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.peselTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.mezczyznaRadioButton = new System.Windows.Forms.RadioButton();
            this.kobietaRadioButton = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.czytelnikTabControl = new System.Windows.Forms.TabControl();
            this.kartaTabPage = new System.Windows.Forms.TabPage();
            this.statusKartyButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.terminLabel = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            this.dataLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.usunButton = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.wypozyczeniaTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wypozyczeniaGridView)).BeginInit();
            this.daneCzytelnikaTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.czytelnikTabControl.SuspendLayout();
            this.kartaTabPage.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // zapiszButton
            // 
            this.zapiszButton.Location = new System.Drawing.Point(496, 324);
            this.zapiszButton.Name = "zapiszButton";
            this.zapiszButton.Size = new System.Drawing.Size(75, 25);
            this.zapiszButton.TabIndex = 1;
            this.zapiszButton.Text = "Zapisz";
            this.zapiszButton.UseVisualStyleBackColor = true;
            this.zapiszButton.Click += new System.EventHandler(this.ZapiszButtonClick);
            // 
            // anulujButton
            // 
            this.anulujButton.Location = new System.Drawing.Point(12, 324);
            this.anulujButton.Name = "anulujButton";
            this.anulujButton.Size = new System.Drawing.Size(75, 25);
            this.anulujButton.TabIndex = 2;
            this.anulujButton.Text = "Anuluj";
            this.anulujButton.UseVisualStyleBackColor = true;
            this.anulujButton.Click += new System.EventHandler(this.AnulujButtonClick);
            // 
            // wypozyczeniaTabPage
            // 
            this.wypozyczeniaTabPage.Controls.Add(this.zwrocButton);
            this.wypozyczeniaTabPage.Controls.Add(this.wypozyczeniaGridView);
            this.wypozyczeniaTabPage.Location = new System.Drawing.Point(4, 22);
            this.wypozyczeniaTabPage.Name = "wypozyczeniaTabPage";
            this.wypozyczeniaTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.wypozyczeniaTabPage.Size = new System.Drawing.Size(552, 280);
            this.wypozyczeniaTabPage.TabIndex = 2;
            this.wypozyczeniaTabPage.Text = "Wypożyczenia";
            this.wypozyczeniaTabPage.UseVisualStyleBackColor = true;
            // 
            // zwrocButton
            // 
            this.zwrocButton.Location = new System.Drawing.Point(475, 252);
            this.zwrocButton.Name = "zwrocButton";
            this.zwrocButton.Size = new System.Drawing.Size(75, 25);
            this.zwrocButton.TabIndex = 1;
            this.zwrocButton.Text = "Zwrot";
            this.zwrocButton.UseVisualStyleBackColor = true;
            this.zwrocButton.Click += new System.EventHandler(this.ZwrocButtonClick);
            // 
            // wypozyczeniaGridView
            // 
            this.wypozyczeniaGridView.AllowUserToAddRows = false;
            this.wypozyczeniaGridView.AllowUserToDeleteRows = false;
            this.wypozyczeniaGridView.AllowUserToResizeColumns = false;
            this.wypozyczeniaGridView.AllowUserToResizeRows = false;
            this.wypozyczeniaGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.wypozyczeniaGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.wypozyczeniaGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.wypozyczeniaGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tytulColumn,
            this.sygnaturaColumn,
            this.dataWypColumn,
            this.terminWypColumn,
            this.idColumn,
            this.typeColumn});
            this.wypozyczeniaGridView.Location = new System.Drawing.Point(0, 3);
            this.wypozyczeniaGridView.MultiSelect = false;
            this.wypozyczeniaGridView.Name = "wypozyczeniaGridView";
            this.wypozyczeniaGridView.ReadOnly = true;
            this.wypozyczeniaGridView.RowHeadersVisible = false;
            this.wypozyczeniaGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.wypozyczeniaGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.wypozyczeniaGridView.Size = new System.Drawing.Size(549, 246);
            this.wypozyczeniaGridView.TabIndex = 0;
            // 
            // tytulColumn
            // 
            this.tytulColumn.HeaderText = "Tytuł";
            this.tytulColumn.Name = "tytulColumn";
            this.tytulColumn.ReadOnly = true;
            // 
            // sygnaturaColumn
            // 
            this.sygnaturaColumn.HeaderText = "Sygnatura";
            this.sygnaturaColumn.Name = "sygnaturaColumn";
            this.sygnaturaColumn.ReadOnly = true;
            // 
            // dataWypColumn
            // 
            this.dataWypColumn.FillWeight = 80F;
            this.dataWypColumn.HeaderText = "Data wypożyczenia";
            this.dataWypColumn.Name = "dataWypColumn";
            this.dataWypColumn.ReadOnly = true;
            // 
            // terminWypColumn
            // 
            this.terminWypColumn.FillWeight = 80F;
            this.terminWypColumn.HeaderText = "Termin zwrotu";
            this.terminWypColumn.Name = "terminWypColumn";
            this.terminWypColumn.ReadOnly = true;
            // 
            // idColumn
            // 
            this.idColumn.HeaderText = "Id";
            this.idColumn.Name = "idColumn";
            this.idColumn.ReadOnly = true;
            this.idColumn.Visible = false;
            // 
            // typeColumn
            // 
            this.typeColumn.HeaderText = "Typ";
            this.typeColumn.Name = "typeColumn";
            this.typeColumn.ReadOnly = true;
            this.typeColumn.Visible = false;
            // 
            // daneCzytelnikaTabPage
            // 
            this.daneCzytelnikaTabPage.Controls.Add(this.pictureBox1);
            this.daneCzytelnikaTabPage.Controls.Add(this.tableLayoutPanel1);
            this.daneCzytelnikaTabPage.Location = new System.Drawing.Point(4, 22);
            this.daneCzytelnikaTabPage.Name = "daneCzytelnikaTabPage";
            this.daneCzytelnikaTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.daneCzytelnikaTabPage.Size = new System.Drawing.Size(552, 280);
            this.daneCzytelnikaTabPage.TabIndex = 0;
            this.daneCzytelnikaTabPage.Text = "Dane";
            this.daneCzytelnikaTabPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Biblioteka.Properties.Resources.user;
            this.pictureBox1.Location = new System.Drawing.Point(442, 168);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.telefonTextBox, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.kodPocztowyTextBox, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.miejscowoscTextBox, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.dataTimePicker, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label8, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.adresTextBox, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.imieTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.nazwiskoTextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.peselTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(540, 149);
            this.tableLayoutPanel1.TabIndex = 14;
            // 
            // telefonTextBox
            // 
            this.telefonTextBox.Location = new System.Drawing.Point(367, 100);
            this.telefonTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.telefonTextBox.Name = "telefonTextBox";
            this.telefonTextBox.Size = new System.Drawing.Size(158, 20);
            this.telefonTextBox.TabIndex = 23;
            // 
            // kodPocztowyTextBox
            // 
            this.kodPocztowyTextBox.Location = new System.Drawing.Point(367, 42);
            this.kodPocztowyTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.kodPocztowyTextBox.MaxLength = 7;
            this.kodPocztowyTextBox.Name = "kodPocztowyTextBox";
            this.kodPocztowyTextBox.Size = new System.Drawing.Size(51, 20);
            this.kodPocztowyTextBox.TabIndex = 21;
            // 
            // miejscowoscTextBox
            // 
            this.miejscowoscTextBox.Location = new System.Drawing.Point(367, 71);
            this.miejscowoscTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.miejscowoscTextBox.Name = "miejscowoscTextBox";
            this.miejscowoscTextBox.Size = new System.Drawing.Size(158, 20);
            this.miejscowoscTextBox.TabIndex = 22;
            // 
            // dataTimePicker
            // 
            this.dataTimePicker.Location = new System.Drawing.Point(93, 100);
            this.dataTimePicker.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.dataTimePicker.Name = "dataTimePicker";
            this.dataTimePicker.Size = new System.Drawing.Size(158, 20);
            this.dataTimePicker.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(277, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Telefon";
            // 
            // adresTextBox
            // 
            this.adresTextBox.Location = new System.Drawing.Point(367, 13);
            this.adresTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.adresTextBox.Name = "adresTextBox";
            this.adresTextBox.Size = new System.Drawing.Size(158, 20);
            this.adresTextBox.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(277, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Adres";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 131);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Płeć";
            // 
            // imieTextBox
            // 
            this.imieTextBox.Location = new System.Drawing.Point(93, 13);
            this.imieTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.imieTextBox.Name = "imieTextBox";
            this.imieTextBox.Size = new System.Drawing.Size(158, 20);
            this.imieTextBox.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(277, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Miejscowość";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Imię";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(277, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Kod pocztowy";
            // 
            // nazwiskoTextBox
            // 
            this.nazwiskoTextBox.Location = new System.Drawing.Point(93, 42);
            this.nazwiskoTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.nazwiskoTextBox.Name = "nazwiskoTextBox";
            this.nazwiskoTextBox.Size = new System.Drawing.Size(158, 20);
            this.nazwiskoTextBox.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nazwisko";
            // 
            // peselTextBox
            // 
            this.peselTextBox.Location = new System.Drawing.Point(93, 71);
            this.peselTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.peselTextBox.Name = "peselTextBox";
            this.peselTextBox.Size = new System.Drawing.Size(158, 20);
            this.peselTextBox.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "PESEL";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.mezczyznaRadioButton, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.kobietaRadioButton, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(90, 126);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(151, 23);
            this.tableLayoutPanel3.TabIndex = 12;
            // 
            // mezczyznaRadioButton
            // 
            this.mezczyznaRadioButton.AutoSize = true;
            this.mezczyznaRadioButton.Checked = true;
            this.mezczyznaRadioButton.Location = new System.Drawing.Point(3, 3);
            this.mezczyznaRadioButton.Name = "mezczyznaRadioButton";
            this.mezczyznaRadioButton.Size = new System.Drawing.Size(78, 17);
            this.mezczyznaRadioButton.TabIndex = 6;
            this.mezczyznaRadioButton.TabStop = true;
            this.mezczyznaRadioButton.Text = "Mężczyzna";
            this.mezczyznaRadioButton.UseVisualStyleBackColor = true;
            // 
            // kobietaRadioButton
            // 
            this.kobietaRadioButton.AutoSize = true;
            this.kobietaRadioButton.Location = new System.Drawing.Point(87, 3);
            this.kobietaRadioButton.Name = "kobietaRadioButton";
            this.kobietaRadioButton.Size = new System.Drawing.Size(61, 17);
            this.kobietaRadioButton.TabIndex = 7;
            this.kobietaRadioButton.Text = "Kobieta";
            this.kobietaRadioButton.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Data urodzenia";
            // 
            // czytelnikTabControl
            // 
            this.czytelnikTabControl.Controls.Add(this.daneCzytelnikaTabPage);
            this.czytelnikTabControl.Controls.Add(this.kartaTabPage);
            this.czytelnikTabControl.Controls.Add(this.wypozyczeniaTabPage);
            this.czytelnikTabControl.Location = new System.Drawing.Point(12, 12);
            this.czytelnikTabControl.Name = "czytelnikTabControl";
            this.czytelnikTabControl.SelectedIndex = 0;
            this.czytelnikTabControl.Size = new System.Drawing.Size(560, 306);
            this.czytelnikTabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.czytelnikTabControl.TabIndex = 0;
            // 
            // kartaTabPage
            // 
            this.kartaTabPage.Controls.Add(this.statusKartyButton);
            this.kartaTabPage.Controls.Add(this.tableLayoutPanel2);
            this.kartaTabPage.Location = new System.Drawing.Point(4, 22);
            this.kartaTabPage.Name = "kartaTabPage";
            this.kartaTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.kartaTabPage.Size = new System.Drawing.Size(552, 280);
            this.kartaTabPage.TabIndex = 3;
            this.kartaTabPage.Text = "Karta";
            this.kartaTabPage.UseVisualStyleBackColor = true;
            // 
            // statusKartyButton
            // 
            this.statusKartyButton.Location = new System.Drawing.Point(475, 252);
            this.statusKartyButton.Name = "statusKartyButton";
            this.statusKartyButton.Size = new System.Drawing.Size(75, 25);
            this.statusKartyButton.TabIndex = 1;
            this.statusKartyButton.Text = "Aktywuj";
            this.statusKartyButton.UseVisualStyleBackColor = true;
            this.statusKartyButton.Click += new System.EventHandler(this.StatusKartyButtonClick);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.terminLabel, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.statusLabel, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.dataLabel, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label12, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(276, 100);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // terminLabel
            // 
            this.terminLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.terminLabel.AutoSize = true;
            this.terminLabel.Location = new System.Drawing.Point(103, 78);
            this.terminLabel.Name = "terminLabel";
            this.terminLabel.Size = new System.Drawing.Size(28, 13);
            this.terminLabel.TabIndex = 2;
            this.terminLabel.Text = "brak";
            // 
            // statusLabel
            // 
            this.statusLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(103, 18);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(28, 13);
            this.statusLabel.TabIndex = 1;
            this.statusLabel.Text = "brak";
            // 
            // dataLabel
            // 
            this.dataLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dataLabel.AutoSize = true;
            this.dataLabel.Location = new System.Drawing.Point(103, 48);
            this.dataLabel.Name = "dataLabel";
            this.dataLabel.Size = new System.Drawing.Size(28, 13);
            this.dataLabel.TabIndex = 1;
            this.dataLabel.Text = "brak";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Termin ważności";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Data wystawienia";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Status";
            // 
            // usunButton
            // 
            this.usunButton.Location = new System.Drawing.Point(415, 324);
            this.usunButton.Name = "usunButton";
            this.usunButton.Size = new System.Drawing.Size(75, 25);
            this.usunButton.TabIndex = 3;
            this.usunButton.Text = "Usuń";
            this.usunButton.UseVisualStyleBackColor = true;
            this.usunButton.Click += new System.EventHandler(this.UsunButtonClick);
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = this;
            // 
            // EdycjaCzytelnika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.usunButton);
            this.Controls.Add(this.anulujButton);
            this.Controls.Add(this.zapiszButton);
            this.Controls.Add(this.czytelnikTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EdycjaCzytelnika";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edycja czytelnika";
            this.wypozyczeniaTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.wypozyczeniaGridView)).EndInit();
            this.daneCzytelnikaTabPage.ResumeLayout(false);
            this.daneCzytelnikaTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.czytelnikTabControl.ResumeLayout(false);
            this.kartaTabPage.ResumeLayout(false);
            this.kartaTabPage.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button zapiszButton;
        private System.Windows.Forms.Button anulujButton;
        private System.Windows.Forms.TabPage wypozyczeniaTabPage;
        private System.Windows.Forms.TabPage daneCzytelnikaTabPage;
        private System.Windows.Forms.TextBox peselTextBox;
        private System.Windows.Forms.TextBox nazwiskoTextBox;
        private System.Windows.Forms.TextBox imieTextBox;
        private System.Windows.Forms.RadioButton kobietaRadioButton;
        private System.Windows.Forms.RadioButton mezczyznaRadioButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl czytelnikTabControl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox telefonTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox miejscowoscTextBox;
        private System.Windows.Forms.TextBox adresTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox kodPocztowyTextBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView wypozyczeniaGridView;
        private System.Windows.Forms.TabPage kartaTabPage;
        private System.Windows.Forms.Button zwrocButton;
        private System.Windows.Forms.DateTimePicker dataTimePicker;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label terminLabel;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Label dataLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button usunButton;
        private System.Windows.Forms.Button statusKartyButton;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.DataGridViewTextBoxColumn tytulColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sygnaturaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataWypColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn terminWypColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeColumn;
    }
}