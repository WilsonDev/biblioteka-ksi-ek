﻿using BibliotekaKsiazek;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka.Forms
{
    public partial class SzczegolyKsiazki : Form
    {
        private Ksiazka ksiazka;
        private List<EgzemplarzKsiazki> egzemplarze;

        private WKRepository wkRepository;

        public SzczegolyKsiazki()
        {
            InitializeComponent();
        }

        public void PobierzDaneKsiazki(Ksiazka ksiazka, List<EgzemplarzKsiazki> egzemplarze)
        {
            wkRepository = new WKRepository();
            wkRepository.PobierzDane();

            this.ksiazka = ksiazka;
            this.egzemplarze = egzemplarze;

            this.isbnKsiazkiLabel.Text = ksiazka.Isbn;
            this.tytulKsiazkiLabel.Text = ksiazka.Tytul;
            this.autorKsiazkiLabel.Text = ksiazka.Autor;
            this.rokKsiazkiLabel.Text = ksiazka.RokWydania.ToString();
            this.wydawnictwoKsiazkiLabel.Text = ksiazka.Wydawnictwo;
            this.kategoriaKsiazkiLabel.Text = ksiazka.Kategoria.Nazwa;

            foreach (var obj in egzemplarze)
            {
                DataGridViewRow row = this.egzemplarzeGridView.RowTemplate.Clone() as DataGridViewRow;
                row.CreateCells(this.egzemplarzeGridView);

                row.Cells[0].Value = obj.Id;
                row.Cells[1].Value = obj.Sygnatura;
                row.Cells[2].Value = (wkRepository.czyEgzemplarzWypozyczony(obj))?"Nie":"Tak";

                this.egzemplarzeGridView.Rows.Add(row);      
            }
        }

        private void WypozyczEgzemplarzButtonClick(object sender, EventArgs e)
        {
            PanelCzytelnika czytelnik = new PanelCzytelnika();

            //Zaznaczony egzemplarz
            if (this.egzemplarzeGridView.SelectedRows.Count > 0 || czytelnik.WypozyczeniaCzytelnika.Count() <= 3)
            {
                EgzemplarzKsiazki selected = this.egzemplarze.Find(k => k.Id == Convert.ToInt64(this.egzemplarzeGridView.SelectedCells[0].Value));
                
                //Sprawdzamy czy egzemplarz jest dostępny
                if (!wkRepository.czyEgzemplarzWypozyczony(selected) && MessageBox.Show("Wypożyczyć?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    WypozyczenieKsiazki wypozyczenie = new WypozyczenieKsiazki();

                    wypozyczenie.EgzemplarzKsiazki = selected;
                    wypozyczenie.DataWypozyczenia = DateTime.Now;
                    wypozyczenie.TerminZwrotu = DateTime.Now.AddMonths(1);
                    wypozyczenie.KartaCzytelnika = czytelnik.Karta;

                    wkRepository.Dodaj(wypozyczenie);
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Nie można wypożyczyć egzemplarza", "Ostrzeżenie", MessageBoxButtons.OK);
                }
            }
        }

        private void AnulujButtonClick(object sender, EventArgs e)
        {
            Close();
        }
    }
}
