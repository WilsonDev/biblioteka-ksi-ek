﻿namespace Biblioteka.Forms
{
    partial class Logowanie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.loginLabel = new System.Windows.Forms.Label();
            this.hasloLabel = new System.Windows.Forms.Label();
            this.loginTextBox = new System.Windows.Forms.TextBox();
            this.hasloTextBox = new System.Windows.Forms.TextBox();
            this.logujButton = new System.Windows.Forms.Button();
            this.anulujButton = new System.Windows.Forms.Button();
            this.goscButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // loginLabel
            // 
            this.loginLabel.AutoSize = true;
            this.loginLabel.Location = new System.Drawing.Point(12, 12);
            this.loginLabel.Name = "loginLabel";
            this.loginLabel.Size = new System.Drawing.Size(33, 13);
            this.loginLabel.TabIndex = 0;
            this.loginLabel.Text = "Login";
            // 
            // hasloLabel
            // 
            this.hasloLabel.AutoSize = true;
            this.hasloLabel.Location = new System.Drawing.Point(12, 38);
            this.hasloLabel.Name = "hasloLabel";
            this.hasloLabel.Size = new System.Drawing.Size(36, 13);
            this.hasloLabel.TabIndex = 1;
            this.hasloLabel.Text = "Hasło";
            // 
            // loginTextBox
            // 
            this.loginTextBox.Location = new System.Drawing.Point(51, 9);
            this.loginTextBox.Name = "loginTextBox";
            this.loginTextBox.Size = new System.Drawing.Size(137, 20);
            this.loginTextBox.TabIndex = 2;
            this.toolTip1.SetToolTip(this.loginTextBox, "Tutaj wpisz login");
            // 
            // hasloTextBox
            // 
            this.hasloTextBox.Location = new System.Drawing.Point(51, 35);
            this.hasloTextBox.Name = "hasloTextBox";
            this.hasloTextBox.PasswordChar = '*';
            this.hasloTextBox.Size = new System.Drawing.Size(137, 20);
            this.hasloTextBox.TabIndex = 3;
            this.toolTip1.SetToolTip(this.hasloTextBox, "Tutaj wpisz hasło");
            // 
            // logujButton
            // 
            this.logujButton.Location = new System.Drawing.Point(12, 61);
            this.logujButton.Name = "logujButton";
            this.logujButton.Size = new System.Drawing.Size(176, 28);
            this.logujButton.TabIndex = 4;
            this.logujButton.Text = "Loguj";
            this.toolTip1.SetToolTip(this.logujButton, "Przycisk do logowania");
            this.logujButton.UseVisualStyleBackColor = true;
            this.logujButton.Click += new System.EventHandler(this.LogujButtonClick);
            // 
            // anulujButton
            // 
            this.anulujButton.Location = new System.Drawing.Point(108, 95);
            this.anulujButton.Name = "anulujButton";
            this.anulujButton.Size = new System.Drawing.Size(80, 25);
            this.anulujButton.TabIndex = 5;
            this.anulujButton.Text = "Anuluj";
            this.toolTip1.SetToolTip(this.anulujButton, "Zamyka program");
            this.anulujButton.UseVisualStyleBackColor = true;
            this.anulujButton.Click += new System.EventHandler(this.AnulujButtonClick);
            // 
            // goscButton
            // 
            this.goscButton.Location = new System.Drawing.Point(12, 95);
            this.goscButton.Name = "goscButton";
            this.goscButton.Size = new System.Drawing.Size(90, 25);
            this.goscButton.TabIndex = 6;
            this.goscButton.Text = "Loguj jako gość";
            this.toolTip1.SetToolTip(this.goscButton, "Loguje jako Gość, bez wszystkich dostępnych funkcjonalności");
            this.goscButton.UseVisualStyleBackColor = true;
            this.goscButton.Click += new System.EventHandler(this.GoscButtonClick);
            // 
            // Logowanie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(200, 132);
            this.Controls.Add(this.goscButton);
            this.Controls.Add(this.anulujButton);
            this.Controls.Add(this.logujButton);
            this.Controls.Add(this.hasloTextBox);
            this.Controls.Add(this.loginTextBox);
            this.Controls.Add(this.hasloLabel);
            this.Controls.Add(this.loginLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Logowanie";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Logowanie";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label loginLabel;
        private System.Windows.Forms.Label hasloLabel;
        private System.Windows.Forms.TextBox loginTextBox;
        private System.Windows.Forms.TextBox hasloTextBox;
        private System.Windows.Forms.Button logujButton;
        private System.Windows.Forms.Button anulujButton;
        private System.Windows.Forms.Button goscButton;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}