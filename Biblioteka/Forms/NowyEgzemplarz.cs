﻿using BibliotekaKsiazek;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka.Forms
{
    public partial class NowyEgzemplarz : Form
    {
        private Ksiazka ksiazka;
        private Czasopismo czasopismo;

        private EgzemplarzKsiazki egzemplarzKsiazki;
        public EgzemplarzKsiazki EgzemplarzKsiazki
        {
            get { return egzemplarzKsiazki; }
        }

        private EgzemplarzCzasopisma egzemplarzCzasopisma;
        public EgzemplarzCzasopisma EgzemplarzCzasopisma
        {
            get { return egzemplarzCzasopisma; }
        }

        public NowyEgzemplarz()
        {
            InitializeComponent();
        }

        public NowyEgzemplarz(Ksiazka ksiazka)
        {
            InitializeComponent();

            this.ksiazka = ksiazka;
        }

        public NowyEgzemplarz(Czasopismo czasopismo)
        {
            InitializeComponent();

            this.czasopismo = czasopismo;
        }

        private void DodajEgzemplarzButtonClick(object sender, EventArgs e)
        {
            if (ksiazka != null)
            {
                EKRepository ekRepository = new EKRepository();

                EgzemplarzKsiazki egzemplarz = new EgzemplarzKsiazki();
                egzemplarz.Ksiazka = ksiazka;
                egzemplarz.Sygnatura = sygnaturaTextBox.Text;

                egzemplarzKsiazki = egzemplarz;
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                ECRepository ecRepository = new ECRepository();

                EgzemplarzCzasopisma egzemplarz = new EgzemplarzCzasopisma();
                egzemplarz.Czasopismo = czasopismo;
                egzemplarz.Sygnatura = sygnaturaTextBox.Text;

                egzemplarzCzasopisma = egzemplarz;
                this.DialogResult = DialogResult.OK;

            }
        }

        private void AnulujButtonClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
