﻿namespace Biblioteka.Forms
{
    partial class SzczegolyCzasopisma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.daneTabPage = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.odbiorcaCzasopismaLabel = new System.Windows.Forms.Label();
            this.tematykaCzasopismaLabel = new System.Windows.Forms.Label();
            this.czestotliwoscCzasopismaLabel = new System.Windows.Forms.Label();
            this.wydawnictwoCzasopismaLabel = new System.Windows.Forms.Label();
            this.tytulCzasopismaLabel = new System.Windows.Forms.Label();
            this.dataWydLabel = new System.Windows.Forms.Label();
            this.nrCzasopismaLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.issnCzasopismaLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.egzemplarzeTabPage = new System.Windows.Forms.TabPage();
            this.wypozyczEgzemplarzButton = new System.Windows.Forms.Button();
            this.egzemplarzeGridView = new System.Windows.Forms.DataGridView();
            this.idColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sygnaturaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dostepnoscColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.anulujButton = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabControl1.SuspendLayout();
            this.daneTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.egzemplarzeTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.egzemplarzeGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.daneTabPage);
            this.tabControl1.Controls.Add(this.egzemplarzeTabPage);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(560, 306);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // daneTabPage
            // 
            this.daneTabPage.Controls.Add(this.pictureBox1);
            this.daneTabPage.Controls.Add(this.tableLayoutPanel1);
            this.daneTabPage.Location = new System.Drawing.Point(4, 22);
            this.daneTabPage.Name = "daneTabPage";
            this.daneTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.daneTabPage.Size = new System.Drawing.Size(552, 280);
            this.daneTabPage.TabIndex = 0;
            this.daneTabPage.Text = "Dane";
            this.daneTabPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Biblioteka.Properties.Resources.magazine;
            this.pictureBox1.Location = new System.Drawing.Point(442, 168);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 164F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.odbiorcaCzasopismaLabel, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.tematykaCzasopismaLabel, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.czestotliwoscCzasopismaLabel, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.wydawnictwoCzasopismaLabel, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tytulCzasopismaLabel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataWydLabel, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.nrCzasopismaLabel, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.issnCzasopismaLabel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(540, 155);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // odbiorcaCzasopismaLabel
            // 
            this.odbiorcaCzasopismaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.odbiorcaCzasopismaLabel.AutoSize = true;
            this.odbiorcaCzasopismaLabel.Location = new System.Drawing.Point(367, 105);
            this.odbiorcaCzasopismaLabel.Name = "odbiorcaCzasopismaLabel";
            this.odbiorcaCzasopismaLabel.Size = new System.Drawing.Size(54, 13);
            this.odbiorcaCzasopismaLabel.TabIndex = 21;
            this.odbiorcaCzasopismaLabel.Text = "_odbiorca";
            // 
            // tematykaCzasopismaLabel
            // 
            this.tematykaCzasopismaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tematykaCzasopismaLabel.AutoSize = true;
            this.tematykaCzasopismaLabel.Location = new System.Drawing.Point(367, 47);
            this.tematykaCzasopismaLabel.Name = "tematykaCzasopismaLabel";
            this.tematykaCzasopismaLabel.Size = new System.Drawing.Size(56, 13);
            this.tematykaCzasopismaLabel.TabIndex = 22;
            this.tematykaCzasopismaLabel.Text = "_tematyka";
            // 
            // czestotliwoscCzasopismaLabel
            // 
            this.czestotliwoscCzasopismaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.czestotliwoscCzasopismaLabel.AutoSize = true;
            this.czestotliwoscCzasopismaLabel.Location = new System.Drawing.Point(367, 76);
            this.czestotliwoscCzasopismaLabel.Name = "czestotliwoscCzasopismaLabel";
            this.czestotliwoscCzasopismaLabel.Size = new System.Drawing.Size(76, 13);
            this.czestotliwoscCzasopismaLabel.TabIndex = 19;
            this.czestotliwoscCzasopismaLabel.Text = "_czestotliwosc";
            // 
            // wydawnictwoCzasopismaLabel
            // 
            this.wydawnictwoCzasopismaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.wydawnictwoCzasopismaLabel.AutoSize = true;
            this.wydawnictwoCzasopismaLabel.Location = new System.Drawing.Point(93, 134);
            this.wydawnictwoCzasopismaLabel.Name = "wydawnictwoCzasopismaLabel";
            this.wydawnictwoCzasopismaLabel.Size = new System.Drawing.Size(77, 13);
            this.wydawnictwoCzasopismaLabel.TabIndex = 21;
            this.wydawnictwoCzasopismaLabel.Text = "_wydawnictwo";
            // 
            // tytulCzasopismaLabel
            // 
            this.tytulCzasopismaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tytulCzasopismaLabel.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.tytulCzasopismaLabel, 4);
            this.tytulCzasopismaLabel.Location = new System.Drawing.Point(93, 18);
            this.tytulCzasopismaLabel.Name = "tytulCzasopismaLabel";
            this.tytulCzasopismaLabel.Size = new System.Drawing.Size(32, 13);
            this.tytulCzasopismaLabel.TabIndex = 19;
            this.tytulCzasopismaLabel.Text = "_tytul";
            // 
            // dataWydLabel
            // 
            this.dataWydLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dataWydLabel.AutoSize = true;
            this.dataWydLabel.Location = new System.Drawing.Point(93, 105);
            this.dataWydLabel.Name = "dataWydLabel";
            this.dataWydLabel.Size = new System.Drawing.Size(34, 13);
            this.dataWydLabel.TabIndex = 20;
            this.dataWydLabel.Text = "_data";
            // 
            // nrCzasopismaLabel
            // 
            this.nrCzasopismaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nrCzasopismaLabel.AutoSize = true;
            this.nrCzasopismaLabel.Location = new System.Drawing.Point(93, 76);
            this.nrCzasopismaLabel.Name = "nrCzasopismaLabel";
            this.nrCzasopismaLabel.Size = new System.Drawing.Size(42, 13);
            this.nrCzasopismaLabel.TabIndex = 20;
            this.nrCzasopismaLabel.Text = "_numer";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Numer";
            // 
            // issnCzasopismaLabel
            // 
            this.issnCzasopismaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.issnCzasopismaLabel.AutoSize = true;
            this.issnCzasopismaLabel.Location = new System.Drawing.Point(93, 47);
            this.issnCzasopismaLabel.Name = "issnCzasopismaLabel";
            this.issnCzasopismaLabel.Size = new System.Drawing.Size(31, 13);
            this.issnCzasopismaLabel.TabIndex = 19;
            this.issnCzasopismaLabel.Text = "_issn";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Data wydania";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Wydawnictwo";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(277, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Odbiorca";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(277, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Częstotliwość";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(277, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Tematyka";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ISSN";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tytuł";
            // 
            // egzemplarzeTabPage
            // 
            this.egzemplarzeTabPage.Controls.Add(this.wypozyczEgzemplarzButton);
            this.egzemplarzeTabPage.Controls.Add(this.egzemplarzeGridView);
            this.egzemplarzeTabPage.Location = new System.Drawing.Point(4, 22);
            this.egzemplarzeTabPage.Name = "egzemplarzeTabPage";
            this.egzemplarzeTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.egzemplarzeTabPage.Size = new System.Drawing.Size(552, 280);
            this.egzemplarzeTabPage.TabIndex = 1;
            this.egzemplarzeTabPage.Text = "Egzemplarze";
            this.egzemplarzeTabPage.UseVisualStyleBackColor = true;
            // 
            // wypozyczEgzemplarzButton
            // 
            this.wypozyczEgzemplarzButton.Location = new System.Drawing.Point(475, 252);
            this.wypozyczEgzemplarzButton.Name = "wypozyczEgzemplarzButton";
            this.wypozyczEgzemplarzButton.Size = new System.Drawing.Size(75, 25);
            this.wypozyczEgzemplarzButton.TabIndex = 1;
            this.wypozyczEgzemplarzButton.Text = "Wypożycz";
            this.wypozyczEgzemplarzButton.UseVisualStyleBackColor = true;
            this.wypozyczEgzemplarzButton.Click += new System.EventHandler(this.WypozyczEgzemplarzButtonClick);
            // 
            // egzemplarzeGridView
            // 
            this.egzemplarzeGridView.AllowUserToAddRows = false;
            this.egzemplarzeGridView.AllowUserToDeleteRows = false;
            this.egzemplarzeGridView.AllowUserToResizeColumns = false;
            this.egzemplarzeGridView.AllowUserToResizeRows = false;
            this.egzemplarzeGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.egzemplarzeGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.egzemplarzeGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.egzemplarzeGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idColumn,
            this.sygnaturaColumn,
            this.dostepnoscColumn});
            this.egzemplarzeGridView.Location = new System.Drawing.Point(0, 3);
            this.egzemplarzeGridView.Name = "egzemplarzeGridView";
            this.egzemplarzeGridView.ReadOnly = true;
            this.egzemplarzeGridView.RowHeadersVisible = false;
            this.egzemplarzeGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.egzemplarzeGridView.Size = new System.Drawing.Size(549, 246);
            this.egzemplarzeGridView.TabIndex = 0;
            // 
            // idColumn
            // 
            this.idColumn.HeaderText = "#";
            this.idColumn.Name = "idColumn";
            this.idColumn.ReadOnly = true;
            // 
            // sygnaturaColumn
            // 
            this.sygnaturaColumn.HeaderText = "Sygnatura";
            this.sygnaturaColumn.Name = "sygnaturaColumn";
            this.sygnaturaColumn.ReadOnly = true;
            // 
            // dostepnoscColumn
            // 
            this.dostepnoscColumn.HeaderText = "Dostępny";
            this.dostepnoscColumn.Name = "dostepnoscColumn";
            this.dostepnoscColumn.ReadOnly = true;
            // 
            // anulujButton
            // 
            this.anulujButton.Location = new System.Drawing.Point(11, 324);
            this.anulujButton.Name = "anulujButton";
            this.anulujButton.Size = new System.Drawing.Size(75, 25);
            this.anulujButton.TabIndex = 1;
            this.anulujButton.Text = "Anuluj";
            this.anulujButton.UseVisualStyleBackColor = true;
            this.anulujButton.Click += new System.EventHandler(this.AnulujButtonClick);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // SzczegolyCzasopisma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.anulujButton);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "SzczegolyCzasopisma";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Szczegóły czasopisma";
            this.tabControl1.ResumeLayout(false);
            this.daneTabPage.ResumeLayout(false);
            this.daneTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.egzemplarzeTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.egzemplarzeGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage daneTabPage;
        private System.Windows.Forms.TabPage egzemplarzeTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button anulujButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView egzemplarzeGridView;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sygnaturaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dostepnoscColumn;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Label wydawnictwoCzasopismaLabel;
        private System.Windows.Forms.Label tytulCzasopismaLabel;
        private System.Windows.Forms.Label nrCzasopismaLabel;
        private System.Windows.Forms.Label issnCzasopismaLabel;
        private System.Windows.Forms.Button wypozyczEgzemplarzButton;
        private System.Windows.Forms.Label odbiorcaCzasopismaLabel;
        private System.Windows.Forms.Label tematykaCzasopismaLabel;
        private System.Windows.Forms.Label czestotliwoscCzasopismaLabel;
        private System.Windows.Forms.Label dataWydLabel;
    }
}