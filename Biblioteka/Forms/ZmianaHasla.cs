﻿using BibliotekaKsiazek;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteka.Forms
{
    public partial class ZmianaHasla : Form
    {
        private Czytelnik czytelnik;

        public ZmianaHasla()
        {
            InitializeComponent();
        }

        public ZmianaHasla(Czytelnik czytelnik)
        {
            this.czytelnik = czytelnik;

            InitializeComponent();
        }

        private void ZapiszButtonClick(object sender, EventArgs e)
        {
            string obecne = this.obecneHasloTextBox.Text;
            string nowe = this.noweHasloTextBox.Text;

            if (obecne.Equals(czytelnik.Haslo))
            {
                CzytelnikRepository czytelnikRepository = new CzytelnikRepository();
                czytelnik.Haslo = nowe;
                czytelnikRepository.Edytuj(czytelnik);
                this.DialogResult = DialogResult.Yes;
            }  
        }
    }
}
