﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka.Baza
{
    class BazaSQLite //Klasa pomocnicza
    {
        private string dbConnection;
        private static long lastId;

        public BazaSQLite()
        {
            dbConnection = "Data Source=.\\Baza\\baza.db";
        }

        public DataTable GetDataTable(string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                SQLiteConnection cnn = new SQLiteConnection(dbConnection);
                cnn.Open();
                SQLiteCommand mycommand = new SQLiteCommand(cnn);
                mycommand.CommandText = sql;
                SQLiteDataReader reader = mycommand.ExecuteReader();
                dt.Load(reader);
                reader.Close();
                cnn.Close();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return dt;
        }

        public string NonQuery(string sql)
        {
            try
            {
                SQLiteConnection cnn = new SQLiteConnection(dbConnection);
                cnn.Open();
                SQLiteCommand mycommand = new SQLiteCommand(cnn);
                mycommand.CommandText = sql;
                mycommand.ExecuteNonQuery();
                mycommand.CommandText = "select last_insert_rowid()";
                lastId = (long)mycommand.ExecuteScalar();
                cnn.Close();
            }
            catch (Exception e)
            {
                return e.Message;
            }
            return "Sukces";
        }

        //Pobieramy id ostatniego dodanego rekordu
        public static long LastId()
        {
            return lastId;
        }
    }
}
