using Biblioteka.Baza;
using System;
using System.Collections.Generic;
using System.Data;

namespace BibliotekaKsiazek {
	/// <summary>
	/// Egzemplarz ksi�zki
	/// </summary>
	public class EKRepository : IRepository<EgzemplarzKsiazki> {
		private List<EgzemplarzKsiazki> lista;
		public List<EgzemplarzKsiazki> Lista {
			get {
				return lista;
			}
			set {
				lista = value;
			}
		}

        //Konstruktor
		public EKRepository() {  
		}

        public void PobierzDane() {
            lista = new List<EgzemplarzKsiazki>(); //Tworzymy obiekt List<EgzemplarzKsiazki>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_egzemplarza, o.sygnatura, o.id_ksiazki from Karty_Egzemplarzy_Ksiazki as o");

            KsiazkaRepository ksiazkaRepository = new KsiazkaRepository();
            ksiazkaRepository.PobierzDane();

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                EgzemplarzKsiazki egzemplarzKsiazki = new EgzemplarzKsiazki(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                egzemplarzKsiazki.Id = Convert.ToInt32(row["id_egzemplarza"]);
                egzemplarzKsiazki.Sygnatura = row["sygnatura"].ToString();
                egzemplarzKsiazki.Ksiazka = ksiazkaRepository.Lista.Find(k => k.Id == Convert.ToInt32(row["id_ksiazki"]));

                lista.Add(egzemplarzKsiazki); //Wrzucamy do listy  
            }
        }

        public List<EgzemplarzKsiazki> PoKsiazce(Ksiazka ksiazka) {
            return lista.FindAll(k => k.Ksiazka.Id == ksiazka.Id);
        }

        public string Usun(EgzemplarzKsiazki obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("DELETE FROM Karty_Egzemplarzy_Ksiazki WHERE id_egzemplarza='" + obiekt.Id + "'");
        }

		public string Edytuj(EgzemplarzKsiazki obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("update Karty_Egzemplarzy_Ksiazki set sygnatura = '" + obiekt.Sygnatura + "', id_ksiazki = '" + obiekt.Ksiazka.Id +
                "' where id_egzemplarza = '" + obiekt.Id + "'");
		}

		public string Dodaj(EgzemplarzKsiazki obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("INSERT INTO  Karty_Egzemplarzy_Ksiazki (sygnatura, id_ksiazki) " +
                "values ('" + obiekt.Sygnatura + "', '" + obiekt.Ksiazka.Id + "')");
		}

	}

}
