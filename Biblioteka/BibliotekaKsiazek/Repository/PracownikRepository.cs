using Biblioteka.Baza;
using System;
using System.Collections.Generic;
using System.Data;

namespace BibliotekaKsiazek {
	/// <summary>
	/// Pracownicy ustaleni na "sztywno" w bazie.
	/// </summary>
	public class PracownikRepository : IRepository<Pracownik> {
        private List<Pracownik> lista;
        public List<Pracownik> Lista {
            get {
                return lista;
            }
            set {
                lista = value;
            }
        }

        //Konstruktor
		public PracownikRepository() {   
		}

        public void PobierzDane() {
            lista = new List<Pracownik>(); //Tworzymy obiekt List<Pracownik>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_pracownika, o.login, o.haslo from Pracownicy as o");

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                Pracownik pracownik = new Pracownik(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                pracownik.Id = Convert.ToInt32(row["id_pracownika"]);
                pracownik.Login = row["login"].ToString();
                pracownik.Haslo = row["haslo"].ToString();

                lista.Add(pracownik); //Wrzucamy do listy   
            }
        }

        public string Dodaj(Pracownik obiekt)
        {
            throw new NotImplementedException();
        }

        public string Edytuj(Pracownik obiekt)
        {
            throw new NotImplementedException();
        }

        public string Usun(Pracownik obiekt)
        {
            throw new NotImplementedException();
        }
    }
}
