﻿using Biblioteka.Baza;
using BibliotekaKsiazek;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekaKsiazek {
    public class AdresRepository : IRepository<Adres> {
        private List<Adres> lista;
        public List<Adres> Lista {
            get {
                return lista;
            }
            set {
                lista = value;
            }
        }

        //Konstruktor
        public AdresRepository() {    
        }

        public void PobierzDane() {
            lista = new List<Adres>(); //Tworzymy obiekt List<Adres>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawierająca metodę GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmująca zapytanie SELECT w String i zwracająca DataTable
            dt = baza.GetDataTable("select o.id_adresu, o.ulica, o.kod_pocztowy, o.miejscowosc, o.telefon from Adresy as o");

            foreach (DataRow row in dt.Rows) //Iterujemy po każdym wierszu w DataTable
            {
                Adres adres = new Adres(); //Tworzymy obiekt do którego bedziemy przypisywać wartości z DataRow
                adres.Id = Convert.ToInt32(row["id_adresu"]);
                adres.Ulica = row["ulica"].ToString();
                adres.KodPocztowy = row["kod_pocztowy"].ToString();
                adres.Miejscowosc = row["miejscowosc"].ToString();
                adres.Telefon = row["telefon"].ToString();

                lista.Add(adres); //Wrzucamy do listy   
            }
        }

        public string Usun(Adres obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("DELETE FROM Adresy WHERE id_adresu='" + obiekt.Id + "'");
        }

        public string Edytuj(Adres obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("update Adresy set ulica = '" + obiekt.Ulica + "', kod_pocztowy = '" + obiekt.KodPocztowy + "', miejscowosc = '" + obiekt.Miejscowosc +  "' where id_adresu = '" 
                + obiekt.Id + "'");
        }

        public string Dodaj(Adres obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("insert into Adresy ( ulica, kod_pocztowy, miejscowosc, telefon)" +
                "values ('" + obiekt.Ulica + "', '" + obiekt.KodPocztowy + "', '" + obiekt.Miejscowosc + "', '" + obiekt.Telefon + "')");
        }
    }
}
