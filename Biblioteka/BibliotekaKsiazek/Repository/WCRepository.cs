using Biblioteka.Baza;
using BibliotekaKsiazek;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekaKsiazek {
	/// <summary>
	/// Wypo�yczenia czasopisma
	/// </summary>
	public class WCRepository : IRepository<WypozyczenieCzasopisma> {
		private List<WypozyczenieCzasopisma> lista;
		public List<WypozyczenieCzasopisma> Lista {
			get {
				return lista;
			}
			set {
				lista = value;
			}
		}

        //Konstruktor
        public WCRepository() {      
		}

        public void PobierzDane() {
            lista = new List<WypozyczenieCzasopisma>(); //Tworzymy obiekt List<Ksiazka>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_egzemplarza, o.id_karty, o.data_wypozyczenia, o.termin_zwrotu, o.data_zwrotu from Karty_Wypozyczen_Czasopisma as o");

            ECRepository ecRepository = new ECRepository();
            ecRepository.PobierzDane();

            KartaCzytelnikaRepository kartaCzytelnikaRepository = new KartaCzytelnikaRepository();
            kartaCzytelnikaRepository.PobierzDane();

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                WypozyczenieCzasopisma wypozyczenieCzasopisma = new WypozyczenieCzasopisma(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                wypozyczenieCzasopisma.EgzemplarzCzasopisma = ecRepository.Lista.Find(k => k.Id == Convert.ToInt32(row["id_egzemplarza"]));
                wypozyczenieCzasopisma.KartaCzytelnika = kartaCzytelnikaRepository.Lista.Find(k => k.Id == Convert.ToInt32(row["id_karty"]));
                wypozyczenieCzasopisma.DataWypozyczenia = Convert.ToDateTime(row["data_wypozyczenia"]);
                wypozyczenieCzasopisma.TerminZwrotu = Convert.ToDateTime(row["termin_zwrotu"]);
                wypozyczenieCzasopisma.DataZwrotu = Convert.ToDateTime(row["data_zwrotu"]);

                lista.Add(wypozyczenieCzasopisma); //Wrzucamy do listy   
            }
        }

        public List<WypozyczenieCzasopisma> Wypozyczone() {
            return lista.FindAll(k => k.DataZwrotu < new DateTime(1900, 1, 1));
        }

        public List<WypozyczenieCzasopisma> WypozyczonePoCzytelniku(KartaCzytelnika karta) {
            return lista.FindAll(k => k.DataZwrotu < new DateTime(1900, 1, 1) && k.KartaCzytelnika.Id == karta.Id);
        }

        public bool czyEgzemplarzWypozyczony(EgzemplarzCzasopisma egzemplarz) {
            List<WypozyczenieCzasopisma> wypozyczenia = lista.FindAll(k => k.EgzemplarzCzasopisma.Id == egzemplarz.Id && k.DataZwrotu < new DateTime(1900, 1, 1));

            return (wypozyczenia.Count == 0) ? false : true;
        }

        public List<WypozyczenieCzasopisma> PoDacieWypozyczenia(DateTime data) {
            return lista.FindAll(k => k.DataWypozyczenia.ToShortDateString().Equals(data.ToShortDateString()));
        }

        public List<WypozyczenieCzasopisma> PoTerminieZwrotu(DateTime data) {
            return lista.FindAll(k => k.TerminZwrotu.ToShortDateString().Equals(data.ToShortDateString()));
        }

        public List<WypozyczenieCzasopisma> PoDacieZwrotu(DateTime data) {
            return lista.FindAll(k => k.DataZwrotu.ToShortDateString().Equals(data.ToShortDateString()));
        }

        public List<WypozyczenieCzasopisma> PoISSN(string issn) {
            return lista.FindAll(k => k.EgzemplarzCzasopisma.Czasopismo.Issn.Contains(issn));
        }

        public List<WypozyczenieCzasopisma> PoCzytelniku(Czytelnik czyt) {
            return lista.FindAll(k => k.KartaCzytelnika.Czytelnik.Id == czyt.Id);
        }

        public List<WypozyczenieCzasopisma> Wyszukaj(DateTime dataWyp, DateTime terminZwr, DateTime dataZwr, string issn, Czytelnik czyt) {
            return lista.FindAll(k => k.DataWypozyczenia.Equals(dataWyp) && k.TerminZwrotu.Equals(terminZwr) && k.DataZwrotu.Equals(dataZwr) && k.EgzemplarzCzasopisma.Czasopismo.Issn.Contains(issn) && k.KartaCzytelnika.Czytelnik.Id == czyt.Id);
        } 

		public List<Czasopismo> TopCzasopisma() {
			throw new System.Exception("Not implemented");
		}

		public List<Tematyka> TopTematyka() {
			throw new System.Exception("Not implemented");
		}

		public List<Odbiorca> TopOdbiorca() {
			throw new System.Exception("Not implemented");
		}

		public double CCS() {
			throw new System.Exception("Not implemented");
		}

		public double CDT() {
			throw new System.Exception("Not implemented");
		}

        public string Zwroc(WypozyczenieCzasopisma obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("UPDATE Karty_Wypozyczen_Czasopisma SET data_zwrotu='" + DateTime.Now.ToShortDateString() +
                "' WHERE id_egzemplarza='" + obiekt.EgzemplarzCzasopisma.Id + "' AND id_karty='" + obiekt.KartaCzytelnika.Id + "'");
        }

        public string UsunWypozyczeniaEgzemplarza(EgzemplarzCzasopisma obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("DELETE FROM Karty_Wypozyczen_Czasopisma WHERE id_egzemplarza='" + obiekt.Id + "'");
        }
        
		public string Usun(WypozyczenieCzasopisma obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("DELETE FROM Karty_Wypozyczen_Czasopisma WHERE id_egzemplarza='" + obiekt.EgzemplarzCzasopisma.Id + "' AND id_karty='" + obiekt.KartaCzytelnika.Id + "'");
		}

        public string Edytuj(WypozyczenieCzasopisma obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("UPDATE Karty_Wypozyczen_Czasopisma SET id_karty='" + obiekt.KartaCzytelnika.Id + "', data_wypozyczenia='" + obiekt.DataWypozyczenia + "', termin_zwrotu='" + 
                obiekt.TerminZwrotu + "', data_zwrotu='" + obiekt.DataZwrotu + "' WHERE id_egzemplarza='" + obiekt.EgzemplarzCzasopisma.Id + "' and id_karty='" + obiekt.KartaCzytelnika.Id + "'");
        }
       
		public string Dodaj(WypozyczenieCzasopisma obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("INSERT INTO  Karty_Wypozyczen_Czasopisma (id_egzemplarza, id_karty, data_wypozyczenia, termin_zwrotu, data_zwrotu)" +
                "values ('" + obiekt.EgzemplarzCzasopisma.Id + "', '" + obiekt.KartaCzytelnika.Id + "', '" + obiekt.DataWypozyczenia.ToShortDateString() + "', '" + obiekt.TerminZwrotu.ToShortDateString() + "' , '" +
                obiekt.DataZwrotu.ToShortDateString() + "')");
		}

	}

}
