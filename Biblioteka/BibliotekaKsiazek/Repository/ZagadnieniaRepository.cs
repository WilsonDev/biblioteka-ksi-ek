using Biblioteka.Baza;
using System;
using System.Collections.Generic;
using System.Data;

namespace BibliotekaKsiazek {
	/// <summary>
	/// W sk�ad "Zagadnien" wchodz�: Tematyka, Czestotliwosc, Odbiorca, Kategoria.
    /// Rekordy s� zapisane na sztywno w bazie wiec wystarczy metoda Wszystkie...()
	/// </summary>
	public static class ZagadnieniaRepository {
		public static List<Tematyka> WszystkieTematyki() {
            List<Tematyka> lista = new List<Tematyka>(); //Tworzymy obiekt List<Tematyka>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_tematyki, o.nazwa from Tematyki as o");

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                Tematyka tematyka = new Tematyka(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                tematyka.Id = Convert.ToInt32(row["id_tematyki"]);
                tematyka.Nazwa = row["nazwa"].ToString();

                lista.Add(tematyka); //Wrzucamy do listy   
            }

            return lista;
		}

		public static List<Czestotliwosc> WszystkieCzestotliwosci() {
            List<Czestotliwosc> lista = new List<Czestotliwosc>(); //Tworzymy obiekt List<Czestotliwosc>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_czestotliwosc, o.nazwa from Czestotliwosc as o");

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                Czestotliwosc czestotliwosc = new Czestotliwosc(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                czestotliwosc.Id = Convert.ToInt32(row["id_czestotliwosc"]);
                czestotliwosc.Nazwa = row["nazwa"].ToString();

                lista.Add(czestotliwosc); //Wrzucamy do listy   
            }

            return lista;
		}

		public static List<Odbiorca> WszyscyOdbiorcy() {
            List<Odbiorca> lista = new List<Odbiorca>(); //Tworzymy obiekt List<Odbiorca>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_odbiorcy, o.nazwa from Odbiorcy as o");

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                Odbiorca odbiorca = new Odbiorca(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                odbiorca.Id = Convert.ToInt32(row["id_odbiorcy"]);
                odbiorca.Nazwa = row["nazwa"].ToString();

                lista.Add(odbiorca); //Wrzucamy do listy   
            }

            return lista;
		}

		public static List<Kategoria> WszystkieKategorie() {
            List<Kategoria> lista = new List<Kategoria>(); //Tworzymy obiekt List<Kategoria>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_kategorii, o.nazwa from Kategorie as o");

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                Kategoria kategoria = new Kategoria(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                kategoria.Id = Convert.ToInt32(row["id_kategorii"]);
                kategoria.Nazwa = row["nazwa"].ToString();

                lista.Add(kategoria); //Wrzucamy do listy   
            }

            return lista;
		}
	}
}
