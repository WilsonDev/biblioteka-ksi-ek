using Biblioteka.Baza;
using System;
using System.Collections.Generic;
using System.Data;

namespace BibliotekaKsiazek {
	public class CzasopismoRepository : IRepository<Czasopismo> {
		private List<Czasopismo> lista;
		public List<Czasopismo> Lista {
			get {
				return lista;
			}
			set {
				lista = value;
			}
		}

        //Konstruktor
		public CzasopismoRepository() {
		}

        public void PobierzDane() {
            lista = new List<Czasopismo>(); //Tworzymy obiekt List<Czasopismo>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_czasopisma, o.ISSN, o.tytul,o.nr_czasopisma, o.data_wydania, o.wydawnictwo, o.id_tematyki, o.id_odbiorcy, o.id_czestotliwosc from Czasopisma as o");

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                Czasopismo czasopismo = new Czasopismo(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                czasopismo.Id = Convert.ToInt32(row["id_czasopisma"]);
                czasopismo.Issn = row["ISSN"].ToString();
                czasopismo.Tytul = row["tytul"].ToString();
                czasopismo.NrCzasopisma = row["nr_czasopisma"].ToString();
                czasopismo.DataWydania = Convert.ToDateTime(row["data_wydania"]);
                czasopismo.Wydawnictwo = row["wydawnictwo"].ToString();
                czasopismo.Tematyka = ZagadnieniaRepository.WszystkieTematyki().Find(k => k.Id == Convert.ToInt32(row["id_tematyki"]));
                czasopismo.Odbiorca = ZagadnieniaRepository.WszyscyOdbiorcy().Find(k => k.Id == Convert.ToInt32(row["id_odbiorcy"]));
                czasopismo.Czestotliwosc = ZagadnieniaRepository.WszystkieCzestotliwosci().Find(k => k.Id == Convert.ToInt32(row["id_czestotliwosc"]));

                lista.Add(czasopismo); //Wrzucamy do listy  
            }
        }

		public List<Czasopismo> PoDacie(DateTime data) {
            return lista.FindAll(k => k.DataWydania.Equals(data));
		}

		public List<Czasopismo> PoTytule(string tytul) {
            return lista.FindAll(k => k.Tytul.Contains(tytul));
		}

		public List<Czasopismo> PoWydawnictwie(string wydawnictwo) {
            return lista.FindAll(k => k.Wydawnictwo.Contains(wydawnictwo));
		}

		public Czasopismo PoISSN(string issn) {
            return lista.Find(k => k.Issn.Equals(issn));
		}

		public List<Czasopismo> PoTematyce(Tematyka tematyka) {
            return lista.FindAll(k => k.Tematyka.Id == tematyka.Id);
		}

		public List<Czasopismo> PoOdbiorcy(Odbiorca odbiorca) {
            return lista.FindAll(k => k.Odbiorca.Id == odbiorca.Id);
		}

		public List<Czasopismo> PoCzestotliwosci(Czestotliwosc czestotliwosc) {
            return lista.FindAll(k => k.Czestotliwosc.Id == czestotliwosc.Id);
		}

		public List<Czasopismo> Wyszukaj(string issn, string tytul, DateTime dataOd, DateTime dataDo, string wyd, Tematyka tem, Czestotliwosc czest, Odbiorca odb) {
            return lista.FindAll(k => 
                    k.Issn.IndexOf(issn, 0, StringComparison.CurrentCultureIgnoreCase) != -1 &&
                    k.Tytul.IndexOf(tytul, 0, StringComparison.CurrentCultureIgnoreCase) != -1 && 
                    (k.DataWydania >= dataOd && k.DataWydania <= dataDo) && 
                    k.Wydawnictwo.Contains(wyd) && 
                    (tem == null || k.Tematyka.Id == tem.Id) && 
                    (czest == null || k.Czestotliwosc.Id == czest.Id) && 
                    (odb == null || k.Odbiorca.Id == odb.Id));
		}

        public string Usun(Czasopismo obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("DELETE FROM Czasopisma WHERE id_czasopisma='" + obiekt.Id + "'");
        }

		public string Edytuj(Czasopismo obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("update Czasopisma set ISSN = '" + obiekt.Issn + "', tytul = '" + obiekt.Tytul + "', nr_czasopisma = '" + obiekt.NrCzasopisma +
                "', data_wydania = '" + obiekt.DataWydania.ToShortDateString() + "', wydawnictwo = '" + obiekt.Wydawnictwo +
                "', id_tematyki = '" + obiekt.Tematyka.Id + "', id_odbiorcy = '" + obiekt.Odbiorca.Id + "', id_czestotliwosc = '" + obiekt.Czestotliwosc.Id +
                "' where id_czasopisma = '" + obiekt.Id + "'");
		}

		public string Dodaj(Czasopismo obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("insert into Czasopisma (ISSN, tytul, nr_czasopisma, data_wydania, wydawnictwo, id_tematyki, id_odbiorcy, id_czestotliwosci)" +
                "values ('" + obiekt.Issn + "', '" + obiekt.Tytul + "', '" + obiekt.NrCzasopisma + "', '" + obiekt.DataWydania.ToShortDateString() + "' , '" +
                obiekt.Wydawnictwo + "', '" + obiekt.Tematyka.Id + "', '" + obiekt.Odbiorca.Id + "', '" + obiekt.Czestotliwosc.Id + "')");
		}

	}

}
