using Biblioteka.Baza;
using System;
using System.Collections.Generic;
using System.Data;

namespace BibliotekaKsiazek {
	/// <summary>
	/// Je�eli usuwamy czytelnika to usuwamy te� jego kart� i adres
	/// </summary>
	public class CzytelnikRepository : IRepository<Czytelnik> {
		private List<Czytelnik> lista;
		public List<Czytelnik> Lista {
			get {
				return lista;
			}
			set {
				lista = value;
			}
		}

        //Kostruktor
		public CzytelnikRepository() {	
		}

        public void PobierzDane() {
            lista = new List<Czytelnik>(); //Tworzymy obiekt List<Czytelnik>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_czytelnika, o.pesel, o.imie, o.nazwisko, o.data_urodzenia, o.plec, o.haslo, o.id_adresu from Czytelnicy as o");

            AdresRepository adresRepository = new AdresRepository();
            adresRepository.PobierzDane();

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                Czytelnik czytelnik = new Czytelnik(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                czytelnik.Id = Convert.ToInt32(row["id_czytelnika"]);
                czytelnik.Pesel = row["pesel"].ToString();
                czytelnik.Imie = row["imie"].ToString();
                czytelnik.Nazwisko = row["nazwisko"].ToString();
                czytelnik.DataUrodzenia = Convert.ToDateTime(row["data_urodzenia"]);
                czytelnik.Plec = Convert.ToChar(row["plec"]);
                czytelnik.Haslo = row["haslo"].ToString();
                czytelnik.Adres = adresRepository.Lista.Find(k => k.Id == Convert.ToInt32(row["id_adresu"]));

                lista.Add(czytelnik); //Wrzucamy do listy 
            }
        }

        public Czytelnik PoId(long id) {
            return lista.Find(k => k.Id == id);
        }

        public List<Czytelnik> PoImieniu(string imie) {
            return lista.FindAll(k => k.Imie.IndexOf(imie, 0, StringComparison.CurrentCultureIgnoreCase) != -1 
                || k.Nazwisko.IndexOf(imie, 0, StringComparison.CurrentCultureIgnoreCase) != -1);
        }

        public string Usun(Czytelnik obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("DELETE FROM Czytelnicy WHERE id_czytelnika='" + obiekt.Id + "'");
        }

		public string Edytuj(Czytelnik obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("update Czytelnicy set pesel = '" + obiekt.Pesel + "', imie = '" + obiekt.Imie + "', nazwisko = '" + obiekt.Nazwisko + "', data_urodzenia = '" + 
                obiekt.DataUrodzenia.ToShortDateString() + "', plec = '" + obiekt.Plec + "', haslo = '" + obiekt.Haslo + "', id_adresu = '" + obiekt.Adres.Id + "' where id_czytelnika = '" + obiekt.Id + "'");
		}
            
		public string Dodaj(Czytelnik obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("insert into Czytelnicy (pesel, imie, nazwisko, data_urodzenia, plec, haslo, id_adresu)" +
                "values ('" + obiekt.Pesel + "', '" + obiekt.Imie + "', '" + obiekt.Nazwisko + "', '" + obiekt.DataUrodzenia.ToShortDateString() + "', '" +
                obiekt.Plec + "', '" + obiekt.Haslo + "', '" + obiekt.Adres.Id + "')");
		}
	}
}
