using Biblioteka.Baza;
using System;
using System.Collections.Generic;
using System.Data;

namespace BibliotekaKsiazek {
	/// <summary>
	/// Egzemplarz czasopisma
	/// </summary>
	public class ECRepository : IRepository<EgzemplarzCzasopisma> {
		private List<EgzemplarzCzasopisma> lista;
		public List<EgzemplarzCzasopisma> Lista {
			get {
				return lista;
			}
			set {
				lista = value;
			}
		}

        //Konstruktor
		public ECRepository() {	
		}

        public void PobierzDane() {
            lista = new List<EgzemplarzCzasopisma>(); //Tworzymy obiekt List<EgzemplarzCzasopisma>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_egzemplarza, o.sygnatura, o.id_czasopisma from Karty_Egzemplarzy_Czasopisma as o");

            CzasopismoRepository czasopismoRepository = new CzasopismoRepository();
            czasopismoRepository.PobierzDane();

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                EgzemplarzCzasopisma egzemplarzCzasopisma = new EgzemplarzCzasopisma(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                egzemplarzCzasopisma.Id = Convert.ToInt32(row["id_egzemplarza"]);
                egzemplarzCzasopisma.Sygnatura = row["sygnatura"].ToString();
                egzemplarzCzasopisma.Czasopismo = czasopismoRepository.Lista.Find(k => k.Id == Convert.ToInt32(row["id_czasopisma"]));

                lista.Add(egzemplarzCzasopisma); //Wrzucamy do listy  
            }
        }

        public List<EgzemplarzCzasopisma> PoCzasopismie(Czasopismo czasopismo) {
            return lista.FindAll(k => k.Czasopismo.Id == czasopismo.Id);
        }

        public string Usun(EgzemplarzCzasopisma obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("DELETE FROM Karty_Egzemplarzy_Czasopisma WHERE id_egzemplarza='" + obiekt.Id + "'");
        }

		public string Edytuj(EgzemplarzCzasopisma obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("update Karty_Egzemplarzy_Czasopisma set sygnatura = '" + obiekt.Sygnatura + "', id_czasopisma = '" + obiekt.Czasopismo.Id + 
                "' where id_egzemplarza = '"+ obiekt.Id + "'");
		}

		public string Dodaj(EgzemplarzCzasopisma obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("insert into Karty_Egzemplarzy_Czasopisma (sygnatura, id_czasopisma) " +
                "values ('" + obiekt.Sygnatura + "', '" + obiekt.Czasopismo.Id + "')");
		}

	}

}
