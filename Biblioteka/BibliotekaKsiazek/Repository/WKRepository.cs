using Biblioteka.Baza;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;

namespace BibliotekaKsiazek {
	/// <summary>
	/// Wypo�yczenia ksia�ki
	/// </summary>
	public class WKRepository : IRepository<WypozyczenieKsiazki> {
		private List<WypozyczenieKsiazki> lista;
		public List<WypozyczenieKsiazki> Lista {
			get {
				return lista;
			}
			set {
				lista = value;
			}
		}

        //Konstruktor
        public WKRepository() {  
        }

        public void PobierzDane() {
            lista = new List<WypozyczenieKsiazki>(); //Tworzymy obiekt List<Ksiazka>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_egzemplarza, o.id_karty, o.data_wypozyczenia, o.termin_zwrotu, o.data_zwrotu from Karty_Wypozyczen_Ksiazki as o");

            EKRepository ekRepository = new EKRepository();
            ekRepository.PobierzDane();

            KartaCzytelnikaRepository kartaCzytelnikaRepository = new KartaCzytelnikaRepository();
            kartaCzytelnikaRepository.PobierzDane();

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                WypozyczenieKsiazki wypozyczenieKsiazki = new WypozyczenieKsiazki(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                wypozyczenieKsiazki.EgzemplarzKsiazki = ekRepository.Lista.Find(k => k.Id == Convert.ToInt32(row["id_egzemplarza"]));
                wypozyczenieKsiazki.KartaCzytelnika = kartaCzytelnikaRepository.Lista.Find(k => k.Id == Convert.ToInt32(row["id_karty"]));
                wypozyczenieKsiazki.DataWypozyczenia = Convert.ToDateTime(row["data_wypozyczenia"]);
                wypozyczenieKsiazki.TerminZwrotu = Convert.ToDateTime(row["termin_zwrotu"]);
                wypozyczenieKsiazki.DataZwrotu = Convert.ToDateTime(row["data_zwrotu"]);

                lista.Add(wypozyczenieKsiazki); //Wrzucamy do listy   
            }
        }

        public List<WypozyczenieKsiazki> Wypozyczone() {
            return lista.FindAll(k => k.DataZwrotu < new DateTime(1900, 1, 1));
        }

        public List<WypozyczenieKsiazki> WypozyczonePoCzytelniku(KartaCzytelnika karta) {
            return lista.FindAll(k => k.DataZwrotu < new DateTime(1900, 1, 1) && k.KartaCzytelnika.Id == karta.Id);
        }

        public bool czyEgzemplarzWypozyczony(EgzemplarzKsiazki egzemplarz) {
            List<WypozyczenieKsiazki> wypozyczenia = lista.FindAll(k => k.EgzemplarzKsiazki.Id == egzemplarz.Id && k.DataZwrotu < new DateTime(1900, 1, 1));

            return (wypozyczenia.Count == 0)?false:true;
        }

		public List<WypozyczenieKsiazki> PoDacieWypozyczenia(DateTime data) {
            return lista.FindAll(k => k.DataWypozyczenia.ToShortDateString().Equals(data.ToShortDateString()));
        }

        public List<WypozyczenieKsiazki> PoTerminieZwrotu(DateTime data) {
            return lista.FindAll(k => k.TerminZwrotu.ToShortDateString().Equals(data.ToShortDateString()));
        }

        public List<WypozyczenieKsiazki> PoDacieZwrotu(DateTime data) {
            return lista.FindAll(k => k.DataZwrotu.ToShortDateString().Equals(data.ToShortDateString()));
        }

        public List<WypozyczenieKsiazki> PoISBN(string isbn) {
            return lista.FindAll(k => k.EgzemplarzKsiazki.Ksiazka.Isbn.Contains(isbn));
        }

        public List<WypozyczenieKsiazki> PoCzytelniku(Czytelnik czytelnik) {
            return lista.FindAll(k => k.KartaCzytelnika.Czytelnik.Id == czytelnik.Id);
        }

        public List<WypozyczenieKsiazki> Wyszukaj(DateTime dataWyp, DateTime terminZwr, DateTime dataZwr, string isbn, Czytelnik czyt) {
            return lista.FindAll(k => k.DataWypozyczenia.Equals(dataWyp) && k.TerminZwrotu.Equals(terminZwr) && k.DataZwrotu.Equals(dataZwr) && k.EgzemplarzKsiazki.Ksiazka.Isbn.Contains(isbn) && k.KartaCzytelnika.Czytelnik.Id == czyt.Id);
        }

		public List<Ksiazka> TopKsiazki() {
			throw new System.Exception("Not implemented");
		}

		public List<Kategoria> TopKategorie() {
			throw new System.Exception("Not implemented");
		}

		public double WskaznikWypozyczen() {
			throw new System.Exception("Not implemented");
		}

        public string Zwroc(WypozyczenieKsiazki obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("UPDATE Karty_Wypozyczen_Ksiazki SET data_zwrotu='" + DateTime.Now.ToShortDateString() + 
                "' WHERE id_egzemplarza='" + obiekt.EgzemplarzKsiazki.Id + "' AND id_karty='" + obiekt.KartaCzytelnika.Id + "'");
        }

        public string UsunWypozyczeniaEgzemplarza(EgzemplarzKsiazki obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("DELETE FROM Karty_Wypozyczen_Ksiazki WHERE id_egzemplarza='" + obiekt.Id + "'");
        }

        public string Usun(WypozyczenieKsiazki obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("DELETE FROM Karty_Wypozyczen_Ksiazki WHERE id_egzemplarza='" + obiekt.EgzemplarzKsiazki.Id +"' AND id_karty='" + obiekt.KartaCzytelnika.Id + "'");
        }

        public string Edytuj(WypozyczenieKsiazki obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("UPDATE Karty_Wypozyczen_Ksiazki SET id_karty='" + obiekt.KartaCzytelnika.Id + "', data_wypozyczenia='" + obiekt.DataWypozyczenia + "', termin_zwrotu='" + 
                obiekt.TerminZwrotu + "', data_zwrotu='" + obiekt.DataZwrotu + "' WHERE id_egzemplarza='" + obiekt.EgzemplarzKsiazki.Id + "' and id_karty='" + obiekt.KartaCzytelnika.Id + "'");
        }
        
		public string Dodaj(WypozyczenieKsiazki obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("INSERT INTO  Karty_Wypozyczen_Ksiazki (id_egzemplarza, id_karty, data_wypozyczenia, termin_zwrotu, data_zwrotu)" +
                "values ('" + obiekt.EgzemplarzKsiazki.Id + "', '" + obiekt.KartaCzytelnika.Id + "', '" + obiekt.DataWypozyczenia.ToShortDateString() + "', '" + obiekt.TerminZwrotu.ToShortDateString() + "' , '" +
                obiekt.DataZwrotu.ToShortDateString() + "')");
		}
	}
}
