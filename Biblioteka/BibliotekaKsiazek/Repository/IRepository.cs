using System;
using System.Collections.Generic;

namespace BibliotekaKsiazek {
	public interface IRepository<T> {
		List<T> Lista {
			get;
			set;
		}

        void PobierzDane();
		string Dodaj(T obiekt);
		string Edytuj(T obiekt);
		string Usun(T obiekt);

	}

}
