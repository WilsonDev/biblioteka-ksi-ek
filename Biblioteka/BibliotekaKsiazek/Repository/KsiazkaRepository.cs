using Biblioteka.Baza;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;

namespace BibliotekaKsiazek {
	public class KsiazkaRepository : IRepository<Ksiazka> {
		private List<Ksiazka> lista;
		public List<Ksiazka> Lista {
			get {
				return lista;
			}
			set {
				lista = value;
			}
		}

        //Konstruktor
		public KsiazkaRepository() {
		}

        public void PobierzDane() {
            lista = new List<Ksiazka>(); //Tworzymy obiekt List<Ksiazka>

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_ksiazki, o.ISBN, o.tytul, o.autor, o.rok_wydania, o.wydawnictwo, o.id_kategorii from Ksiazki as o");

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                Ksiazka ksiazka = new Ksiazka(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                ksiazka.Id = Convert.ToInt32(row["id_ksiazki"]);
                ksiazka.Isbn = row["ISBN"].ToString();
                ksiazka.Tytul = row["tytul"].ToString();
                ksiazka.Autor = row["autor"].ToString();
                ksiazka.RokWydania = Convert.ToInt32(row["rok_wydania"]);
                ksiazka.Wydawnictwo = row["wydawnictwo"].ToString();
                ksiazka.Kategoria = ZagadnieniaRepository.WszystkieKategorie().Find(k => k.Id == Convert.ToInt32(row["id_kategorii"]));

                lista.Add(ksiazka); //Wrzucamy do listy   
            }
        }

		public List<Ksiazka> PoRoku(int rok) {
			return lista.FindAll(k => k.RokWydania == rok);
		}

		public List<Ksiazka> PoKategorii(Kategoria kategoria) {
			return lista.FindAll(k => k.Kategoria.Id == kategoria.Id);
		}

		public List<Ksiazka> PoTytule(string tytul) {
            return lista.FindAll(k => k.Tytul.Contains(tytul));
		}

		public List<Ksiazka> PoWydawnictwie(string wydawnictwo) {
            return lista.FindAll(k => k.Wydawnictwo.Contains(wydawnictwo));
		}

        public List<Ksiazka> PoAutorze(string autor) {
            return lista.FindAll(k => k.Autor.Contains(autor));
        }

		public Ksiazka PoISBN(string isbn) {
			return lista.Find(k => k.Isbn.Equals(isbn));
		}

        public List<Ksiazka> Wyszukaj(string isbn, string tytul, string autor, int? rok, string wyd, Kategoria kat)
        {
            return lista.FindAll(k =>
                    k.Isbn.IndexOf(isbn, 0, StringComparison.CurrentCultureIgnoreCase) != -1 &&
                    k.Tytul.IndexOf(tytul, 0, StringComparison.CurrentCultureIgnoreCase) != -1 &&
                    k.Autor.IndexOf(autor, 0, StringComparison.CurrentCultureIgnoreCase) != -1 &&
                    (rok == null || k.RokWydania == rok) &&
                    k.Wydawnictwo.IndexOf(wyd, 0, StringComparison.CurrentCultureIgnoreCase) != -1 &&
                    (kat == null || k.Kategoria.Id == kat.Id));                
        }

        public string Usun(Ksiazka obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("DELETE FROM Ksiazki WHERE id_ksiazki='" + obiekt.Id + "'");
        }

        public string Edytuj(Ksiazka obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("UPDATE Ksiazki SET autor='" + obiekt.Autor + "', ISBN='"+obiekt.Isbn+"', tytul='"+obiekt.Tytul+"', rok_wydania='"+obiekt.RokWydania+"', wydawnictwo='"+obiekt.Wydawnictwo+"', id_kategorii='"+obiekt.Kategoria.Id+"' WHERE id_ksiazki= '"
                + obiekt.Id + "'");
        }

		public string Dodaj(Ksiazka obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("INSERT INTO  Ksiazki (autor, ISBN, tytul, rok_wydania, wydawnictwo, id_kategorii)" +
                "values ('" + obiekt.Autor + "', '" + obiekt.Isbn + "', '" + obiekt.Tytul + "', '" + obiekt.RokWydania + "' , '" +
                obiekt.Wydawnictwo + "', '" + obiekt.Kategoria.Id +  "')");
		}
	}
}
