using Biblioteka.Baza;
using System;
using System.Collections.Generic;
using System.Data;

namespace BibliotekaKsiazek {
	public class KartaCzytelnikaRepository : IRepository<KartaCzytelnika>  {
		private List<KartaCzytelnika> lista;
		public List<KartaCzytelnika> Lista {
			get {
				return lista;
			}
			set {
				lista = value;
			}
		}

        public enum Status //Dopuszczalne statusy karty czytelnika
        {
            aktywna,
            nieaktywna
            //uniewazniona,
            //zablokowana
        }

        //Konstruktor
		public KartaCzytelnikaRepository() {    
		}

        public void PobierzDane() {
            lista = new List<KartaCzytelnika>(); //Tworzymy obiekt listy

            BazaSQLite baza = new BazaSQLite(); //Pomocnicza klasa zawieraj�ca metod� GetDataTable
            DataTable dt = new DataTable();
            //Metoda przyjmuj�ca zapytanie SELECT w String i zwracaj�ca DataTable
            dt = baza.GetDataTable("select o.id_karty, o.id_czytelnika, o.status, o.data_wystawienia, o.termin_waznosci from Karty as o");

            CzytelnikRepository czytelnikRepository = new CzytelnikRepository();
            czytelnikRepository.PobierzDane();

            foreach (DataRow row in dt.Rows) //Iterujemy po ka�dym wierszu w DataTable
            {
                KartaCzytelnika KC = new KartaCzytelnika(); //Tworzymy obiekt do kt�rego bedziemy przypisywa� warto�ci z DataRow
                KC.Id = Convert.ToInt32(row["id_karty"]);
                KC.Status = row["status"].ToString();
                KC.DataWystawienia = Convert.ToDateTime(row["data_wystawienia"]);
                KC.TerminWaznosci = Convert.ToDateTime(row["termin_waznosci"]);
                KC.Czytelnik = czytelnikRepository.Lista.Find(k => k.Id == Convert.ToInt32(row["id_czytelnika"]));

                if (!KC.Status.Equals("RIP")) //nie�ywych nie wrzucamy do listy
                lista.Add(KC); //Wrzucamy do listy  
            }
        }

        public List<KartaCzytelnika> Wyszukaj(string imie, string status)
        {
            if (status.Equals(""))
                return lista.FindAll(k => k.Czytelnik.Imie.IndexOf(imie, 0, StringComparison.CurrentCultureIgnoreCase) != -1
                    || k.Czytelnik.Nazwisko.IndexOf(imie, 0, StringComparison.CurrentCultureIgnoreCase) != -1);
            else
                return lista.FindAll(k => (k.Czytelnik.Imie.IndexOf(imie, 0, StringComparison.CurrentCultureIgnoreCase) != -1
                    || k.Czytelnik.Nazwisko.IndexOf(imie, 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                    && k.Status.Equals(status));
        }

        public KartaCzytelnika PoCzytelniku(Czytelnik czytelnik) {
            return lista.Find(k => k.Czytelnik.Id == czytelnik.Id);
        }

        public List<KartaCzytelnika> PoStatusie(string status) {
            return lista.FindAll(k => k.Status.Equals(status));
        }

        public string Usun(KartaCzytelnika obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("DELETE FROM Karty WHERE id_karty='" + obiekt.Id + "'");
        }

        public string Edytuj(KartaCzytelnika obiekt)
        {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("UPDATE Karty SET id_czytelnika='"+obiekt.Czytelnik.Id+"', status='"+obiekt.Status+"', data_wystawienia='"+obiekt.DataWystawienia+"', termin_waznosci='"+obiekt.TerminWaznosci+"' WHERE id_karty= '"
                + obiekt.Id + "'");
        }

		public string Dodaj(KartaCzytelnika obiekt) {
            BazaSQLite baza = new BazaSQLite();
            return baza.NonQuery("INSERT INTO  Karty (id_czytelnika, status, data_wystawienia, termin_waznosci)" +
                "values ('" + obiekt.Czytelnik.Id + "', '" + obiekt.Status + "', '" + obiekt.DataWystawienia.ToShortDateString() + "', '" + obiekt.TerminWaznosci.ToShortDateString() + "')");
		}
	}
}
