using System;
namespace BibliotekaKsiazek {
	public abstract class Egzemplarz {
		private long id;
		public long Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		private string sygnatura;
		public string Sygnatura {
			get {
				return sygnatura;
			}
			set {
				sygnatura = value;
			}
		}
	}
}
