using System;
using System.Collections.Generic;
using System.Diagnostics;
namespace BibliotekaKsiazek {
	public class EgzemplarzKsiazki : Egzemplarz  {
        private Ksiazka ksiazka;
        public Ksiazka Ksiazka
        {
            get
            {
                return ksiazka;
            }
            set
            {
                ksiazka = value;
            }
        }

		public EgzemplarzKsiazki() {
		}
	}
}
