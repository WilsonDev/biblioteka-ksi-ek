﻿using System;

namespace BibliotekaKsiazek {

    public class NajczesciejWybierany
    {
        private int pozycja;
        public int Pozycja
        {
            get
            {
                return pozycja;
            }
            set
            {
                pozycja = value;
            }
        }

        private string nazwa;
        public string Nazwa
        {
            get
            {
                return nazwa;
            }
            set
            {
                nazwa = value;
            }
        }

        private int liczbaWypozyczen;
        public int LiczbaWypozyczen
        {
            get
            {
                return liczbaWypozyczen;
            }
            set
            {
                liczbaWypozyczen = value;
            }
        }

        public NajczesciejWybierany()
        {
		}
    }
}
