using System;

namespace BibliotekaKsiazek {
	public class Czasopismo {
		private long id;
		public long Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		private string issn;
		public string Issn {
			get {
				return issn;
			}
			set {
				issn = value;
			}
		}

		private string tytul;
		public string Tytul {
			get {
				return tytul;
			}
			set {
				tytul = value;
			}
		}

		private string nrCzasopisma;
		public string NrCzasopisma {
			get {
				return nrCzasopisma;
			}
			set {
				nrCzasopisma = value;
			}
		}

		private DateTime dataWydania;
		public DateTime DataWydania {
			get {
				return dataWydania;
			}
			set {
				dataWydania = value;
			}
		}

		private string wydawnictwo;
		public string Wydawnictwo {
			get {
				return wydawnictwo;
			}
			set {
				wydawnictwo = value;
			}
		}

        private Tematyka tematyka;
        public Tematyka Tematyka
        {
            get
            {
                return tematyka;
            }
            set
            {
                tematyka = value;
            }
        }

        private Czestotliwosc czestotliwosc;
        public Czestotliwosc Czestotliwosc
        {
            get
            {
                return czestotliwosc;
            }
            set
            {
                czestotliwosc = value;
            }
        }

        private Odbiorca odbiorca;
        public Odbiorca Odbiorca
        {
            get
            {
                return odbiorca;
            }
            set
            {
                odbiorca = value;
            }
        }

		public Czasopismo() {
		}

	}

}
