using System;
namespace BibliotekaKsiazek {
	public class Adres {
		private long id;
		public long Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		private string ulica;
		public string Ulica {
			get {
				return ulica;
			}
			set {
				ulica = value;
			}
		}

		private string kodPocztowy;
		public string KodPocztowy {
			get {
				return kodPocztowy;
			}
			set {
				kodPocztowy = value;
			}
		}

		private string miejscowosc;
		public string Miejscowosc {
			get {
				return miejscowosc;
			}
			set {
				miejscowosc = value;
			}
		}

		private string telefon;
		public string Telefon {
			get {
				return telefon;
			}
			set {
				telefon = value;
			}
		}

		public Adres() {
		}

	}

}
