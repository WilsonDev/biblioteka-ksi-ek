using System;
namespace BibliotekaKsiazek {
	public class WypozyczenieCzasopisma : Wypozyczenie {
        private EgzemplarzCzasopisma egzemplarzCzasopisma;
        public EgzemplarzCzasopisma EgzemplarzCzasopisma
        {
            get
            {
                return egzemplarzCzasopisma;
            }
            set
            {
                egzemplarzCzasopisma = value;
            }
        }

		public WypozyczenieCzasopisma() {
		}

        public override string Tytul()
        {
            return EgzemplarzCzasopisma.Czasopismo.Tytul;
        }

        public override string Sygnatura()
        {
            return egzemplarzCzasopisma.Sygnatura;
        }

        public override long Id()
        {
            return egzemplarzCzasopisma.Czasopismo.Id;
        }
	}
}
