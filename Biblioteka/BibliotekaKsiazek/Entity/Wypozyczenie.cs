using System;
namespace BibliotekaKsiazek {
	public abstract class Wypozyczenie {
		private DateTime dataWypozyczenia;
		public DateTime DataWypozyczenia {
			get {
				return dataWypozyczenia;
			}
			set {
				dataWypozyczenia = value;
			}
		}

		private DateTime terminZwrotu;
		public DateTime TerminZwrotu {
			get {
				return terminZwrotu;
			}
			set {
				terminZwrotu = value;
			}
		}

		private DateTime dataZwrotu;
		public DateTime DataZwrotu {
			get {
				return dataZwrotu;
			}
			set {
				dataZwrotu = value;
			}
		}

        private KartaCzytelnika kartaCzytelnika;
        public KartaCzytelnika KartaCzytelnika
        {
            get
            {
                return kartaCzytelnika;
            }
            set
            {
                kartaCzytelnika = value;
            }
        }

		public bool PrzekroczonoTermin() {
			return (DataZwrotu.Date <= TerminZwrotu.Date)?false:true;
		}

		/// <summary>
		/// O ile dni przekroczono termin oddania ksiazki/czasopisma
		/// </summary>
		public int OIleDni() {
            return (DateTime.Now - DataZwrotu).Days;
		}

		public string ZwrocKsiazke() {
            TerminZwrotu = DateTime.Now;

            return "Oddano";
		}

        public abstract string Tytul();

        public abstract string Sygnatura();

        public abstract long Id();

        public long IdCzytelnika()
        {
            return kartaCzytelnika.Czytelnik.Id;
        }

        public override string ToString()
        {
            return String.Format("{0} {1} {2}", this.kartaCzytelnika.Id, this.kartaCzytelnika.Czytelnik.Imie, this.kartaCzytelnika.Czytelnik.Nazwisko);
        }
	}
}
