using System;
namespace BibliotekaKsiazek {
	public class WypozyczenieKsiazki : Wypozyczenie  {
        private EgzemplarzKsiazki egzemplarzKsiazki;
        public EgzemplarzKsiazki EgzemplarzKsiazki
        {
            get
            {
                return egzemplarzKsiazki;
            }
            set
            {
                egzemplarzKsiazki = value;
            }
        }

		public WypozyczenieKsiazki() {
		}

        public override string Tytul()
        {
            return egzemplarzKsiazki.Ksiazka.Tytul;
        }

        public override string Sygnatura()
        {
            return egzemplarzKsiazki.Sygnatura;
        }

        public override long Id()
        {
            return egzemplarzKsiazki.Ksiazka.Id;
        }
	}
}
