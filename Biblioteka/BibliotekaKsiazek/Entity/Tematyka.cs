using System;
namespace BibliotekaKsiazek {
	public class Tematyka {
        private long id;
        public long Id {
            get {
                return id;
            }
            set {
                id = value;
            }
        }

        private string nazwa;
        public string Nazwa {
            get {
                return nazwa;
            }
            set {
                nazwa = value;
            }
        }

		public Tematyka() {
		}

	}

}
