using System;
namespace BibliotekaKsiazek {
	public class KartaCzytelnika {
		private long id;
		public long Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		private string status;
		public string Status {
			get {
				return status;
			}
			set {
				status = value;
			}
		}

		private DateTime dataWystawienia;
		public DateTime DataWystawienia {
			get {
				return dataWystawienia;
			}
			set {
				dataWystawienia = value;
			}
		}

		private DateTime terminWaznosci;
		public DateTime TerminWaznosci {
			get {
				return terminWaznosci;
			}
			set {
				terminWaznosci = value;
			}
		}

        private Czytelnik czytelnik;
        public Czytelnik Czytelnik
        {
            get
            {
                return czytelnik;
            }
            set
            {
                czytelnik = value;
            }
        }

		public KartaCzytelnika() {
		}

        //Do poprawy
		public bool PrzekroczonoTermin() {
            return (DateTime.Now.Date <= TerminWaznosci.Date) ? false : true;
		}

		public void PrzedluzWaznosc() {
			throw new System.Exception("Not implemented");
		}

		public void Zablokuj() {
			throw new System.Exception("Not implemented");
		}

		public void Odblokuj() {
			throw new System.Exception("Not implemented");
		}

		public bool CzyAktywna() {
			throw new System.Exception("Not implemented");
		}
	}
}
