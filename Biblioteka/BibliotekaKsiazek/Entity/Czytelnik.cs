using System;
namespace BibliotekaKsiazek {
	public class Czytelnik {
		private long id;
		public long Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		private string pesel;
		public string Pesel {
			get {
				return pesel;
			}
			set {
				pesel = value;
			}
		}

		private string imie;
		public string Imie {
			get {
				return imie;
			}
			set {
				imie = value;
			}
		}

		private string nazwisko;
		public string Nazwisko {
			get {
				return nazwisko;
			}
			set {
				nazwisko = value;
			}
		}

		private DateTime dataUrodzenia;
		public DateTime DataUrodzenia {
			get {
				return dataUrodzenia;
			}
			set {
				dataUrodzenia = value;
			}
		}

		private char plec;
		public char Plec {
			get {
				return plec;
			}
			set {
				plec = value;
			}
		}

		private string haslo;
		public string Haslo {
			get {
				return haslo;
			}
			set {
				haslo = value;
			}
		}

        private Adres adres;
        public Adres Adres
        {
            get
            {
                return adres;
            }
            set
            {
                adres = value;
            }
        }

		public Czytelnik() {
		}

	}

}
