using System;
namespace BibliotekaKsiazek {
	public class EgzemplarzCzasopisma : Egzemplarz  {
        private Czasopismo czasopismo;
        public Czasopismo Czasopismo
        {
            get
            {
                return czasopismo;
            }
            set
            {
                czasopismo = value;
            }
        }

		public EgzemplarzCzasopisma() {
		}
	}
}
