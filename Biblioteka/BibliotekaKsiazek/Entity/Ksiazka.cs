using System;
namespace BibliotekaKsiazek {
	public class Ksiazka {
		private long id;
		public long Id {
			get {
				return id;
			}
			set {
				id = value;
			}
		}

		private string isbn;
		public string Isbn {
			get {
				return isbn;
			}
			set {
				isbn = value;
			}
		}

		private string tytul;
		public string Tytul {
			get {
				return tytul;
			}
			set {
				tytul = value;
			}
		}

        private string autor;
        public string Autor {
            get {
                return autor;
            }
            set {
                autor = value;
            }
        }

		private int rokWydania;
		public int RokWydania {
			get {
				return rokWydania;
			}
			set {
				rokWydania = value;
			}
		}

		private string wydawnictwo;
		public string Wydawnictwo {
			get {
				return wydawnictwo;
			}
			set {
				wydawnictwo = value;
			}
		}

        private Kategoria kategoria;
        public Kategoria Kategoria
        {
            get
            {
                return kategoria;
            }
            set
            {
                kategoria = value;
            }
        }

		public Ksiazka() {
		}

	}

}
