﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biblioteka.Forms;
using BibliotekaKsiazek;

namespace Biblioteka {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            Logowanie logowanie = new Logowanie();
            logowanie.ShowDialog();
            if (logowanie.CzyZalogowano == Logowanie.Permission.Admin)
            {
                Application.Run(new PanelAdministratora());
            }
            else if (logowanie.CzyZalogowano == Logowanie.Permission.User)
	        {
                Application.Run(new PanelCzytelnika(logowanie.czytelnikLogowany));
            }
            else if (logowanie.CzyZalogowano == Logowanie.Permission.Guest)
	        {
                Application.Run(new PanelCzytelnika("Gosc"));
            }
        }
    }
}
